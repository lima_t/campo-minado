package dataImage.functions;

import java.awt.*;
import java.net.URL;
import javax.swing.*;

/**
 *Classe responsavel por armazenar os metodos de processamento de imagens da aplicação, as
 * instâncias dessa biblioteca devem chamar exclusivamente os metodos dessa classe, pois
 * ela é o centro de toda a biblioteca.
 * 
 * @author Tarcísio de Lima
 * @version 1.0.0
 */
public class DataImage {
    
    /**
     * O Metodo <b>iconeJanela</b> é responsavel por carregar o icone da imagem na barra de titulos da aplicação, ela recebe
     * como parametros uma <b>String</b> que é o caminho da imagem e retorna a imagem para a janela caso tenha
     * sucesso ou nulo caso gere alguma exeção.
     * 
     * @param urlIcone
     * @version 1.0.0
     */
    public Image  iconeJanela(String urlIcone){
        try{
            URL urlImagemTitulo = Class.class.getClass().getResource(urlIcone);
            Image imagemJanela = Toolkit.getDefaultToolkit().getImage(urlImagemTitulo);
            return imagemJanela;
        }catch(Exception erro){            
            JOptionPane.showMessageDialog(null, "Um erro inesperado ocorreu, não foi possivel carregar o icone da janela "
                    + "assinatura do erro:\n"+erro, "Erro Fatal!", JOptionPane.ERROR_MESSAGE);
            return null;
        }
    }
    
    /**
     * O Metodo <b>iconeComponent</b> é responsavel por carregar o icones da aplicação, ela recebe
     * como parametros uma <b>String</b> que é o caminho da imagem e retorna a imagem para a janela caso tenha
     * sucesso ou nulo caso gere alguma exeção.
     * 
     * @param urlIcone
     * @version 1.0.0
     */
    public Icon  iconeComponent(String urlIcone){
        try{
            URL urlImagem = Class.class.getClass().getResource(urlIcone);
            Icon imagemIcone = new ImageIcon(urlImagem);
            return imagemIcone;
        }catch(Exception erro){            
            JOptionPane.showMessageDialog(null, "Um erro inesperado ocorreu, não foi possivel carregar o icone de um dos "
                    + "componentes assinatura do erro:\n"+erro, "Erro Fatal!", JOptionPane.ERROR_MESSAGE);
            return null;
        }
    }
}
