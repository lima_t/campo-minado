package dataSound.functions.types;

/**
 *Classe responsavel por armazenar as variaveis contendo o endereço dos arquivos de sons do dialogos da aplicação.
 * 
 * @author Tarcísio de Lima
 * @version 1.0.0
 */
public class SoundDialog {
    
    //Variaveis dos dialogos
    private final String DIALOGO_AVISO = "/dataSound/sounds/dialogs/warningDialog.wav";
    private final String DIALOGO_ERRO = "/dataSound/sounds/dialogs/errorDialog.wav";
    private final String DIALOGO_INFORMACAO = "/dataSound/sounds/dialogs/informationDialog.wav";    
    private final String DIALOGO_SUCESSO = "/dataSound/sounds/dialogs/sucessDialog.wav";

    //Metodos Getters para retornar o sons para o metodo de reprodução
    public String getDIALOGO_AVISO() {
        return DIALOGO_AVISO;
    }

    public String getDIALOGO_ERRO() {
        return DIALOGO_ERRO;
    }

    public String getDIALOGO_INFORMACAO() {
        return DIALOGO_INFORMACAO;
    }

    public String getDIALOGO_SUCESSO() {
        return DIALOGO_SUCESSO;
    }
}
