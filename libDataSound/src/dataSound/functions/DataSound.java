package dataSound.functions;

import java.applet.*;
import java.awt.Component;
import java.net.URL;
import javax.swing.JOptionPane;

/**
 * Classe responsavel por armazenar os metodos de reprodução dos sons da aplicação, as
 * instâncias dessa biblioteca devem chamar exclusivamente os metodos dessa classe, pois
 * ela é o centro de toda a biblioteca.
 *
 * @author Tarcísio de Lima
 * @version 1.0.0
 */
public class DataSound {
    
    //Objetos e Variaveis Globais
    AudioClip som = null;

    /**
     * O metodo <b>reproduzirSom</b> é responsavel por executar um arquivo de som em
     * formato <b>.wav</b> no aplicativo, este é por padrão o metodo que deverá ser usado
     * sempre que possivel, criando uma instância dessa classe, ele recebe dois
     * paramêtros, um objeto do tipo <b>Component</b> e uma <b>String</b> sendo a
     * <b>Tela Referente</b>(telaReferente) na qual o dialogo de erro deverá aparecer caso
     * ocorra alguma exceção e o outro o <b>Caminho do Arquivo de Som</b>(urlArquivoSom)
     * responsavel por dizer onde é que está o arquivo de som à ser reproduzido.
     *
     * @param telaReferente
     * @param urlArquivoSom
     */
    public void reproduzirSom(Component telaReferente, String urlArquivoSom) {
        try {
            URL urlArquivo = Class.class.getClass().getResource(urlArquivoSom);
            som = Applet.newAudioClip(urlArquivo);
            som.play();
        } catch (Exception erro) {
            JOptionPane.showMessageDialog(telaReferente, "Ocorreu um erro ao tentar reproduzir um som do aplicativo,\n"
                    + "assinatura do erro:\n" + erro, "Erro!", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    /**
     * O metodo  <b>reproduzirLoop</b> é responsavel por executar um arquivo de som em
     * formato <b>.wav</b> no aplicativo, sua grande diferença é que ele reproduz o clipe de audio 
     * em loop e só finaliza quando o usuário intevir com uma ação que chame o metodo de parar 
     * desta mesma classe, a instância dessa classe, recebe dois paramêtros, um objeto do tipo 
     * <b>Component</b> e uma <b>String</b> sendo a <b>Tela Referente</b>(telaReferente) na
     * qual o dialogo de erro deverá aparecer caso ocorra alguma exceção e o outro o
     * <b>Caminho do Arquivo de Som</b>(urlArquivoSom) responsavel por dizer onde é que está o
     * arquivo de som à ser reproduzido.
     * 
     * @param telaReferente
     * @param urlArquivoSom 
     */
    public void reproduzirLoop(Component telaReferente, String urlArquivoSom){
        try {
            URL urlArquivo = Class.class.getClass().getResource(urlArquivoSom);
            som = Applet.newAudioClip(urlArquivo);
            som.loop();
        } catch (Exception erro) {
            JOptionPane.showMessageDialog(telaReferente, "Ocorreu um erro ao tentar reproduzir um som do aplicativo,\n"
                    + "assinatura do erro:\n" + erro, "Erro!", JOptionPane.ERROR_MESSAGE);
        }
    }
    
    /**
     * O metodo <b>pararSom</b> é responsavel por parar um som que esteja reproduzindo em Loop instantaneamente
     * este metodo deve ser chamado na instancia de um botão para parar a reprodução do clipe de som, ele recebe como
     * parametro um objeto do tipo <b>Component</b> que por sua vez é a tela referente na qual o dialogo de erro deve
     * aparecer caso ocorra alguma exceção.
     * 
     * @param telaReferente 
     */
    public void pararSom(Component telaReferente){
        try {            
            som.stop();
        } catch (Exception erro) {
            JOptionPane.showMessageDialog(telaReferente, "Ocorreu um erro ao tentar reproduzir um som do aplicativo,\n"
                    + "assinatura do erro:\n" + erro, "Erro!", JOptionPane.ERROR_MESSAGE);
        }
    }
}
