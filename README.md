
# Campo Minado

## 1. Resumo
Este projeto é a re-imaginação do clássico game do campo minado presente nas principais plataformas desktop do mercado (Windows & Linux) desenvolvido como projeto da disciplina de Java durante o ano de **2011** e versionado recentemente no GIT. Esta aplicação foi desenvolvida como complemento parcial para aprovação em uma das disciplinas do ensino técnico durante a formação em *Técnico em Desenvolvimento de Sistemas* pela escola *ETE - José Humberto de Moura Cavalcante* 

## 2. Requisitos Funcionais
A solução deve contemplar as seguintes funcionalidades:

- Aplicação deverá rodar em todos os sistemas operacionais (Windows e Linux);
- Deverá trabalhar com o conceito de bibliotecas no Java Swing;
- Realizar conexão com banco de dados JBDC e armazenar os dados dos scores e históricos;
- Gerar uma matriz aleatória a cada partida;
- A matriz deve possuir um nível de risco compatível conforme a dificuldade selecionada;

Para fins de bonificação as seguintes funcionalidades podem ser adicionadas:

- Cadastro de perfil do jogador;
- Diferentes modos de jogo;
- Sons e animações;

## 3. Dependências

O que você precisa para configurar e executar esta aplicação no ambiente local?

- [Java 7 ou superior](https://www.oracle.com/br/java/technologies/javase/javase7-archive-downloads.html)
- [Netbeans 7.1 ou superior](https://netbeans.apache.org/download/nb121/nb121.html)
- [Git SCM](https://git-scm.com/downloads)
- [SourceTree](https://www.sourcetreeapp.com/)
- [SQLite-jdbc3.7.2.jar ou superior](https://github.com/xerial/sqlite-jdbc/releases)
- [Inno Setup Installer](https://jrsoftware.org/isinfo.php)

---

## 4. Tecnologias usadas no Projeto

Neste projeto foram utilizadas as seguintes tecnologias/frameworks.

- **Java Swing** para o desenvolvimento das telinhas, iterações e funcionalidades em geral;
- **Corel Draw** para edição dos ícones/redimensionamento e splash art;
- **SQLite JDBC** para conexão com banco de dados;
- **Inno Setup** para empacotamento da aplicação no Windows e geração do instalador

---

## 5. Executando o Projeto

Para configurar o projeto e executar no ambiente local de desenvolvimento siga os passos:

1. Configurar projeto para incluir a biblioteca *SQLite JDBC* no caminho da aplicação;
2. Adicionar os dois projetos (libDataImage e libDataSound) como depedências da aplicação principal (Campo Minado) e configurar para buildar ambas como libs da aplicação;
3. Executar o projeto como Java Swing (Em versões mais modernas do java configurar o compilador no modo compatibilidade do java 7)

## 6. Download do executável publicado

| Release  | Link |
| ------------- | ------------- |
| v1.0.2  | [Download](https://drive.google.com/file/d/1umwSAiQJz22QoZoY7ollcOyDWLOGfGTj/view?usp=sharing)  |

## Referências

Alguns artigos que foram usados como referência para estudo e construção desse projeto:

1. Referências fornecidas em aula presencial na disciplina de Java em 2011

## Sobre o Autor

Tarcísio de Lima Amorim <br>
Email: tarcisio.lima.amorim@outlook.com <br>
Linkedin: https://www.linkedin.com/in/tarcisio-lima/