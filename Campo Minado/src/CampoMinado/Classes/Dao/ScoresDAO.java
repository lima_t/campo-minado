package CampoMinado.Classes.Dao;

import CampoMinado.Classes.*;
import CampoMinado.Classes.Tabelas.TabelaScores;
import java.awt.Component;
import java.sql.*;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 * @author Tarcísio de Lima
 * @version 1.0.0
 * 
 * Esta classe é responsavel por executar as funções básicas de manipulação da base de dados referente a tabela de
 * scores do jogo, nela estão inclusas as funções de <b>inserir</b>, <b>alterar</b>, <b>exlcuir</b> e <b>consultar</b>
 */
public class ScoresDAO {
    
    //Metodos
    
    /**
     * @param dataJogo 
     * @param nickName 
     * @param nivelDificuldade 
     * @param modalidadeJogo 
     * @param pontos 
     * @param horaJogo 
     * 
     * Este metodo é responsavel por inserir um novo registro na base de dados quando o jogo for finalizado, ele recebe
     * 4 parametros que os mesmos são os dados que serão armazenados no banco
     */
    public static void inserirRecord(int pontos, String nickName, String nivelDificuldade, String modalidadeJogo, String dataJogo, String horaJogo){
        try {
            ConexaoSQLite.conectarBancoDados(null);
            ConexaoSQLite.comando = ConexaoSQLite.conexaoBanco.createStatement();
            
            ConexaoSQLite.comando.execute("INSERT INTO tb_records VALUES(null, '"
                    +nickName+"', "+pontos+", '"+nivelDificuldade+"', '"+modalidadeJogo+"','"+dataJogo+"', '"+horaJogo+"');");
            
        } catch (SQLException erroBd) {
            Values.reproduzirSom(null, DataSound.getSOM_ERRO());
            JOptionPane.showMessageDialog(null, "Não foi possivel inserir o novo registro no banco, assinatura do erro:\n"+
                    erroBd, "Erro!", JOptionPane.ERROR_MESSAGE);
        }finally{
            ConexaoSQLite.desconectarBancoDados(null);
            zerarValores();
        }       
    }
    
    /**
     * @param telaReferente 
     * 
     * O Metodo "excluirDados" é responsavel por excluir todos os registros do banco de dados, fazendo assim com que
     * todo o histórico do jogo até hoje seja limpado deixando a aplicação como se fosse recém instalada, este metodo 
     * recebe como parametro um objeto do tipo <b>Component</b>
     */
    public static void excluirDados(Component telaReferente){
        try {
            ConexaoSQLite.conectarBancoDados(telaReferente);
            ConexaoSQLite.comando = ConexaoSQLite.conexaoBanco.createStatement();            
            ConexaoSQLite.comando.execute("delete from tb_records;");
            
            JOptionPane.showMessageDialog(telaReferente, "Todos os SCORES foram deletados com sucesso!",
                    "Sucesso!",JOptionPane.INFORMATION_MESSAGE);
            
        } catch (SQLException erroBd) {
            Values.reproduzirSom(telaReferente, DataSound.getSOM_ERRO());
            JOptionPane.showMessageDialog(telaReferente, "Erro ao tentar excluir os dados do banco de dados, assinatura"
                    + "do erro:\n"+erroBd,"Erro!",JOptionPane.ERROR_MESSAGE);
        }
    }
    
    /**
     * 
     * @param telaReferente
     * @return  modeloTabelaPadrao
     * 
     * O metodo "consultarRecords" é responsavel por preencher a tabela da TelaScores, através de uma consulta no
     * banco de dados onde ele captura todos os registros de Records do jogo feito até então, este metodo recebe como
     * parametro uma Tela para referência casa haja alguma exeção o Dialogo terá referencia de onde aparecer e não 
     * tem como retorno um <b>TableModel</b>
     */
    public static DefaultTableModel consultarRecords(Component telaReferente){
        ResultSet comandoConsulta;
        DefaultTableModel modeloTabelaPadrao = new DefaultTableModel();        
        
        modeloTabelaPadrao.addColumn("Score");
        modeloTabelaPadrao.addColumn("Jogador");
        modeloTabelaPadrao.addColumn("Nível");
        modeloTabelaPadrao.addColumn("Modo do Jogo");
        modeloTabelaPadrao.addColumn("Data do Jogo");
        modeloTabelaPadrao.addColumn("Hora do Jogo");

        try {
            ConexaoSQLite.conectarBancoDados(telaReferente);
            comandoConsulta = ConexaoSQLite.comando.executeQuery("SELECT * FROM tb_records ORDER BY score_jogador DESC;");
            
           while(comandoConsulta.next()){                
                if(comandoConsulta != null){               
                    modeloTabelaPadrao.addRow(new Object[]{
                        comandoConsulta.getInt("score_jogador"),
                        comandoConsulta.getString("nick_jogador"),
                        comandoConsulta.getString("nivel_dificuldade"),
                        comandoConsulta.getString("modalidade"),
                        comandoConsulta.getString("data_jogo"),
                        comandoConsulta.getString("hora_jogo")
                    });
                }else{
                    Values.reproduzirSom(null, DataSound.getSOM_ERRO());
                    JOptionPane.showMessageDialog(null, "Não há registros de records na base de dados!", "Erro!",
                            JOptionPane.INFORMATION_MESSAGE);
                }
            }
            
            comandoConsulta.close();
            
        } catch (SQLException erroBd) {
            Values.reproduzirSom(null, DataSound.getSOM_ERRO());
            JOptionPane.showMessageDialog(null, "Não foi possivel efetuar a consulta no banco, assinatura do erro:\n"+
                    erroBd, "Erro!", JOptionPane.ERROR_MESSAGE);
        }finally{
            ConexaoSQLite.desconectarBancoDados(null);
            zerarValores();
        }
        
        return modeloTabelaPadrao;        
    }
    
    /**
     * O metodo "zerarValores" tem como principal função zerar todos os valores da classe "TabelaScores" para que ao
     * tentar dar um novo "insert" na base de dados não some os valores posteriores com os novos
     */
    public static void zerarValores(){
        TabelaScores.setId(0);
        TabelaScores.setScoreJogador(0);
        TabelaScores.setNickJogador("");
        TabelaScores.setNivelDificuldade("");
        TabelaScores.setDataJogo("");
        TabelaScores.setHoraJogo("");
    }
}
