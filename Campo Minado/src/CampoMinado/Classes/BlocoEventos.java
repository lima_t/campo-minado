package CampoMinado.Classes;

import java.awt.*;
import javax.swing.*;

/**
 * @author Tarcísio de Lima
 * @version 1.0.1
 * 
 * A Classe "BlocoEventos" é responsavel por monitorar a modalidade e os eventos dos botões de acordo com a dificuldade
 * e tipo de jogo.
 */
public class BlocoEventos {
        
    /**
     * @param telaReferente
     * @param lbPontuacao
     * @param botaoClicado
     * @param linhaMatriz
     * @param colunaMatriz 
     * 
     * O metodo "modoPaciencia" é o metodo responsavel por monitorar os eventos dos botões e da matriz onde estão
     * armazenados as informações da localização das <b>minas</b>, <b>pontos</b> e <b>extras</b>, este metodo
     * sempre será chamado quando a modalidade <b>paciência</b> do jogo for selecionada antes de iniciar a partida.
     * Este metodo recebe 5 parametros herdados da <b>tela referente</b> e não retorna nada.
     */
    public static void modoPaciencia(Component telaReferente, JLabel lbPontuacao, JButton botaoClicado, byte linhaMatriz, byte colunaMatriz){ 
        switch(Values.nivelDificuldade){
            case 1:
                 if(Values.matrizBlocos[linhaMatriz][colunaMatriz] == 0){            
                    botaoClicado.setIcon(new DataImage().getICONE_EXPLOSAO());
                    Values.reproduzirSom(telaReferente, DataSound.getSOM_EXPLOSAO());
                    Values.gameOver(telaReferente);
                }else{
                    Values.pontuacao+= (int) Values.matrizBlocos[linhaMatriz][colunaMatriz];
                    Values.reproduzirSom(telaReferente, DataSound.getSOM_PONTO());            
                    botaoClicado.setText(String.valueOf(Values.matrizBlocos[linhaMatriz][colunaMatriz]));
                    botaoClicado.setEnabled(false);
                    lbPontuacao.setText(String.valueOf(Values.pontuacao)+" Pontos");
                 }
                break;
                
            case 2:
                if(Values.matrizBlocos[linhaMatriz][colunaMatriz] == 0){            
                    botaoClicado.setIcon(new DataImage().getICONE_EXPLOSAO());
                    Values.reproduzirSom(telaReferente, DataSound.getSOM_EXPLOSAO());
                    Values.gameOver(telaReferente);
                }else if(Values.matrizBlocos[linhaMatriz][colunaMatriz] == 5 || Values.matrizBlocos[linhaMatriz][colunaMatriz] == 4){
                        Values.pontuacao+= 10;
                        botaoClicado.setIcon(new DataImage().get$10PONTOS());
                        Values.reproduzirSom(telaReferente, DataSound.getSOM_EXTRA());
                        botaoClicado.setEnabled(false);
                        lbPontuacao.setText(String.valueOf(Values.pontuacao)+" Pontos");
                }else{
                    Values.pontuacao+= (int) Values.matrizBlocos[linhaMatriz][colunaMatriz];
                    Values.reproduzirSom(telaReferente, DataSound.getSOM_PONTO());            
                    botaoClicado.setText(String.valueOf(Values.matrizBlocos[linhaMatriz][colunaMatriz]));
                    botaoClicado.setEnabled(false);
                    lbPontuacao.setText(String.valueOf(Values.pontuacao)+" Pontos");
                 }
                break;
                
            case 3:
                if(Values.matrizBlocos[linhaMatriz][colunaMatriz] == 0 ||
                    Values.matrizBlocos[linhaMatriz][colunaMatriz] == 1 ||
                    Values.matrizBlocos[linhaMatriz][colunaMatriz] == 2
                        ){            
                    botaoClicado.setIcon(new DataImage().getICONE_EXPLOSAO());
                    Values.reproduzirSom(telaReferente, DataSound.getSOM_EXPLOSAO());
                    Values.gameOver(telaReferente);
                }else if(Values.matrizBlocos[linhaMatriz][colunaMatriz] == 3 || Values.matrizBlocos[linhaMatriz][colunaMatriz] == 4){
                        Values.pontuacao+= 10;
                        botaoClicado.setIcon(new DataImage().get$10PONTOS());
                        botaoClicado.addActionListener(null);
                        Values.reproduzirSom(telaReferente, DataSound.getSOM_EXTRA());
                        botaoClicado.setEnabled(false);
                        lbPontuacao.setText(String.valueOf(Values.pontuacao)+" Pontos");
                }else if(Values.matrizBlocos[linhaMatriz][colunaMatriz] == 5){
                        Values.pontuacao+= 15;
                        botaoClicado.setIcon(new DataImage().get$15PONTOS());
                        botaoClicado.addActionListener(null);
                        Values.reproduzirSom(telaReferente, DataSound.getSOM_EXTRA());
                        botaoClicado.setEnabled(false);
                        lbPontuacao.setText(String.valueOf(Values.pontuacao)+" Pontos");
                }else if(Values.matrizBlocos[linhaMatriz][colunaMatriz] == 6){
                        Values.pontuacao+= 20;
                        botaoClicado.setIcon(new DataImage().get$20PONTOS());
                        botaoClicado.addActionListener(null);
                        Values.reproduzirSom(telaReferente, DataSound.getSOM_EXTRA());
                        botaoClicado.setEnabled(false);
                        lbPontuacao.setText(String.valueOf(Values.pontuacao)+" Pontos");
                }else if(Values.matrizBlocos[linhaMatriz][colunaMatriz] == 7){
                        Values.pontuacao+= 25;
                        botaoClicado.setIcon(new DataImage().get$25PONTOS());                        
                        Values.reproduzirSom(telaReferente, DataSound.getSOM_EXTRA());
                       botaoClicado.setEnabled(false);
                        lbPontuacao.setText(String.valueOf(Values.pontuacao)+" Pontos");
                }else if(Values.matrizBlocos[linhaMatriz][colunaMatriz] == 8){
                        Values.pontuacao+= 30;
                        botaoClicado.setIcon(new DataImage().get$30PONTOS());
                        botaoClicado.addActionListener(null);
                        Values.reproduzirSom(telaReferente, DataSound.getSOM_EXTRA());
                        botaoClicado.setEnabled(false);
                        lbPontuacao.setText(String.valueOf(Values.pontuacao)+" Pontos");
                }else{
                        Values.pontuacao+= 50;                        
                        botaoClicado.setIcon(new DataImage().get$50PONTOS());
                        botaoClicado.addActionListener(null);
                        Values.reproduzirSom(telaReferente, DataSound.getSOM_EXTRA());
                        botaoClicado.setEnabled(false);
                        lbPontuacao.setText(String.valueOf(Values.pontuacao)+" Pontos");
                }
                break;
                
            default:
                Values.reproduzirSom(telaReferente, DataSound.getSOM_ERRO());
                JOptionPane.showMessageDialog(telaReferente, "A variavel nivelDificuldade está com um valor fora do padrão"
                        + "a seleção dos blocos não pode continuar!", "Erro!", JOptionPane.ERROR_MESSAGE);
                break;
        }
    }
    
     /**
     * @param telaReferente
     * @param lbPontuacao
     * @param botaoClicado
     * @param linhaMatriz
     * @param colunaMatriz 
     * 
     * O metodo "modoPaciencia" é o metodo responsavel por monitorar os eventos dos botões e da matriz onde estão
     * armazenados as informações da localização das <b>minas</b>, <b>pontos</b> e <b>extras</b>, este metodo
     * sempre será chamado quando a modalidade <b>capture a bandeira</b> do jogo for selecionada antes de iniciar a partida.
     * Este metodo recebe 5 parametros herdados da <b>tela referente</b> e não retorna nada.
     */
    public static void modoCaptureFlag(Component telaReferente, JLabel lbPontuacao, JButton botaoClicado, byte linhaMatriz, byte colunaMatriz){
        switch(Values.nivelDificuldade){
            case 1:
                if(Values.matrizBlocos[linhaMatriz][colunaMatriz] == 0){            
                    botaoClicado.setIcon(new DataImage().getICONE_EXPLOSAO());
                    Values.reproduzirSom(telaReferente, DataSound.getSOM_EXPLOSAO());
                    Values.gameOver(telaReferente);
                }else if(Values.matrizBlocos[linhaMatriz][colunaMatriz] == 5){
                    botaoClicado.setIcon(new DataImage().getBANDEIRA_VERDE());
                    Values.reproduzirSom(telaReferente, DataSound.getSOM_EXTRA());
                    Values.pontuacao *= 10;
                    Values.gameOver(telaReferente);
                }else{
                    Values.pontuacao+= 1;
                    Values.reproduzirSom(telaReferente, DataSound.getSOM_PONTO());            
                    botaoClicado.setIcon(new DataImage().getBANDEIRA_VERMELHA());
                    botaoClicado.setEnabled(false);
                    lbPontuacao.setText(String.valueOf(Values.pontuacao)+" Vermelhos");
                 }
                break;
            case 2:
                if(Values.matrizBlocos[linhaMatriz][colunaMatriz] == 0){            
                    botaoClicado.setIcon(new DataImage().getICONE_EXPLOSAO());
                    Values.reproduzirSom(telaReferente, DataSound.getSOM_EXPLOSAO());
                    Values.gameOver(telaReferente);
                }else if(Values.matrizBlocos[linhaMatriz][colunaMatriz] == 5){
                    botaoClicado.setIcon(new DataImage().getBANDEIRA_VERDE());
                    Values.reproduzirSom(telaReferente, DataSound.getSOM_EXTRA());
                    Values.pontuacao *= 20;
                    Values.gameOver(telaReferente);
                }else{
                    Values.pontuacao+= 1;
                    Values.reproduzirSom(telaReferente, DataSound.getSOM_PONTO());            
                    botaoClicado.setIcon(new DataImage().getBANDEIRA_VERMELHA());
                    botaoClicado.setEnabled(false);
                    lbPontuacao.setText(String.valueOf(Values.pontuacao)+" Vermelhos");
                 }
                break;
                
            case 3:
                if(Values.matrizBlocos[linhaMatriz][colunaMatriz] == 0 || Values.matrizBlocos[linhaMatriz][colunaMatriz] == 1){            
                    botaoClicado.setIcon(new DataImage().getICONE_EXPLOSAO());
                    Values.reproduzirSom(telaReferente, DataSound.getSOM_EXPLOSAO());
                    Values.gameOver(telaReferente);
                }else if(Values.matrizBlocos[linhaMatriz][colunaMatriz] == 5){
                    botaoClicado.setIcon(new DataImage().getBANDEIRA_VERDE());
                    Values.reproduzirSom(telaReferente, DataSound.getSOM_EXTRA());
                    Values.pontuacao *= 25;
                    Values.gameOver(telaReferente);
                }else{
                    Values.pontuacao+= 1;
                    Values.reproduzirSom(telaReferente, DataSound.getSOM_PONTO());            
                    botaoClicado.setIcon(new DataImage().getBANDEIRA_VERMELHA());
                    botaoClicado.setEnabled(false);
                    lbPontuacao.setText(String.valueOf(Values.pontuacao)+" Vermelhos");
                 }
                break;
            default:
                Values.reproduzirSom(telaReferente, DataSound.getSOM_ERRO());
                JOptionPane.showMessageDialog(telaReferente, "A variavel nivelDificuldade está com um valor fora do padrão"
                        + "a seleção dos blocos não pode continuar!", "Erro!", JOptionPane.ERROR_MESSAGE);
                break;
        }
    }
    
     /**
     * @param telaReferente
     * @param lbPontuacao
     * @param botaoClicado
     * @param linhaMatriz
     * @param colunaMatriz
     * 
     * O metodo "modoPaciencia" é o metodo responsavel por monitorar os eventos dos botões e da matriz onde estão
     * armazenados as informações da localização das <b>minas</b>, <b>pontos</b> e <b>extras</b>, este metodo
     * sempre será chamado quando a modalidade <b>danos</b> do jogo for selecionada antes de iniciar a partida.
     * Este metodo recebe 5 parametros herdados da <b>tela referente</b> e não retorna nada.
     */
    public static void modoDamage(Component telaReferente, JLabel lbPontuacao, JButton botaoClicado, byte linhaMatriz, byte colunaMatriz){
        
        byte[] blocosClicados = new byte[3];
        
        switch(Values.nivelDificuldade){            
            case 1:                               
                    if(Values.matrizBlocos[linhaMatriz][colunaMatriz] == 0){            
                        Values.reproduzirSom(telaReferente, DataSound.getSOM_EXPLOSAO());
                        botaoClicado.setIcon(new DataImage().getICONE_EXPLOSAO());
                        botaoClicado.setEnabled(false);
                        Values.pontuacao -= 20;            
                        blocosClicados[0]++;
                        lbPontuacao.setText(String.valueOf(Values.pontuacao)+" Pontos");
                        lbPontuacao.setForeground(new Color(255, 0, 0));                        
                    }else{
                        Values.pontuacao+= Values.matrizBlocos[linhaMatriz][colunaMatriz];
                        Values.reproduzirSom(telaReferente, DataSound.getSOM_PONTO());                                    
                        botaoClicado.setText(String.valueOf(Values.matrizBlocos[linhaMatriz][colunaMatriz]));
                        botaoClicado.setEnabled(false);
                        lbPontuacao.setText(String.valueOf(Values.pontuacao)+" Pontos");
                        lbPontuacao.setForeground(new Color(50, 255, 0));
                        blocosClicados[0]++;
                    }                
                    
                    if(Values.pontuacao <= 0 || blocosClicados[0] == 36){
                            Values.gameOver(telaReferente);
                        }
                break;
                
            case 2:
                if(Values.matrizBlocos[linhaMatriz][colunaMatriz] == 0){            
                        Values.reproduzirSom(telaReferente, DataSound.getSOM_EXPLOSAO());
                        botaoClicado.setIcon(new DataImage().getICONE_EXPLOSAO());
                        botaoClicado.setEnabled(false);
                        Values.pontuacao -= 30;                
                        blocosClicados[1]++;
                        lbPontuacao.setText(String.valueOf(Values.pontuacao)+" Pontos");
                        lbPontuacao.setForeground(new Color(255, 0, 0));                        
                    }else if(Values.matrizBlocos[linhaMatriz][colunaMatriz] == 5){
                        Values.pontuacao+= 20;
                        blocosClicados[1]++;
                        Values.reproduzirSom(telaReferente, DataSound.getSOM_EXTRA());            
                        botaoClicado.setIcon(new DataImage().get$20PONTOS());
                        botaoClicado.setEnabled(false);
                        lbPontuacao.setText(String.valueOf(Values.pontuacao)+" Pontos");
                        lbPontuacao.setForeground(new Color(50, 255, 0));
                    }else{
                        Values.pontuacao+= Values.matrizBlocos[linhaMatriz][colunaMatriz];
                        blocosClicados[1]++;
                        Values.reproduzirSom(telaReferente, DataSound.getSOM_PONTO());      
                        botaoClicado.setText(String.valueOf(Values.matrizBlocos[linhaMatriz][colunaMatriz]));
                        botaoClicado.setEnabled(false);
                        lbPontuacao.setText(String.valueOf(Values.pontuacao)+" Pontos");
                        lbPontuacao.setForeground(new Color(50, 255, 0));
                    }
                
                    if(Values.pontuacao <= 0 || blocosClicados[1] == 70){
                            Values.gameOver(telaReferente);
                        }                    
                break;
                
            case 3:
                if(Values.matrizBlocos[linhaMatriz][colunaMatriz] == 0 ||
                    Values.matrizBlocos[linhaMatriz][colunaMatriz] == 1 ||
                    Values.matrizBlocos[linhaMatriz][colunaMatriz] == 2
                    ){            
                        Values.reproduzirSom(telaReferente, DataSound.getSOM_EXPLOSAO());
                        botaoClicado.setIcon(new DataImage().getICONE_EXPLOSAO());
                        botaoClicado.setEnabled(false);
                        Values.pontuacao -= 50;             
                        blocosClicados[2]++;
                        lbPontuacao.setText(String.valueOf(Values.pontuacao)+" Pontos");
                        lbPontuacao.setForeground(new Color(255, 0, 0));                        
                    }else if(Values.matrizBlocos[linhaMatriz][colunaMatriz] == 3 || Values.matrizBlocos[linhaMatriz][colunaMatriz] == 4){
                        Values.pontuacao+= 10;
                        blocosClicados[2]++;
                        Values.reproduzirSom(telaReferente, DataSound.getSOM_EXTRA());            
                        botaoClicado.setIcon(new DataImage().get$10PONTOS());
                        botaoClicado.setEnabled(false);
                        lbPontuacao.setText(String.valueOf(Values.pontuacao)+" Pontos");
                        lbPontuacao.setForeground(new Color(50, 255, 0));
                    }else if(Values.matrizBlocos[linhaMatriz][colunaMatriz] == 5){
                        Values.pontuacao+= 15;
                        blocosClicados[2]++;
                        Values.reproduzirSom(telaReferente, DataSound.getSOM_EXTRA());            
                        botaoClicado.setIcon(new DataImage().get$15PONTOS());
                        botaoClicado.setEnabled(false);
                        lbPontuacao.setText(String.valueOf(Values.pontuacao)+" Pontos");
                        lbPontuacao.setForeground(new Color(50, 255, 0));
                    }else if(Values.matrizBlocos[linhaMatriz][colunaMatriz] == 6){
                        Values.pontuacao+= 20;
                        blocosClicados[2]++;
                        Values.reproduzirSom(telaReferente, DataSound.getSOM_EXTRA());            
                        botaoClicado.setIcon(new DataImage().get$20PONTOS());
                        botaoClicado.setEnabled(false);
                        lbPontuacao.setText(String.valueOf(Values.pontuacao)+" Pontos");
                        lbPontuacao.setForeground(new Color(50, 255, 0));
                    }else if(Values.matrizBlocos[linhaMatriz][colunaMatriz] == 7){
                        Values.pontuacao+= 25;
                        blocosClicados[2]++;
                        Values.reproduzirSom(telaReferente, DataSound.getSOM_EXTRA());            
                        botaoClicado.setIcon(new DataImage().get$25PONTOS());
                        botaoClicado.setEnabled(false);
                        lbPontuacao.setText(String.valueOf(Values.pontuacao)+" Pontos");
                        lbPontuacao.setForeground(new Color(50, 255, 0));
                    }else if(Values.matrizBlocos[linhaMatriz][colunaMatriz] == 8){
                        Values.pontuacao+= 30;
                        blocosClicados[2]++;
                        Values.reproduzirSom(telaReferente, DataSound.getSOM_EXTRA());            
                        botaoClicado.setIcon(new DataImage().get$30PONTOS());
                        botaoClicado.setEnabled(false);
                        lbPontuacao.setText(String.valueOf(Values.pontuacao)+" Pontos");
                        lbPontuacao.setForeground(new Color(50, 255, 0));
                    }else{
                        Values.pontuacao+= 50;
                        blocosClicados[2]++;
                        Values.reproduzirSom(telaReferente, DataSound.getSOM_EXTRA());            
                        botaoClicado.setIcon(new DataImage().get$50PONTOS());
                        botaoClicado.setEnabled(false);
                        lbPontuacao.setText(String.valueOf(Values.pontuacao)+" Pontos");
                        lbPontuacao.setForeground(new Color(50, 255, 0));
                    }
                
                    if(Values.pontuacao <= 0 || blocosClicados[2] == 205){
                            Values.gameOver(telaReferente);
                        }
                break;
                
            default:
                Values.reproduzirSom(telaReferente, DataSound.getSOM_ERRO());
                JOptionPane.showMessageDialog(telaReferente, "A variavel nivelDificuldade está com um valor fora do padrão"
                        + "a seleção dos blocos não pode continuar!", "Erro!", JOptionPane.ERROR_MESSAGE);
                break;
        }
    }
}
