package CampoMinado.Classes;

import CampoMinado.Classes.Dao.ScoresDAO;
import CampoMinado.Classes.Tabelas.TabelaScores;
import CampoMinado.Telas.*;
import java.applet.*;
import java.awt.*;
import java.io.IOException;
import java.net.*;
import java.util.*;
import javax.swing.*;

/**
 * @author Tarcísio de Lima
 * @version 1.0.2
 * 
 * Classe <b>Values</b> é responsavel por armazenar metodos e valores temporários que serão usado frequentementes
 * na aplicação além de garantir a passagem de valores de uma tela para outra quando necessário.
 */
public class Values {
    //Atributos e Objetos globais
    public static byte nivelDificuldade = 1, avatarJogador = 1, modalidadeJogo = 1;    
    public static Color corBlocos = new Color(200, 200, 200);
    public static byte[][] matrizBlocos = new byte[14][14];          
    public static int pontuacao = 0;   
    
    //Metodos não Staticos
    
    /**
     * 
     * @param urlIcone
     * @return imagemTitulo ou nulo
     * 
     * O Metodo "iconeJanela" é responsavel por carregar o icone da imagem na barra de titulos da aplicação, ela recebe
     * como parametros uma <b>String</b> que é o caminho da imagem e retorna a imagem para a janela caso tenha
     * sucesso ou nulo caso gere alguma exeção.
     */
    public Image  iconeJanela(String urlIcone){
        try{
            URL urlImagemTitulo = this.getClass().getResource(urlIcone);
            Image imagemTitulo = Toolkit.getDefaultToolkit().getImage(urlImagemTitulo);
            return imagemTitulo;
        }catch(Exception erro){
            reproduzirSom(null, DataSound.getSOM_ERRO());
            JOptionPane.showMessageDialog(null, "Um erro inesperado ocorreu, não foi possivel carregar o icone da janela "
                    + "assinatura do erro:\n"+erro, "Erro Fatal!", JOptionPane.ERROR_MESSAGE);
            return null;
        }
    }
    
    /**
     * @param telaReferente 
     * @param componenteIcone 
     * 
     * O metodo iconeAvatarJogador é responsavel por exibir a foto do avatar selecionado pelo jogador na tela de
     * modalidades na tela do jogo na painel de informações, este metodo não recebe nada como parametro e retorna
     * um objeto do tipo Image
     */
    public void  iconeAvatarJogador(Component telaReferente, JLabel componenteIcone){
        switch(avatarJogador){
            case 1:
                componenteIcone.setIcon(new DataImage().getAVATAR_1());                
                break;
                
            case 2:
                componenteIcone.setIcon(new DataImage().getAVATAR_2());
                break;
                
            case 3:
                componenteIcone.setIcon(new DataImage().getAVATAR_3());
                break;
                
            case 4:
                componenteIcone.setIcon(new DataImage().getAVATAR_4());
                break;
                
            case 5:
                componenteIcone.setIcon(new DataImage().getAVATAR_5());
                break;
                
            case 6:
                componenteIcone.setIcon(new DataImage().getAVATAR_6());
                break;
                
            case 7:
                componenteIcone.setIcon(new DataImage().getAVATAR_8());
                break;
                
            case 8:
                componenteIcone.setIcon(new DataImage().getAVATAR_9());
                break;
                
            case 9:
                componenteIcone.setIcon(new DataImage().getAVATAR_7());
                break;
                
            case 10:
                componenteIcone.setIcon(new DataImage().getAVATAR_11());
                break;
                
            case 11:
                componenteIcone.setIcon(new DataImage().getAVATAR_12());
                break;
                
            case 12:
                componenteIcone.setIcon(new DataImage().getAVATAR_10());
                break;
                
            default:
                reproduzirSom(null, DataSound.getSOM_ERRO());
                JOptionPane.showMessageDialog(null, "Erro ao tentar exibir o seu avatar no painel de informações\n"
                        ,"Erro!", JOptionPane.ERROR_MESSAGE);
                break;
        }        
    }
    
    //Metodos Estáticos    
    /**
     * Este metodo é responsavel por verificar qual é o nivel de dificuldade selecionado pelo usuário, ele não recebe
     * paramentros nem retorna nada, apenas verifica através da variavel global <b>nivelDificuldade</b> qual o
     * valor que está armazenado nela e inicia a partida de acordo com o valor do nivel dela.     * 
     */
    public static void iniciarJogo(){
        switch(nivelDificuldade){
            case 1:
                new TelaJogoFacil().setVisible(true);
                break;
            case 2:
                new TelaJogoNormal().setVisible(true);
                break;
            case 3:
                new TelaJogoDificil().setVisible(true);
                break;
            default:
                reproduzirSom(null, DataSound.getSOM_ERRO());
                JOptionPane.showMessageDialog(null, "Não foi possível iniciar o jogo pois o nivel não foi selecionado seu valor é nulo!\n"
                        + "volte para o \"Menu Principal\" e selecione um nível de dificuldade!","Erro!", JOptionPane.ERROR_MESSAGE);
                new TelaMenuPrincipal().setVisible(true);
                break;
        }
        
        preencherCampoMinado();
        
    }
    
    /**
     * @param telaReferente
     * @return descisao
     * 
     * O metodo "voltarMenuPrincipal" é responsavel por voltar para o menu principal do aplicativo ao clicar no botão de
     * <b>Abandonar</b> de umas das telas do Campo minado, esse metodo recebe como parametro um Objetod do
     * tipo Componente que por sua vez é a "Tela Referente" à qual o JOptionPane deve aparecer quando for chamado
     * o dialogo de confirmação da saída, esse metodo também não retorna nada
     * 
     */
    public static short voltarMenuPrincipal(Component telaReferente){
        reproduzirSom(telaReferente, DataSound.getSOM_QUESTAO());
        byte descisao = (byte) JOptionPane.showConfirmDialog(telaReferente, "Você realmente deseja abandonar a partida e "
                + "retornar para o menu principal?", "Aviso!", JOptionPane.YES_NO_OPTION);
        
        switch(descisao){
            case 0:
                new TelaMenuPrincipal().setVisible(true);                
                break;
            case 1:
                /*Não faz nada*/
                break;
            default:
                reproduzirSom(telaReferente, DataSound.getSOM_ERRO());
                JOptionPane.showMessageDialog(telaReferente, "Erro ao tentar voltar para o \"Menu Principal\" o valor recebido\n"
                        + "pela variavel short não está dentro da condição!","Erro!",JOptionPane.ERROR_MESSAGE);
                break;
        }
        return descisao;
    }
    
    /**
     * @param numeroCor 
     * 
     * O Metodo "mudarCorBlocos" tem como principal função mudar a cor dos blocos do Campo Minado, fazendo eles
     * assumirem cores diferentes dependendo da opção selecionada no <b>spinnerCorBloco</b> na tela de opções
     * do aplicativo ele recebe como parametro um valor do tipo byte para seleção da cor e não retorna valor nenhum
     */    
    public static void mudarCorBlocos(byte numeroCor){
        switch(numeroCor){
            case 0:
                corBlocos = new Color(200, 200, 200);
                break;
            case 1:
                corBlocos = new Color(255, 50, 255);
                break;
            case 2:
                corBlocos = new Color(255, 0, 0);
                break;
            case 3:
                corBlocos = new Color(255, 100, 0);
                break;
            case 4:
                corBlocos = new Color(255, 205, 0);
                break;
            case 5:
                corBlocos = new Color(50, 255, 0);
                break;
            case 6:
                corBlocos = new Color(0, 150, 255);
                break;
            case 7:
                corBlocos = new Color(100, 0, 255);
                break;
            case 8:
                corBlocos = new Color(255, 255, 255);
                break;
            case 9:
                corBlocos = new Color(100, 100, 100);
                break;
            default:
                corBlocos = new Color(200, 200, 200);
                break;
        }        
    }
    
    /** 
     * O metodo "preencherCampoMinado" tem como função preencher uma matriz do tipo <b>byte</b> para depois
     * ser usada na interface do Campo Minado, ela não recebe nada como parametro nem retorna valor nenhum
     */
    public static void preencherCampoMinado(){
        
        Random numAleatorio = new Random();
        
        switch(nivelDificuldade){
            case 1:
                for(int linha = 0; linha < 6; linha++){
                    for(int coluna = 0; coluna <6; coluna++){
                        matrizBlocos[coluna][linha] = (byte) (0 + numAleatorio.nextInt(6 ));
                    }
                }
                break;
            case 2:
                for(int linha = 0; linha < 10; linha++){
                    for(int coluna = 0; coluna <10; coluna++){
                        matrizBlocos[coluna][linha] = (byte) (0 + numAleatorio.nextInt(6));
                    }
                }
                break;
            case 3:
                for(int linha = 0; linha < 14; linha++){
                    for(int coluna = 0; coluna <14; coluna++){
                        matrizBlocos[coluna][linha] = (byte) (0 + numAleatorio.nextInt(10));
                    }
                }
                break;
            default:
                for(int linha = 0; linha < 14; linha++){
                    for(int coluna = 0; coluna < 14; coluna++){
                        matrizBlocos[coluna][linha] = (byte) (0 + numAleatorio.nextInt(6));
                    }
                }
                break;
        }
    }
    
    /**
     * 
     * @param telaReferente
     * @param lbPontuacao
     * @param botaoClicado
     * @param linhaMatriz
     * @param colunaMatriz 
     * 
     * O Metodo "clickBloco" corresponde a ação quando um botão é clicado na tela do jogador, este metodo diz se o jogador
     * caiu em uma mina ou caiu em uma casa premiada com pontos aleatórios, este metodo recebe como parametro
     * a tela que está aberta, um rotulo que marca os pontos acumulados, o botão que foi clicaco pelo jogador e dois valores
     * do tipo <b>byte<b/> um representa a linha na matriz e o outro a coluna da matriz que está com os valores armazenados     
     */    
    public static void clickBloco(Component telaReferente, JLabel lbPontuacao, JButton botaoClicado, byte linhaMatriz, byte colunaMatriz){
        switch(modalidadeJogo){
            case 1:
                BlocoEventos.modoPaciencia(telaReferente, lbPontuacao, botaoClicado, linhaMatriz, colunaMatriz);
                break;
                
            case 2:
                BlocoEventos.modoCaptureFlag(telaReferente, lbPontuacao, botaoClicado, linhaMatriz, colunaMatriz);
                break;
                
            case 3:
                BlocoEventos.modoDamage(telaReferente, lbPontuacao, botaoClicado, linhaMatriz, colunaMatriz);
                break;
                
            default:
                reproduzirSom(telaReferente, DataSound.getSOM_ERRO());
                JOptionPane.showMessageDialog(telaReferente, "Nenhum valor foi passado para a variavel modalidadeJogo"
                        + "ou o valor da mesma é inválido!", "Erro!", JOptionPane.ERROR_MESSAGE);
                break;
        }
    }
    
    /**
     * O Metodo "recomecarPartida" é responsavel por reiniciar a ultima partida jogada pelo jogador, zerando sua pontuação
     * total e limpando o campo minado preenchendo ele com novos valores, este metodo não recebe nada como parametro
     * e não retorna nada
     */
    public static void recomecarPartida(){
        iniciarJogo();
        pontuacao = 0;
    }
    
    /**
     * @param telaReferente 
     * @param urlArquivoSom 
     * O Metodo "reproduzirSom" é responsavel por executar os <b>Sons</b> dos eventos do jogo, este metodo recebe
     * dois parametros o 1º é a referencia à tela à qual o Dialogo se caso algum erro ocorrer aparecer e o segundo parametro
     * é uma String com a Localização do arquivo de som.
     */
    public static void reproduzirSom(Component telaReferente, String urlArquivoSom){
       try { 
            URL urlArquivo = Class.class.getClass().getResource(urlArquivoSom);
            AudioClip som = Applet.newAudioClip(urlArquivo); 
            som.play();             
        } catch (Exception erro) { 
             JOptionPane.showMessageDialog(telaReferente, "Ocorreu um erro ao tentar reproduzir um som do aplicativo,\n"
                     + "assinatura do erro:\n"+erro, "Erro!", JOptionPane.ERROR_MESSAGE);             
        }        
    }    
    
    /**
     * @param telaReferente 
     *  O metodo "gameOver" tem como principal função inserir verificar os dados para inserção do record do jogador quando
     * ele perder o jogo, este metodo recebe como parametro um objeto do tipo "Component" que corresponde a tela
     * na qual o Dialogo deverá aparecer para efetuar as entradas de dados dos dialogos.
     */
    public static void gameOver(Component telaReferente){        
        /*Verifica Nick Name*/
        if(TabelaScores.getNickJogador().length() < 20 && TabelaScores.getNickJogador().length() > 0){
            
            byte descisao = (byte)JOptionPane.showConfirmDialog(telaReferente, "<html><h1>Game Over!!!</h1></html>\n"
                    + "Você deseja repetir esta partida?","Game Over!!",JOptionPane.YES_NO_OPTION);
            
            switch(descisao){
                case 0:
                    //Reiniciando a Partida
                    telaReferente.setVisible(false);
                    recomecarPartida();
                    break;
                case 1:
                    //Pegando e verificando dificuldade
                    switch(nivelDificuldade){
                        case 1:
                            pontuacao += 0;
                            TabelaScores.setNivelDificuldade("Facil");
                            break;
                        case 2:
                            pontuacao += 10;
                            TabelaScores.setNivelDificuldade("Normal");
                            break;
                        case 3:
                            pontuacao += 20;
                            TabelaScores.setNivelDificuldade("Dificil");
                            break;
                        default:
                            TabelaScores.setNivelDificuldade("Facil");
                            break;
                    }

                    //Pegando modalidade
                    switch(modalidadeJogo){
                        case 1:                
                            TabelaScores.setModalidadeJogo("Paciencia");
                            break;
                        case 2:                    
                            TabelaScores.setModalidadeJogo("Capture Flag");
                            break;
                        case 3:                    
                            TabelaScores.setModalidadeJogo("Dano Critico");
                            break;
                        default:
                            TabelaScores.setNivelDificuldade("Paciencia");
                            break;
                    }

                    //Pegando pontuação Total
                    if(Values.pontuacao < 0){
                        TabelaScores.setScoreJogador(0);
                    }else{
                        TabelaScores.setScoreJogador(pontuacao);
                    }

                    //Pegando data e hora atual
                    int[] vetorHoraData = new int[3];
                    Calendar calendarioAtual = Calendar.getInstance();

                    vetorHoraData[0] = calendarioAtual.get(Calendar.DAY_OF_MONTH);
                    vetorHoraData[1] = calendarioAtual.get(Calendar.MONTH)+1;
                    vetorHoraData[2] = calendarioAtual.get(Calendar.YEAR);

                    TabelaScores.setDataJogo(vetorHoraData[0]+"/"+vetorHoraData[1]+"/"+vetorHoraData[2]);

                    vetorHoraData[0] = calendarioAtual.get(Calendar.HOUR_OF_DAY);
                    vetorHoraData[1] = calendarioAtual.get(Calendar.MINUTE);
                    vetorHoraData[2] = calendarioAtual.get(Calendar.SECOND);

                    TabelaScores.setHoraJogo(vetorHoraData[0]+":"+vetorHoraData[1]+" "+vetorHoraData[2]);

                    //Salvando no banco
                    ScoresDAO.inserirRecord(TabelaScores.getScoreJogador(), TabelaScores.getNickJogador(),
                            TabelaScores.getNivelDificuldade(), TabelaScores.getModalidadeJogo(), TabelaScores.getDataJogo(),
                            TabelaScores.getHoraJogo());            

                    //Zerando os Valores
                    pontuacao = 0;

                    //Chamando Tela de Scores
                    new TelaScores(null, true).setVisible(true);            
                    telaReferente.setVisible(false);
                    break;
                default:
                    
                    break;
            }
            
        }else{
            reproduzirSom(telaReferente, DataSound.getSOM_QUESTAO());
            JOptionPane.showMessageDialog(telaReferente, "O seu \"Nick\" é muito grande, tente novamente e ponha"
                    + "um \"Nick Name\" menor","Aviso!", JOptionPane.WARNING_MESSAGE);
            gameOver(telaReferente);
        }
    }
    
    /**
     * @param telaReferente 
     * @param url 
     * O metodo <b>irURL</b> abre o link de um site que está armazenada no parametro, quando o botão é clicado
     * durante o evento do botão é passada uma "String" que define a URL da qual o metodo "irURL" deve caso 
     * ocorra uma exeção de Input e Output (IO) será exibida uma mensagem de erro para o usuário, caso ocorra
     * uma exeção do tipo URiSintax (Sintaxe de URL) será também exibida uma mensagem de erro para o usuário,
     * outro caso é que antes de excutar o comando para ir para URL ele verifica se o campo de texto do visor não está
     * vazio, se o mesmo estiver ele informa ao usuário com um Dialogo dizendo que não nenhuma URL no visor.
     * Detalhe, este metodo abre o navegador padrão do sistema operacional, tanto no Linux, Mac e Windows
     */
    public static void irURL(Component telaReferente, String url){
             try {
                Desktop.getDesktop().browse(new URI(url));
            } catch (URISyntaxException | IOException ex) {
                Values.reproduzirSom(telaReferente, DataSound.getSOM_ERRO());
                JOptionPane.showMessageDialog(telaReferente, "O local informado não pode ser acessado, URL deve estar\n"
                        + "incorreta verifique os parametros, assinatura do erro:\n","Erro!"+ex, JOptionPane.ERROR_MESSAGE);                
            }
    }
}