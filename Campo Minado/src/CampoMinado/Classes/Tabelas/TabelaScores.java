package CampoMinado.Classes.Tabelas;

/**
 * @author Tarcísio de Lima
 * @version 1.0.0
 * 
 * Classe corresponde a tabela de scores do jogo do banco de dados, é responsavel pelo intermédio entre o java e o
 * SQLite
 */
public class TabelaScores {
    private static int id, scoreJogador;
    private static String nickJogador, nivelDificuldade, modalidadeJogo, dataJogo, horaJogo;

    //Getters e Setters
    public static String getDataJogo() {
        return dataJogo;
    }

    public static void setDataJogo(String dataJogo) {
        TabelaScores.dataJogo = dataJogo;
    }

    public static int getId() {
        return id;
    }

    public static void setId(int id) {
        TabelaScores.id = id;
    }

    public static String getNickJogador() {
        return nickJogador;
    }

    public static void setNickJogador(String nickJogador) {
        TabelaScores.nickJogador = nickJogador;
    }

    public static String getNivelDificuldade() {
        return nivelDificuldade;
    }

    public static void setNivelDificuldade(String nivelDificuldade) {
        TabelaScores.nivelDificuldade = nivelDificuldade;
    }

    public static String getModalidadeJogo() {
        return modalidadeJogo;
    }

    public static void setModalidadeJogo(String modalidadeJogo) {
        TabelaScores.modalidadeJogo = modalidadeJogo;
    }
    
    public static int getScoreJogador() {
        return scoreJogador;
    }

    public static void setScoreJogador(int scoreJogador) {
        TabelaScores.scoreJogador = scoreJogador;
    }
    
    public static String getHoraJogo() {
        return horaJogo;
    }

    public static void setHoraJogo(String horaJogo) {
        TabelaScores.horaJogo = horaJogo;
    }

}
