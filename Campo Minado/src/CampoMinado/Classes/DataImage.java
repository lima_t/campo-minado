package CampoMinado.Classes;

import javax.swing.Icon;

/**
 * @author Tarcísio de Lima
 * @version 1.0.0
 * 
 * Classe DataImage é responsavel por armazenar varivaeis constantes com a localização das imagens do projeto
 */
public class DataImage {
    //Strings Constantes - Icones das telas
    public static final String ICONE_JOGO = "/CampoMinado/Imagens/iconeJogo.png";
    public static final String ICONE_NOVO_JOGO = "/CampoMinado/Imagens/iconeNovoJogo.png";
    public static final String ICONE_OPCOES = "/CampoMinado/Imagens/iconeOpcoes.png";
    public static final String ICONE_SCORES = "/CampoMinado/Imagens/iconeScore.png";
    public static final String ICONE_SOBRE = "/CampoMinado/Imagens/iconeAjuda.png";
    public static final String ICONE_MODALIDADES = "/CampoMinado/Imagens/iconeModalidade.png";
    
   //Icones Constantes - Botões
     private final Icon AVATAR_1 = new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar01.jpg"));    
     private final Icon AVATAR_2 = new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar02.jpg"));
     private final Icon AVATAR_3 = new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar03.jpg"));
     private final Icon AVATAR_4 = new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar04.jpg"));
     private final Icon AVATAR_5 = new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar05.jpg"));
     private final Icon AVATAR_6 = new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar06.jpg"));
     private final Icon AVATAR_7 = new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar07.jpg"));
     private final Icon AVATAR_8 = new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar08.jpg"));
     private final Icon AVATAR_9 = new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar09.jpg"));
     private final Icon AVATAR_10 = new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar10.jpg"));
     private final Icon AVATAR_11 = new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar11.jpg"));
     private final Icon AVATAR_12 = new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar12.jpg"));
         
    //Icones Constantes - Botões    
    private final Icon ICONE_EXPLOSAO = new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/explosao40x40.png"));
    private final Icon BANDEIRA_VERDE = new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/flagVerde.png"));
    private final Icon BANDEIRA_VERMELHA = new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/flagVermelho.png"));
    private final Icon $10PONTOS = new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/$10pontos.png"));
    private final Icon $15PONTOS = new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/$15pontos.png"));
    private final Icon $20PONTOS = new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/$20pontos.png"));
    private final Icon $25PONTOS = new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/$25pontos.png"));
    private final Icon $30PONTOS = new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/$30pontos.png"));
    private final Icon $50PONTOS = new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/$50pontos.png"));

    //Getters
    public Icon getBANDEIRA_VERDE() {
        return BANDEIRA_VERDE;
    }

    public Icon getBANDEIRA_VERMELHA() {
        return BANDEIRA_VERMELHA;
    }

    public Icon get$10PONTOS() {
        return $10PONTOS;
    }

    public Icon get$15PONTOS() {
        return $15PONTOS;
    }

    public Icon get$20PONTOS() {
        return $20PONTOS;
    }

    public Icon get$25PONTOS() {
        return $25PONTOS;
    }

    public Icon get$30PONTOS() {
        return $30PONTOS;
    }

    public Icon get$50PONTOS() {
        return $50PONTOS;
    }    

    public Icon getAVATAR_1() {
        return AVATAR_1;
    }

    public Icon getAVATAR_10() {
        return AVATAR_10;
    }

    public Icon getAVATAR_11() {
        return AVATAR_11;
    }

    public Icon getAVATAR_12() {
        return AVATAR_12;
    }

    public Icon getAVATAR_2() {
        return AVATAR_2;
    }

    public Icon getAVATAR_3() {
        return AVATAR_3;
    }

    public Icon getAVATAR_4() {
        return AVATAR_4;
    }

    public Icon getAVATAR_5() {
        return AVATAR_5;
    }

    public Icon getAVATAR_6() {
        return AVATAR_6;
    }

    public Icon getAVATAR_7() {
        return AVATAR_7;
    }

    public Icon getAVATAR_8() {
        return AVATAR_8;
    }

    public Icon getAVATAR_9() {
        return AVATAR_9;
    }

    public Icon getICONE_EXPLOSAO() {
        return ICONE_EXPLOSAO;
    }

}
