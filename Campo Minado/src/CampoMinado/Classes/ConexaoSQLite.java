
package CampoMinado.Classes;

import java.awt.Component;
import java.sql.*;
import javax.swing.JOptionPane;

/**
 * @author Tarcísio de Lima
 * @version 1.0.0
 * 
 * Classe responsavel pela conexão com o banco de dados SQLite
 */
public class ConexaoSQLite {
    
    public static Statement comando;
    public static Connection conexaoBanco;
    
    private static final String nomeBanco = "MineSweeperBD.db";
        
    /**
     * @param telaReferente 
     * 
     * O metodo "conectarBancoDados" é responsavel por fazer a conexão com o banco do SQLite através do driver
     * <b>SQLite-JDBC</b>, este metoo recebe como parametro um objeto do tipo <b>Component</b> que corresponde
     * a Tela na qual o Dialogo deverá aparecer caso uma exeção seja gerada durante a conexão
     */
    public static void conectarBancoDados(Component telaReferente){
        try {
            
            Class.forName("org.sqlite.JDBC");
            conexaoBanco = DriverManager.getConnection("jdbc:sqlite:"+nomeBanco);
            comando = conexaoBanco.createStatement();
            
        } catch (ClassNotFoundException | SQLException  erroBd) {
            Values.reproduzirSom(telaReferente, DataSound.getSOM_ERRO());
            JOptionPane.showMessageDialog(telaReferente, "Não foi possivel conectar-se ao banco, assinatura do erro:\n"+
                    erroBd, "Erro!", JOptionPane.ERROR_MESSAGE);            
        }
        
    }
        
    /**
     * @param telaReferente 
     * 
     * O metodo "criarBancoDados" é responsavel por criar a pequena base de dados que será responsavel por armazenar
     * os valores dos Scores dos jogadores.
     */
    public static void criarBancoDados(Component telaReferente){
        try {
            comando = conexaoBanco.createStatement();
            
            //Cria tabela de records
            comando.execute("CREATE TABLE IF NOT EXISTS tb_records("
                    +"id INTEGER NOT NULL,"
                    +"nick_jogador VARCHAR(20) NOT NULL,"
                    +"score_jogador INT NOT NULL,"
                    +"nivel_dificuldade VARCHAR(10) NOT NULL,"
                    +"modalidade VARCHAR(20) NOT NULL,"
                    +"data_jogo VARCHAR(10) NOT NULL,"
                    +"hora_jogo VARCHAR(10) NOT NULL,"
                    +"PRIMARY KEY(id)"
                    +");"
                    );
        } catch (SQLException erroSQL) {
            Values.reproduzirSom(telaReferente, DataSound.getSOM_ERRO());
            JOptionPane.showMessageDialog(null, "Não foi possivel criar a base de dados, assinatura do erro:\n"+erroSQL,
                    "Erro!", JOptionPane.ERROR_MESSAGE);
        }finally{            
                  desconectarBancoDados(telaReferente);
        }        
    }
    
    /**
     * @param telaReferente 
     * 
     * O metodo "desconectarBancoDados" é responsavel por fechar qualquer conexão estabelecida com o banco de dados
     * da aplicação, este metodo recebe como parametro um objetod do tipo <b>Component</b> que corresponde à tela
     * para o qual o Diaologo de erro deverá ser estourado caso algum exeção ocorra.
     */
    public static void desconectarBancoDados(Component telaReferente){
        try {
            conexaoBanco.close();
        } catch (SQLException erroSQL) {
            Values.reproduzirSom(telaReferente, DataSound.getSOM_ERRO());
            JOptionPane.showMessageDialog(null, "Não foi possivel criar a base de dados, assinatura do erro:\n"+erroSQL,
                    "Erro!", JOptionPane.ERROR_MESSAGE);
        }
    }
}
