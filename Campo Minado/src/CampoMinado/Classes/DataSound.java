package CampoMinado.Classes;

/**
 * @author Tarcísio de Lima
 * @version 1.0.0
 * 
 * Classe Estática para armazenar valores Constantes dos sons do projeto
 */
public class DataSound {
    private static final String SOM_CLICK = "/CampoMinado/Sons/click.wav";
    private static final String SOM_EXPLOSAO = "/CampoMinado/Sons/explosao.wav";
    private static final String SOM_ERRO = "/CampoMinado/Sons/erro.wav";
    private static final String SOM_EXTRA = "/CampoMinado/Sons/extra.wav";
    private static final String SOM_PONTO = "/CampoMinado/Sons/ponto.wav";
    private static final String SOM_QUESTAO = "/CampoMinado/Sons/questao.wav";

    //Getters dos Sons do jogo
    public static String getSOM_CLICK() {
        return SOM_CLICK;
    }

    public static String getSOM_ERRO() {
        return SOM_ERRO;
    }

    public static String getSOM_EXPLOSAO() {
        return SOM_EXPLOSAO;
    }

    public static String getSOM_EXTRA() {
        return SOM_EXTRA;
    }

    public static String getSOM_PONTO() {
        return SOM_PONTO;
    }

    public static String getSOM_QUESTAO() {
        return SOM_QUESTAO;
    }
}
