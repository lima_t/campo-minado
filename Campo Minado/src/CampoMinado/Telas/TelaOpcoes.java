
package CampoMinado.Telas;

import CampoMinado.Classes.Dao.ScoresDAO;
import CampoMinado.Classes.*;
import javax.swing.JOptionPane;

/**
 * @author Tarcísio de Lima
 * @version 1.0.0
 * 
 * A Classe "TelaOpções" é a classe responsavel pela tela de opções do jogo
 */
public class TelaOpcoes extends javax.swing.JDialog {

    public TelaOpcoes(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(parent);
        this.setIconImage(new Values().iconeJanela(DataImage.ICONE_OPCOES));
        
        choiceNivelPadrao();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dificuldadeJogo = new javax.swing.ButtonGroup();
        painelDifuldade = new javax.swing.JPanel();
        choiceNivelFacil = new javax.swing.JRadioButton();
        choiceNivelMedio = new javax.swing.JRadioButton();
        choiceNivelDificil = new javax.swing.JRadioButton();
        iconNivelFacil = new javax.swing.JLabel();
        iconNivelMedio = new javax.swing.JLabel();
        iconNivelDificil = new javax.swing.JLabel();
        painelCorBloco = new javax.swing.JPanel();
        spinnerCorBloco = new javax.swing.JComboBox();
        btSalvarConfig = new javax.swing.JButton();
        painelLimparDados = new javax.swing.JPanel();
        btLimparDados = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Opções do Jogo");
        setResizable(false);

        painelDifuldade.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Nível de Dificuldade", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.ABOVE_TOP, new java.awt.Font("Calibri", 0, 15), new java.awt.Color(255, 0, 0))); // NOI18N

        dificuldadeJogo.add(choiceNivelFacil);
        choiceNivelFacil.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        choiceNivelFacil.setForeground(new java.awt.Color(255, 204, 0));
        choiceNivelFacil.setText("Nível Fácil (06x06)");
        choiceNivelFacil.setToolTipText("O nível fácil do jogo é recomendado para os iniciantes,\\n pois existem poucos blocos e menos minhas");

        dificuldadeJogo.add(choiceNivelMedio);
        choiceNivelMedio.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        choiceNivelMedio.setForeground(new java.awt.Color(255, 102, 0));
        choiceNivelMedio.setText("Nível Médio (10x10)");
        choiceNivelMedio.setToolTipText("O nível intermédiario é recomendado para os jogadores\\n que já estão mais acostumados, tem o nivel normal de \\nnúmero de blocos e minhas para cair");

        dificuldadeJogo.add(choiceNivelDificil);
        choiceNivelDificil.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        choiceNivelDificil.setForeground(new java.awt.Color(255, 0, 0));
        choiceNivelDificil.setText("Nível Difícil (14x14)");
        choiceNivelDificil.setToolTipText("O nível díficil é apropriado para os experts que já sabem jogar bem\\n nesse modo o número de blocos e de minas é extremo\\n um passo em falso e você perderá fácilmente!");

        iconNivelFacil.setIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/nivel_facil.png"))); // NOI18N
        iconNivelFacil.setToolTipText("O nível fácil do jogo é recomendado para os iniciantes,\\n pois existem poucos blocos e menos minhas");

        iconNivelMedio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/nivel_normal.png"))); // NOI18N
        iconNivelMedio.setToolTipText("O nível intermédiario é recomendado para os jogadores\\n que já estão mais acostumados, tem o nivel normal de \\nnúmero de blocos e minhas para cair");

        iconNivelDificil.setIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/nivel_dificil.png"))); // NOI18N
        iconNivelDificil.setToolTipText("O nível díficil é apropriado para os experts que já sabem jogar bem\\n nesse modo o número de blocos e de minas é extremo\\n um passo em falso e você perderá fácilmente!");

        javax.swing.GroupLayout painelDifuldadeLayout = new javax.swing.GroupLayout(painelDifuldade);
        painelDifuldade.setLayout(painelDifuldadeLayout);
        painelDifuldadeLayout.setHorizontalGroup(
            painelDifuldadeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelDifuldadeLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(choiceNivelFacil)
                .addGap(36, 36, 36)
                .addComponent(choiceNivelMedio)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(choiceNivelDificil)
                .addContainerGap())
            .addGroup(painelDifuldadeLayout.createSequentialGroup()
                .addGap(54, 54, 54)
                .addComponent(iconNivelFacil)
                .addGap(118, 118, 118)
                .addComponent(iconNivelMedio)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(iconNivelDificil)
                .addGap(54, 54, 54))
        );
        painelDifuldadeLayout.setVerticalGroup(
            painelDifuldadeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelDifuldadeLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelDifuldadeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(choiceNivelFacil)
                    .addComponent(choiceNivelMedio)
                    .addComponent(choiceNivelDificil))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(painelDifuldadeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(iconNivelMedio)
                    .addComponent(iconNivelDificil)
                    .addComponent(iconNivelFacil))
                .addContainerGap())
        );

        painelCorBloco.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Cor dos Blocos", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.ABOVE_TOP, new java.awt.Font("Calibri", 0, 15), new java.awt.Color(255, 0, 0))); // NOI18N

        spinnerCorBloco.setFont(new java.awt.Font("Calibri", 1, 15)); // NOI18N
        spinnerCorBloco.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Blocos Padrão", "Blocos Rosa", "Blocos Vermelhos", "Blocos Laranjas", "Blocos Amarelos", "Blocos Verdes", "Blocos Azuis", "Blocos Roxos", "Blocos Brancos", "Blocos Pretos" }));
        spinnerCorBloco.setToolTipText("Selecione a cor dos blocos do jogo");

        javax.swing.GroupLayout painelCorBlocoLayout = new javax.swing.GroupLayout(painelCorBloco);
        painelCorBloco.setLayout(painelCorBlocoLayout);
        painelCorBlocoLayout.setHorizontalGroup(
            painelCorBlocoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelCorBlocoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(spinnerCorBloco, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        painelCorBlocoLayout.setVerticalGroup(
            painelCorBlocoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelCorBlocoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(spinnerCorBloco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        btSalvarConfig.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        btSalvarConfig.setForeground(new java.awt.Color(255, 0, 0));
        btSalvarConfig.setText("Salvar e Voltar");
        btSalvarConfig.setToolTipText("Clique aqui para salvar as configurações e voltar para o menu principal");
        btSalvarConfig.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btSalvarConfig.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSalvarConfigActionPerformed(evt);
            }
        });

        painelLimparDados.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Limpar Dados", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.ABOVE_TOP, new java.awt.Font("Calibri", 0, 15), new java.awt.Color(255, 0, 0))); // NOI18N
        painelLimparDados.setForeground(new java.awt.Color(255, 0, 0));
        painelLimparDados.setToolTipText("");
        painelLimparDados.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        painelLimparDados.setFont(new java.awt.Font("CAC Moose", 0, 16)); // NOI18N

        btLimparDados.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        btLimparDados.setForeground(new java.awt.Color(255, 0, 0));
        btLimparDados.setText("Limpar Dados do Jogo");
        btLimparDados.setToolTipText("Limpa todos os dados e histórico de records feitos até então");
        btLimparDados.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btLimparDados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btLimparDadosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout painelLimparDadosLayout = new javax.swing.GroupLayout(painelLimparDados);
        painelLimparDados.setLayout(painelLimparDadosLayout);
        painelLimparDadosLayout.setHorizontalGroup(
            painelLimparDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, painelLimparDadosLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btLimparDados)
                .addGap(23, 23, 23))
        );
        painelLimparDadosLayout.setVerticalGroup(
            painelLimparDadosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelLimparDadosLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btLimparDados, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(painelDifuldade, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(painelCorBloco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(painelLimparDados, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(btSalvarConfig)
                        .addGap(194, 194, 194))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(painelDifuldade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(painelCorBloco, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(painelLimparDados, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 76, Short.MAX_VALUE)
                .addComponent(btSalvarConfig)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btSalvarConfigActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSalvarConfigActionPerformed
        Values.reproduzirSom(TelaOpcoes.this, DataSound.getSOM_CLICK());
        Values.mudarCorBlocos((byte)spinnerCorBloco.getSelectedIndex());
        
        if(choiceNivelFacil.isSelected()){
            Values.nivelDificuldade = 1;            
            dispose();
            
        }else if(choiceNivelMedio.isSelected()){
            Values.nivelDificuldade = 2;
            dispose();
            
        }else if(choiceNivelDificil.isSelected()){
            Values.nivelDificuldade = 3;
            dispose();
            
        }else{
            JOptionPane.showMessageDialog(this, "Nenhum nível de dificuldade foi selecionado, por favor selecionar um nível de dificuldade!",
                    "Aviso!", JOptionPane.PLAIN_MESSAGE);
        }
        
    }//GEN-LAST:event_btSalvarConfigActionPerformed

    private void btLimparDadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btLimparDadosActionPerformed
        Values.reproduzirSom(TelaOpcoes.this, DataSound.getSOM_CLICK());
        ScoresDAO.excluirDados(TelaOpcoes.this);
    }//GEN-LAST:event_btLimparDadosActionPerformed

    private void choiceNivelPadrao(){
        switch(Values.nivelDificuldade){
            case 1:
                choiceNivelFacil.setSelected(true);
                break;
            case 2:
                choiceNivelMedio.setSelected(true);
                break;
            case 3:
                choiceNivelDificil.setSelected(true);
                break;
            default:
                choiceNivelFacil.setSelected(true);
                break;
        }
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btLimparDados;
    private javax.swing.JButton btSalvarConfig;
    private javax.swing.JRadioButton choiceNivelDificil;
    private javax.swing.JRadioButton choiceNivelFacil;
    private javax.swing.JRadioButton choiceNivelMedio;
    private javax.swing.ButtonGroup dificuldadeJogo;
    private javax.swing.JLabel iconNivelDificil;
    private javax.swing.JLabel iconNivelFacil;
    private javax.swing.JLabel iconNivelMedio;
    private javax.swing.JPanel painelCorBloco;
    private javax.swing.JPanel painelDifuldade;
    private javax.swing.JPanel painelLimparDados;
    private javax.swing.JComboBox spinnerCorBloco;
    // End of variables declaration//GEN-END:variables
}
