package CampoMinado.Telas;

import CampoMinado.Classes.*;
import javax.swing.JOptionPane;

/**
 * @author Tarcísio de Lima
 * @version 1.0.0
 */
public class TelaMenuPrincipal extends javax.swing.JFrame {
    
    public TelaMenuPrincipal() {
        //Chamada dos metodos
        initComponents();
        ConexaoSQLite.conectarBancoDados(TelaMenuPrincipal.this);
        
        this.setLocationRelativeTo(null);
        this.setIconImage(new Values().iconeJanela(DataImage.ICONE_JOGO));
        
    }
 
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        painelMenuPrincipal = new javax.swing.JPanel();
        btNovoJogo = new javax.swing.JButton();
        btOpcoes = new javax.swing.JButton();
        btScores = new javax.swing.JButton();
        btAjuda = new javax.swing.JButton();
        btSobre = new javax.swing.JButton();
        btSair = new javax.swing.JButton();
        icon = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Minas Revolucion");
        setResizable(false);

        painelMenuPrincipal.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Menu", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.ABOVE_TOP, new java.awt.Font("Calibri", 0, 15), new java.awt.Color(255, 0, 0))); // NOI18N

        btNovoJogo.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        btNovoJogo.setForeground(new java.awt.Color(255, 0, 0));
        btNovoJogo.setText("Novo Jogo");
        btNovoJogo.setToolTipText("Clique aqui para iniciar um novo jogo");
        btNovoJogo.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btNovoJogo.setPreferredSize(new java.awt.Dimension(250, 50));
        btNovoJogo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btNovoJogoActionPerformed(evt);
            }
        });

        btOpcoes.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        btOpcoes.setForeground(new java.awt.Color(255, 0, 0));
        btOpcoes.setText("Opções");
        btOpcoes.setToolTipText("Clique aqui para mudar as configurações do jogo");
        btOpcoes.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btOpcoes.setPreferredSize(new java.awt.Dimension(250, 50));
        btOpcoes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btOpcoesActionPerformed(evt);
            }
        });

        btScores.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        btScores.setForeground(new java.awt.Color(255, 0, 0));
        btScores.setText("Scores");
        btScores.setToolTipText("Clique aqui para ver os records dos jogadores");
        btScores.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btScores.setPreferredSize(new java.awt.Dimension(250, 50));
        btScores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btScoresActionPerformed(evt);
            }
        });

        btAjuda.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        btAjuda.setForeground(new java.awt.Color(255, 0, 0));
        btAjuda.setText("Ajuda");
        btAjuda.setToolTipText("Clique aqui para exibir a ajuda sobre o jogo");
        btAjuda.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btAjuda.setPreferredSize(new java.awt.Dimension(250, 50));
        btAjuda.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAjudaActionPerformed(evt);
            }
        });

        btSobre.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        btSobre.setForeground(new java.awt.Color(255, 0, 0));
        btSobre.setText("Sobre");
        btSobre.setToolTipText("Clique aqui para jogar contra um amigo");
        btSobre.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btSobre.setPreferredSize(new java.awt.Dimension(250, 50));
        btSobre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSobreActionPerformed(evt);
            }
        });

        btSair.setFont(new java.awt.Font("Calibri", 1, 24)); // NOI18N
        btSair.setForeground(new java.awt.Color(255, 0, 0));
        btSair.setText("Sair");
        btSair.setToolTipText("Clique aqui para abandonar o jogo");
        btSair.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btSair.setPreferredSize(new java.awt.Dimension(250, 50));
        btSair.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btSairActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout painelMenuPrincipalLayout = new javax.swing.GroupLayout(painelMenuPrincipal);
        painelMenuPrincipal.setLayout(painelMenuPrincipalLayout);
        painelMenuPrincipalLayout.setHorizontalGroup(
            painelMenuPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelMenuPrincipalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelMenuPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, painelMenuPrincipalLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(painelMenuPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btOpcoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btScores, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btAjuda, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(painelMenuPrincipalLayout.createSequentialGroup()
                        .addGroup(painelMenuPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btNovoJogo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btSair, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btSobre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        painelMenuPrincipalLayout.setVerticalGroup(
            painelMenuPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelMenuPrincipalLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btNovoJogo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btOpcoes, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btScores, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btAjuda, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btSobre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(12, 12, 12)
                .addComponent(btSair, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        icon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/placa_logomarca.png"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(painelMenuPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(icon)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(painelMenuPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(icon, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btNovoJogoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btNovoJogoActionPerformed
        dispose();
        Values.reproduzirSom(TelaMenuPrincipal.this, DataSound.getSOM_CLICK());        
        new TelaModalidades(this, true).setVisible(true);
    }//GEN-LAST:event_btNovoJogoActionPerformed

    private void btSobreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSobreActionPerformed
        Values.reproduzirSom(TelaMenuPrincipal.this, DataSound.getSOM_CLICK());
        new TelaSobre(this, true).setVisible(true);
    }//GEN-LAST:event_btSobreActionPerformed

    private void btOpcoesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btOpcoesActionPerformed
        Values.reproduzirSom(TelaMenuPrincipal.this, DataSound.getSOM_CLICK());
        new TelaOpcoes(this, true).setVisible(true);        
    }//GEN-LAST:event_btOpcoesActionPerformed

    private void btScoresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btScoresActionPerformed
        Values.reproduzirSom(TelaMenuPrincipal.this, DataSound.getSOM_CLICK());
        dispose();
        new TelaScores(this, true).setVisible(true);
    }//GEN-LAST:event_btScoresActionPerformed

    private void btAjudaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAjudaActionPerformed
        Values.reproduzirSom(TelaMenuPrincipal.this, DataSound.getSOM_CLICK());
        Values.irURL(TelaMenuPrincipal.this, "C:/Windows/ajuda.chm");
    }//GEN-LAST:event_btAjudaActionPerformed

    private void btSairActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btSairActionPerformed
        Values.reproduzirSom(TelaMenuPrincipal.this, DataSound.getSOM_QUESTAO());
        short descisao = (short) JOptionPane.showConfirmDialog(this, "Você realmente deseja sair do jogo?","Aviso!", JOptionPane.YES_NO_OPTION);        
        switch(descisao){
            case 0:
                System.exit(0);
                break;
            case 1:
                /*Não faz ação nenhuma*/
                break;
            default:
                Values.reproduzirSom(TelaMenuPrincipal.this, DataSound.getSOM_ERRO());
                JOptionPane.showMessageDialog(this, "Ocorreu um erro na aplicação será necessário forçar a saída, clique em \"Ok\"\n"
                        + "para sair da aplicação e voltar para o Windows!","Erro!", JOptionPane.ERROR_MESSAGE);
                System.exit(0);
                break;
        }
    }//GEN-LAST:event_btSairActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btAjuda;
    private javax.swing.JButton btNovoJogo;
    private javax.swing.JButton btOpcoes;
    private javax.swing.JButton btSair;
    private javax.swing.JButton btScores;
    private javax.swing.JButton btSobre;
    private javax.swing.JLabel icon;
    private javax.swing.JPanel painelMenuPrincipal;
    // End of variables declaration//GEN-END:variables
}
