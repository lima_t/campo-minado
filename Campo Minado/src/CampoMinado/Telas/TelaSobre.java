package CampoMinado.Telas;

import CampoMinado.Classes.*;

/**
 * @author Tarcísio de Lima
 * @version 1.0.0
 */
public class TelaSobre extends javax.swing.JDialog {

    public TelaSobre(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(parent);
        this.setIconImage(new Values().iconeJanela(DataImage.ICONE_SOBRE));
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        textInfo = new javax.swing.JLabel();
        textUrlFacebook = new javax.swing.JLabel();
        btFechar = new javax.swing.JButton();
        textInfoLink = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Sobre o Campo Minado");
        setResizable(false);

        textInfo.setFont(new java.awt.Font("Calibri", 0, 15)); // NOI18N
        textInfo.setText("<html>\n<p>\n<b>Produto: </b>Campo Minado<br/>\n<b>Versão: </b>1.0.2<br/>\n<b>Java: </b>1.7.0 update 5<br/>\n<b>Desenvolvedor: </b>Tarcísio de Lima<br/>\n<b>Fabricante: </b>TM Softwares®<br/>\n<b>Descrição:</b><br/>\n</p>\n<p>\nEste game é um software desenvolvido por Tarcísio de Lima, sem fins lucrativos<br/>\nou comercial, esta é uma aplicação caseira com o objetivo de testar<br/>\nconhecimentos sobre a linguagem \"java\"adquirida no decorrer de 10 meses.<br/>\n</p>\n</html>");
        textInfo.setBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(255, 0, 0), new java.awt.Color(102, 102, 102)));

        textUrlFacebook.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        textUrlFacebook.setForeground(new java.awt.Color(255, 0, 0));
        textUrlFacebook.setText("http://www.facebook.com/unknoww.user");
        textUrlFacebook.setToolTipText("Mural do facebook de Tarcísio");
        textUrlFacebook.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        textUrlFacebook.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                textUrlFacebookMouseClicked(evt);
            }
        });

        btFechar.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        btFechar.setForeground(new java.awt.Color(255, 0, 0));
        btFechar.setText("Fechar");
        btFechar.setToolTipText("Clique aqui para fechar a janela");
        btFechar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btFechar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btFecharActionPerformed(evt);
            }
        });

        textInfoLink.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        textInfoLink.setText("Para obter mais informações sobre o game acesse:");

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/logomarca.png"))); // NOI18N

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(textInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(textUrlFacebook)
                            .addComponent(textInfoLink))))
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(134, 134, 134))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(btFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(214, 214, 214))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(textInfo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(textInfoLink)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(textUrlFacebook)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 10, Short.MAX_VALUE)
                .addComponent(btFechar, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void textUrlFacebookMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_textUrlFacebookMouseClicked
        Values.reproduzirSom(TelaSobre.this, DataSound.getSOM_CLICK());
        Values.irURL(TelaSobre.this, "http://www.facebook.com/unknoww.user");
    }//GEN-LAST:event_textUrlFacebookMouseClicked

    private void btFecharActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btFecharActionPerformed
        Values.reproduzirSom(TelaSobre.this, DataSound.getSOM_CLICK());
        dispose();
    }//GEN-LAST:event_btFecharActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btFechar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel textInfo;
    private javax.swing.JLabel textInfoLink;
    private javax.swing.JLabel textUrlFacebook;
    // End of variables declaration//GEN-END:variables
}
