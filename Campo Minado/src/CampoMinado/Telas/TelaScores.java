package CampoMinado.Telas;

import CampoMinado.Classes.Dao.ScoresDAO;
import CampoMinado.Classes.*;

/**
 * @author Tarcísio de Lima
 * @version 1.0.0
 */
public class TelaScores extends javax.swing.JDialog {

    public TelaScores(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();        
        this.setLocationRelativeTo(parent);
        this.setIconImage(new Values().iconeJanela(DataImage.ICONE_SCORES));        
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        barraRolagem = new javax.swing.JScrollPane();
        tabelaRecordes = new javax.swing.JTable();
        lbIconeScores = new javax.swing.JLabel();
        btVoltarMenu = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Melhores Scores");
        setResizable(false);

        tabelaRecordes.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        tabelaRecordes.setModel(ScoresDAO.consultarRecords(TelaScores.this));
        tabelaRecordes.setToolTipText("Tabela de recordes");
        tabelaRecordes.setEnabled(false);
        tabelaRecordes.setGridColor(new java.awt.Color(51, 51, 51));
        tabelaRecordes.setRequestFocusEnabled(false);
        tabelaRecordes.setSelectionBackground(new java.awt.Color(255, 0, 0));
        tabelaRecordes.setSelectionForeground(new java.awt.Color(255, 255, 255));
        tabelaRecordes.getTableHeader().setReorderingAllowed(false);
        barraRolagem.setViewportView(tabelaRecordes);

        lbIconeScores.setFont(new java.awt.Font("Calibri", 0, 24)); // NOI18N
        lbIconeScores.setForeground(new java.awt.Color(255, 0, 0));
        lbIconeScores.setIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/iconeScore.png"))); // NOI18N
        lbIconeScores.setText("Records do Jogo");
        lbIconeScores.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);

        btVoltarMenu.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        btVoltarMenu.setForeground(new java.awt.Color(255, 0, 0));
        btVoltarMenu.setText("Menu Principal");
        btVoltarMenu.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btVoltarMenu.setRequestFocusEnabled(false);
        btVoltarMenu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btVoltarMenuActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(barraRolagem, javax.swing.GroupLayout.DEFAULT_SIZE, 591, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(lbIconeScores)
                                .addGap(169, 169, 169))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(btVoltarMenu)
                                .addGap(237, 237, 237))))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbIconeScores)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(barraRolagem, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btVoltarMenu)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    
    private void btVoltarMenuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btVoltarMenuActionPerformed
        dispose();
        new TelaMenuPrincipal().setVisible(true);
    }//GEN-LAST:event_btVoltarMenuActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane barraRolagem;
    private javax.swing.JButton btVoltarMenu;
    private javax.swing.JLabel lbIconeScores;
    private javax.swing.JTable tabelaRecordes;
    // End of variables declaration//GEN-END:variables
}
