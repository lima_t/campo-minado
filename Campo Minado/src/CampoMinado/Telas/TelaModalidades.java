package CampoMinado.Telas;

import CampoMinado.Classes.Tabelas.TabelaScores;
import CampoMinado.Classes.*;
import javax.swing.JOptionPane;

/**
 * @author Tarcísio de Lima
 * @version 1.0.1
 * @see TelaMenuPrincipal
 * 
 * Tela de Modalidades é Aberta logo quando se clica no botão do menu "Novo Jogo"
 */
public class TelaModalidades extends javax.swing.JDialog {

    public TelaModalidades(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        cacheAnterior();
        this.setLocationRelativeTo(parent);
        this.setIconImage(new Values().iconeJanela(DataImage.ICONE_MODALIDADES));
    }
    
    /**
     * @param nulo
     * 
     * O metodo "validarModalidade" tem como principal função verificar se todos os campos e opções da tela de
     * modalidades foram escolhidos corretamente e no final iniciar o jogo caso tudo esteja certo. Este metodo não
     * recebe nada como parametro e não retorna valor algum.
     */
    private void validarModalidade(){
        
        if(txtNickName.getText() == null || "".equals(txtNickName.getText()) ||
             txtNickName.getText().length() > 19 || txtNickName.getText().contains("'") ||
             txtNickName.getText().contains("\"")
           ){  
            
            Values.reproduzirSom(TelaModalidades.this, DataSound.getSOM_ERRO());
            JOptionPane.showMessageDialog(TelaModalidades.this, "Você colocou um \"Nick Name\" inválido! Seu Nick não "
                    + "pode ser muito grande,\nnão pode ser vazio, nem pode conter \" ou '!", "Aviso!",
                    JOptionPane.INFORMATION_MESSAGE);
            
        }else{
            
            if(choiceAvatar1.isSelected()){
                Values.avatarJogador = 1;
            }else if(choiceAvatar2.isSelected()){
                Values.avatarJogador = 2;
            }else if(choiceAvatar3.isSelected()){
               Values.avatarJogador = 3;
            }else if(choiceAvatar4.isSelected()){
                Values.avatarJogador = 4;
            }else if(choiceAvatar5.isSelected()){
                Values.avatarJogador = 5;
            }else if(choiceAvatar6.isSelected()){
                Values.avatarJogador = 6;
            }else if(choiceAvatar7.isSelected()){
                Values.avatarJogador = 7;
            }else if(choiceAvatar8.isSelected()){
                Values.avatarJogador = 8;
            }else if(choiceAvatar9.isSelected()){
                Values.avatarJogador = 9;
            }else if(choiceAvatar10.isSelected()){
                Values.avatarJogador = 10;
            }else if(choiceAvatar11.isSelected()){
                Values.avatarJogador = 11;
            }else if(choiceAvatar12.isSelected()){
                Values.avatarJogador = 12;
            }else{
                Values.reproduzirSom(TelaModalidades.this, DataSound.getSOM_ERRO());
                JOptionPane.showMessageDialog(TelaModalidades.this, "Nenhuma opção de modalidade foi selecionada!"
                    , "Aviso!", JOptionPane.INFORMATION_MESSAGE);                
            }
            
            if(choicePacienciaMode.isSelected()){
                Values.modalidadeJogo = 1;
            }else if(choiceCaptureMode.isSelected()){
                Values.modalidadeJogo = 2;
            }else if(choiceDamageMode.isSelected()){
                Values.modalidadeJogo = 3;
            }else{
                Values.reproduzirSom(TelaModalidades.this, DataSound.getSOM_ERRO());
            JOptionPane.showMessageDialog(TelaModalidades.this, "Nenhum dos avatares foi escolhido pelo jogador"
                    , "Aviso!", JOptionPane.INFORMATION_MESSAGE);
            }
            
            TabelaScores.setNickJogador(txtNickName.getText());
            dispose();
            Values.iniciarJogo();
        }
    }
    
    /**
     * @param nulo
     * @return nulo
     * 
     * O metodo "cacheAnterior" recupera as escolhas selecionadas pelo jogador da ultima partida e já as deixam marcadas
     * como padrão para facilitar e agilizar o processo de seleção, este metodo não recebe nada como parametro e não 
     * retorna valor nenhum
     */
    private void cacheAnterior(){
        switch(Values.avatarJogador){
            case 1:
                choiceAvatar1.setSelected(true);
                break;                
            case 2:
                choiceAvatar2.setSelected(true);
                break;                
            case 3:
                choiceAvatar3.setSelected(true);
                break;                
            case 4:
                choiceAvatar4.setSelected(true);
                break;                
            case 5:
                choiceAvatar5.setSelected(true);
                break;                
            case 6:
                choiceAvatar6.setSelected(true);
                break;                
            case 7:
                choiceAvatar8.setSelected(true);
                break;                
            case 8:
                choiceAvatar9.setSelected(true);
                break;                
            case 9:
                choiceAvatar7.setSelected(true);
                break;                
            case 10:
                choiceAvatar11.setSelected(true);
                break;                
            case 11:
                choiceAvatar12.setSelected(true);
                break;                
            case 12:
                choiceAvatar10.setSelected(true);
                break;                
            default:                
                choiceAvatar1.setSelected(true);
                break;
        }
        
        switch(Values.modalidadeJogo){
            case 1:
                choicePacienciaMode.setSelected(true);
                break;
            case 2:
                choiceCaptureMode.setSelected(true);
                break;
            case 3:
                choiceDamageMode.setSelected(true);
                break;
            default:
                choicePacienciaMode.setSelected(true);
                break;
        }
   }
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        avatarJogador = new javax.swing.ButtonGroup();
        modalidadeJogo = new javax.swing.ButtonGroup();
        painelAvatares = new javax.swing.JPanel();
        choiceAvatar1 = new javax.swing.JRadioButton();
        choiceAvatar2 = new javax.swing.JRadioButton();
        choiceAvatar3 = new javax.swing.JRadioButton();
        choiceAvatar4 = new javax.swing.JRadioButton();
        choiceAvatar5 = new javax.swing.JRadioButton();
        choiceAvatar6 = new javax.swing.JRadioButton();
        choiceAvatar7 = new javax.swing.JRadioButton();
        choiceAvatar8 = new javax.swing.JRadioButton();
        choiceAvatar9 = new javax.swing.JRadioButton();
        choiceAvatar10 = new javax.swing.JRadioButton();
        choiceAvatar11 = new javax.swing.JRadioButton();
        choiceAvatar12 = new javax.swing.JRadioButton();
        painelaModalidade = new javax.swing.JPanel();
        choicePacienciaMode = new javax.swing.JRadioButton();
        choiceCaptureMode = new javax.swing.JRadioButton();
        choiceDamageMode = new javax.swing.JRadioButton();
        painelNickName = new javax.swing.JPanel();
        txtNickName = new javax.swing.JTextField();
        btVoltar = new javax.swing.JButton();
        btIniciarJogo = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Modalidades");
        setResizable(false);

        painelAvatares.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Selecione seu jogador", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.ABOVE_TOP, new java.awt.Font("Calibri", 0, 16), new java.awt.Color(255, 0, 0))); // NOI18N

        avatarJogador.add(choiceAvatar1);
        choiceAvatar1.setSelected(true);
        choiceAvatar1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        choiceAvatar1.setDisabledIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar01invalid.jpg"))); // NOI18N
        choiceAvatar1.setDisabledSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar01invalid.jpg"))); // NOI18N
        choiceAvatar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar01.jpg"))); // NOI18N
        choiceAvatar1.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar01selected.jpg"))); // NOI18N

        avatarJogador.add(choiceAvatar2);
        choiceAvatar2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        choiceAvatar2.setDisabledIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar02invalid.jpg"))); // NOI18N
        choiceAvatar2.setDisabledSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar02invalid.jpg"))); // NOI18N
        choiceAvatar2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar02.jpg"))); // NOI18N
        choiceAvatar2.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar02selected.jpg"))); // NOI18N

        avatarJogador.add(choiceAvatar3);
        choiceAvatar3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        choiceAvatar3.setDisabledIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar03invalid.jpg"))); // NOI18N
        choiceAvatar3.setDisabledSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar03invalid.jpg"))); // NOI18N
        choiceAvatar3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar03.jpg"))); // NOI18N
        choiceAvatar3.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar03selected.jpg"))); // NOI18N

        avatarJogador.add(choiceAvatar4);
        choiceAvatar4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        choiceAvatar4.setDisabledIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar04invalid.jpg"))); // NOI18N
        choiceAvatar4.setDisabledSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar04invalid.jpg"))); // NOI18N
        choiceAvatar4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar04.jpg"))); // NOI18N
        choiceAvatar4.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar04selected.jpg"))); // NOI18N

        avatarJogador.add(choiceAvatar5);
        choiceAvatar5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        choiceAvatar5.setDisabledIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar05invalid.jpg"))); // NOI18N
        choiceAvatar5.setDisabledSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar05invalid.jpg"))); // NOI18N
        choiceAvatar5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar05.jpg"))); // NOI18N
        choiceAvatar5.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar05selected.jpg"))); // NOI18N

        avatarJogador.add(choiceAvatar6);
        choiceAvatar6.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        choiceAvatar6.setDisabledIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar06invalid.jpg"))); // NOI18N
        choiceAvatar6.setDisabledSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar06invalid.jpg"))); // NOI18N
        choiceAvatar6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar06.jpg"))); // NOI18N
        choiceAvatar6.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar06selected.jpg"))); // NOI18N

        avatarJogador.add(choiceAvatar7);
        choiceAvatar7.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        choiceAvatar7.setDisabledIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar08invalid.jpg"))); // NOI18N
        choiceAvatar7.setDisabledSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar08invalid.jpg"))); // NOI18N
        choiceAvatar7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar08.jpg"))); // NOI18N
        choiceAvatar7.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar08selected.jpg"))); // NOI18N

        avatarJogador.add(choiceAvatar8);
        choiceAvatar8.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        choiceAvatar8.setDisabledIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar09invalid.jpg"))); // NOI18N
        choiceAvatar8.setDisabledSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar09invalid.jpg"))); // NOI18N
        choiceAvatar8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar09.jpg"))); // NOI18N
        choiceAvatar8.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar09selected.jpg"))); // NOI18N

        avatarJogador.add(choiceAvatar9);
        choiceAvatar9.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        choiceAvatar9.setDisabledIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar07invalid.jpg"))); // NOI18N
        choiceAvatar9.setDisabledSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar07invalid.jpg"))); // NOI18N
        choiceAvatar9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar07.jpg"))); // NOI18N
        choiceAvatar9.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar07selected.jpg"))); // NOI18N

        avatarJogador.add(choiceAvatar10);
        choiceAvatar10.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        choiceAvatar10.setDisabledIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar11invalid.jpg"))); // NOI18N
        choiceAvatar10.setDisabledSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar11invalid.jpg"))); // NOI18N
        choiceAvatar10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar11.jpg"))); // NOI18N
        choiceAvatar10.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar11selected.jpg"))); // NOI18N

        avatarJogador.add(choiceAvatar11);
        choiceAvatar11.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        choiceAvatar11.setDisabledIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar12invalid.jpg"))); // NOI18N
        choiceAvatar11.setDisabledSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar12invalid.jpg"))); // NOI18N
        choiceAvatar11.setIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar12.jpg"))); // NOI18N
        choiceAvatar11.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar12selected.jpg"))); // NOI18N

        avatarJogador.add(choiceAvatar12);
        choiceAvatar12.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        choiceAvatar12.setDisabledIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar10invalid.jpg"))); // NOI18N
        choiceAvatar12.setDisabledSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar10invalid.jpg"))); // NOI18N
        choiceAvatar12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar10.jpg"))); // NOI18N
        choiceAvatar12.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/avatar10selected.jpg"))); // NOI18N

        javax.swing.GroupLayout painelAvataresLayout = new javax.swing.GroupLayout(painelAvatares);
        painelAvatares.setLayout(painelAvataresLayout);
        painelAvataresLayout.setHorizontalGroup(
            painelAvataresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelAvataresLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelAvataresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(choiceAvatar1)
                    .addComponent(choiceAvatar7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelAvataresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(painelAvataresLayout.createSequentialGroup()
                        .addComponent(choiceAvatar2)
                        .addGap(2, 2, 2))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, painelAvataresLayout.createSequentialGroup()
                        .addComponent(choiceAvatar8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                .addGroup(painelAvataresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(painelAvataresLayout.createSequentialGroup()
                        .addComponent(choiceAvatar3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(choiceAvatar4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(choiceAvatar5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(choiceAvatar6))
                    .addGroup(painelAvataresLayout.createSequentialGroup()
                        .addComponent(choiceAvatar9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(choiceAvatar10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(choiceAvatar11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(choiceAvatar12)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        painelAvataresLayout.setVerticalGroup(
            painelAvataresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelAvataresLayout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(painelAvataresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(painelAvataresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(painelAvataresLayout.createSequentialGroup()
                            .addGroup(painelAvataresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(choiceAvatar3)
                                .addComponent(choiceAvatar2)
                                .addComponent(choiceAvatar4)
                                .addComponent(choiceAvatar1))
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addGroup(painelAvataresLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(choiceAvatar7)
                                .addComponent(choiceAvatar8)
                                .addComponent(choiceAvatar9)
                                .addComponent(choiceAvatar10)
                                .addComponent(choiceAvatar11)
                                .addComponent(choiceAvatar12))
                            .addGap(368, 368, 368))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, painelAvataresLayout.createSequentialGroup()
                            .addComponent(choiceAvatar5)
                            .addGap(492, 492, 492)))
                    .addGroup(painelAvataresLayout.createSequentialGroup()
                        .addComponent(choiceAvatar6)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        painelaModalidade.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Escolha o modo do jogo", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.ABOVE_TOP, new java.awt.Font("Calibri", 0, 16), new java.awt.Color(255, 0, 0))); // NOI18N

        modalidadeJogo.add(choicePacienciaMode);
        choicePacienciaMode.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        choicePacienciaMode.setSelected(true);
        choicePacienciaMode.setText("Paciência");
        choicePacienciaMode.setToolTipText("Modo paciência");
        choicePacienciaMode.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        choicePacienciaMode.setIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/modePaciencia.png"))); // NOI18N
        choicePacienciaMode.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/modePacienciaSelected.png"))); // NOI18N

        modalidadeJogo.add(choiceCaptureMode);
        choiceCaptureMode.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        choiceCaptureMode.setText("Capture a Bandeira");
        choiceCaptureMode.setToolTipText("Modo capture a bandeira");
        choiceCaptureMode.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        choiceCaptureMode.setIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/modeCaptureFlag.png"))); // NOI18N
        choiceCaptureMode.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/modeCaptureFlagSelected.png"))); // NOI18N

        modalidadeJogo.add(choiceDamageMode);
        choiceDamageMode.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        choiceDamageMode.setText("Dano Crítico");
        choiceDamageMode.setToolTipText("Modo dano crítico");
        choiceDamageMode.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        choiceDamageMode.setIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/modeDamage.png"))); // NOI18N
        choiceDamageMode.setSelectedIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/modeDamageSelected.png"))); // NOI18N

        javax.swing.GroupLayout painelaModalidadeLayout = new javax.swing.GroupLayout(painelaModalidade);
        painelaModalidade.setLayout(painelaModalidadeLayout);
        painelaModalidadeLayout.setHorizontalGroup(
            painelaModalidadeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelaModalidadeLayout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addComponent(choicePacienciaMode)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(choiceCaptureMode)
                .addGap(85, 85, 85)
                .addComponent(choiceDamageMode)
                .addGap(45, 45, 45))
        );
        painelaModalidadeLayout.setVerticalGroup(
            painelaModalidadeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelaModalidadeLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelaModalidadeLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(choicePacienciaMode)
                    .addComponent(choiceCaptureMode)
                    .addComponent(choiceDamageMode))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        painelNickName.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Nick Name do Jogador", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.ABOVE_TOP, new java.awt.Font("Calibri", 0, 16), new java.awt.Color(255, 0, 0))); // NOI18N

        txtNickName.setFont(new java.awt.Font("Calibri", 0, 15)); // NOI18N
        txtNickName.setHorizontalAlignment(javax.swing.JTextField.LEFT);
        txtNickName.setToolTipText("Digite seu Nick Name aqui para ser referenciado na tabela de records");
        txtNickName.setSelectedTextColor(new java.awt.Color(255, 255, 255));
        txtNickName.setSelectionColor(new java.awt.Color(255, 0, 0));

        javax.swing.GroupLayout painelNickNameLayout = new javax.swing.GroupLayout(painelNickName);
        painelNickName.setLayout(painelNickNameLayout);
        painelNickNameLayout.setHorizontalGroup(
            painelNickNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelNickNameLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtNickName, javax.swing.GroupLayout.DEFAULT_SIZE, 355, Short.MAX_VALUE)
                .addContainerGap())
        );
        painelNickNameLayout.setVerticalGroup(
            painelNickNameLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelNickNameLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txtNickName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btVoltar.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        btVoltar.setForeground(new java.awt.Color(255, 0, 0));
        btVoltar.setText("Menu Principal");
        btVoltar.setToolTipText("Voltar para o menu principal do jogo");
        btVoltar.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btVoltarActionPerformed(evt);
            }
        });

        btIniciarJogo.setFont(new java.awt.Font("Calibri", 1, 16)); // NOI18N
        btIniciarJogo.setForeground(new java.awt.Color(255, 0, 0));
        btIniciarJogo.setText("Iniciar Jogo");
        btIniciarJogo.setToolTipText("Iniciar partida!");
        btIniciarJogo.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btIniciarJogo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btIniciarJogoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(painelAvatares, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(painelaModalidade, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(painelNickName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btVoltar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btIniciarJogo)))
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btIniciarJogo, btVoltar});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(painelAvatares, javax.swing.GroupLayout.PREFERRED_SIZE, 286, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(painelaModalidade, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(painelNickName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btIniciarJogo)))
                .addContainerGap(13, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btIniciarJogo, btVoltar});

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btVoltarActionPerformed
        Values.reproduzirSom(TelaModalidades.this, DataSound.getSOM_CLICK());
        new TelaMenuPrincipal().setVisible(true);
        dispose();
    }//GEN-LAST:event_btVoltarActionPerformed

    private void btIniciarJogoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btIniciarJogoActionPerformed
        Values.reproduzirSom(TelaModalidades.this, DataSound.getSOM_CLICK());
        validarModalidade();
    }//GEN-LAST:event_btIniciarJogoActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup avatarJogador;
    private javax.swing.JButton btIniciarJogo;
    private javax.swing.JButton btVoltar;
    private javax.swing.JRadioButton choiceAvatar1;
    private javax.swing.JRadioButton choiceAvatar10;
    private javax.swing.JRadioButton choiceAvatar11;
    private javax.swing.JRadioButton choiceAvatar12;
    private javax.swing.JRadioButton choiceAvatar2;
    private javax.swing.JRadioButton choiceAvatar3;
    private javax.swing.JRadioButton choiceAvatar4;
    private javax.swing.JRadioButton choiceAvatar5;
    private javax.swing.JRadioButton choiceAvatar6;
    private javax.swing.JRadioButton choiceAvatar7;
    private javax.swing.JRadioButton choiceAvatar8;
    private javax.swing.JRadioButton choiceAvatar9;
    private javax.swing.JRadioButton choiceCaptureMode;
    private javax.swing.JRadioButton choiceDamageMode;
    private javax.swing.JRadioButton choicePacienciaMode;
    private javax.swing.ButtonGroup modalidadeJogo;
    private javax.swing.JPanel painelAvatares;
    private javax.swing.JPanel painelNickName;
    private javax.swing.JPanel painelaModalidade;
    private javax.swing.JTextField txtNickName;
    // End of variables declaration//GEN-END:variables
}
