package CampoMinado.Telas;

import CampoMinado.Classes.*;
import CampoMinado.Classes.Tabelas.TabelaScores;

/**
 * @author Tarcísio de Lima
 * @version 1.0.0
 * 
 * Essa Tela é chamada apenas se a opção do nível fácil do jogo estiver selecionada, nela temos um campo 6x6 onde
 * o jogador deverá tentar captaurar todos pontos
 */
public class TelaJogoNormal extends javax.swing.JFrame {

    public TelaJogoNormal() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.setIconImage(new Values().iconeJanela(DataImage.ICONE_NOVO_JOGO));
        new Values().iconeAvatarJogador(TelaJogoNormal.this, avatarIcon);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        painelJogoNormal = new javax.swing.JPanel();
        bt1 = new javax.swing.JButton();
        bt2 = new javax.swing.JButton();
        bt3 = new javax.swing.JButton();
        bt4 = new javax.swing.JButton();
        bt5 = new javax.swing.JButton();
        bt6 = new javax.swing.JButton();
        bt7 = new javax.swing.JButton();
        bt8 = new javax.swing.JButton();
        bt9 = new javax.swing.JButton();
        bt10 = new javax.swing.JButton();
        bt11 = new javax.swing.JButton();
        bt12 = new javax.swing.JButton();
        bt13 = new javax.swing.JButton();
        bt14 = new javax.swing.JButton();
        bt15 = new javax.swing.JButton();
        bt16 = new javax.swing.JButton();
        bt17 = new javax.swing.JButton();
        bt18 = new javax.swing.JButton();
        bt19 = new javax.swing.JButton();
        bt20 = new javax.swing.JButton();
        bt21 = new javax.swing.JButton();
        bt22 = new javax.swing.JButton();
        bt23 = new javax.swing.JButton();
        bt24 = new javax.swing.JButton();
        bt25 = new javax.swing.JButton();
        bt26 = new javax.swing.JButton();
        bt27 = new javax.swing.JButton();
        bt28 = new javax.swing.JButton();
        bt29 = new javax.swing.JButton();
        bt30 = new javax.swing.JButton();
        bt31 = new javax.swing.JButton();
        bt32 = new javax.swing.JButton();
        bt33 = new javax.swing.JButton();
        bt34 = new javax.swing.JButton();
        bt35 = new javax.swing.JButton();
        bt36 = new javax.swing.JButton();
        bt37 = new javax.swing.JButton();
        bt38 = new javax.swing.JButton();
        bt39 = new javax.swing.JButton();
        bt40 = new javax.swing.JButton();
        bt41 = new javax.swing.JButton();
        bt42 = new javax.swing.JButton();
        bt43 = new javax.swing.JButton();
        bt44 = new javax.swing.JButton();
        bt45 = new javax.swing.JButton();
        bt46 = new javax.swing.JButton();
        bt47 = new javax.swing.JButton();
        bt48 = new javax.swing.JButton();
        bt49 = new javax.swing.JButton();
        bt50 = new javax.swing.JButton();
        bt51 = new javax.swing.JButton();
        bt52 = new javax.swing.JButton();
        bt53 = new javax.swing.JButton();
        bt54 = new javax.swing.JButton();
        bt55 = new javax.swing.JButton();
        bt56 = new javax.swing.JButton();
        bt57 = new javax.swing.JButton();
        bt58 = new javax.swing.JButton();
        bt59 = new javax.swing.JButton();
        bt60 = new javax.swing.JButton();
        bt61 = new javax.swing.JButton();
        bt62 = new javax.swing.JButton();
        bt63 = new javax.swing.JButton();
        bt64 = new javax.swing.JButton();
        bt65 = new javax.swing.JButton();
        bt66 = new javax.swing.JButton();
        bt67 = new javax.swing.JButton();
        bt68 = new javax.swing.JButton();
        bt69 = new javax.swing.JButton();
        bt70 = new javax.swing.JButton();
        bt71 = new javax.swing.JButton();
        bt72 = new javax.swing.JButton();
        bt73 = new javax.swing.JButton();
        bt74 = new javax.swing.JButton();
        bt75 = new javax.swing.JButton();
        bt76 = new javax.swing.JButton();
        bt77 = new javax.swing.JButton();
        bt78 = new javax.swing.JButton();
        bt79 = new javax.swing.JButton();
        bt80 = new javax.swing.JButton();
        bt81 = new javax.swing.JButton();
        bt82 = new javax.swing.JButton();
        bt83 = new javax.swing.JButton();
        bt84 = new javax.swing.JButton();
        bt85 = new javax.swing.JButton();
        bt86 = new javax.swing.JButton();
        bt87 = new javax.swing.JButton();
        bt88 = new javax.swing.JButton();
        bt89 = new javax.swing.JButton();
        bt90 = new javax.swing.JButton();
        bt91 = new javax.swing.JButton();
        bt92 = new javax.swing.JButton();
        bt93 = new javax.swing.JButton();
        bt94 = new javax.swing.JButton();
        bt95 = new javax.swing.JButton();
        bt96 = new javax.swing.JButton();
        bt97 = new javax.swing.JButton();
        bt98 = new javax.swing.JButton();
        bt99 = new javax.swing.JButton();
        bt100 = new javax.swing.JButton();
        painelInfo = new javax.swing.JPanel();
        painelPontuacao = new javax.swing.JPanel();
        lbPontuacao = new javax.swing.JLabel();
        painelJogador = new javax.swing.JPanel();
        lbNivelDificuldade = new javax.swing.JLabel();
        avatarIcon = new javax.swing.JLabel();
        painelMenuRapido = new javax.swing.JPanel();
        btRestart = new javax.swing.JButton();
        btDesistir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Minas Nível Normal");
        setResizable(false);

        painelJogoNormal.setBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(255, 0, 0), new java.awt.Color(102, 102, 102)));
        painelJogoNormal.setToolTipText("Blocos");

        bt1.setBackground(Values.corBlocos);
        bt1.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt1.setPreferredSize(new java.awt.Dimension(48, 48));
        bt1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt1ActionPerformed(evt);
            }
        });

        bt2.setBackground(Values.corBlocos);
        bt2.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt2.setPreferredSize(new java.awt.Dimension(48, 48));
        bt2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt2ActionPerformed(evt);
            }
        });

        bt3.setBackground(Values.corBlocos);
        bt3.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt3.setPreferredSize(new java.awt.Dimension(48, 48));
        bt3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt3ActionPerformed(evt);
            }
        });

        bt4.setBackground(Values.corBlocos);
        bt4.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt4.setPreferredSize(new java.awt.Dimension(48, 48));
        bt4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt4ActionPerformed(evt);
            }
        });

        bt5.setBackground(Values.corBlocos);
        bt5.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt5.setPreferredSize(new java.awt.Dimension(48, 48));
        bt5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt5ActionPerformed(evt);
            }
        });

        bt6.setBackground(Values.corBlocos);
        bt6.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt6.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt6.setPreferredSize(new java.awt.Dimension(48, 48));
        bt6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt6ActionPerformed(evt);
            }
        });

        bt7.setBackground(Values.corBlocos);
        bt7.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt7.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt7.setPreferredSize(new java.awt.Dimension(48, 48));
        bt7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt7ActionPerformed(evt);
            }
        });

        bt8.setBackground(Values.corBlocos);
        bt8.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt8.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt8.setPreferredSize(new java.awt.Dimension(48, 48));
        bt8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt8ActionPerformed(evt);
            }
        });

        bt9.setBackground(Values.corBlocos);
        bt9.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt9.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt9.setPreferredSize(new java.awt.Dimension(48, 48));
        bt9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt9ActionPerformed(evt);
            }
        });

        bt10.setBackground(Values.corBlocos);
        bt10.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt10.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt10.setPreferredSize(new java.awt.Dimension(48, 48));
        bt10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt10ActionPerformed(evt);
            }
        });

        bt11.setBackground(Values.corBlocos);
        bt11.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt11.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt11.setPreferredSize(new java.awt.Dimension(48, 48));
        bt11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt11ActionPerformed(evt);
            }
        });

        bt12.setBackground(Values.corBlocos);
        bt12.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt12.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt12.setPreferredSize(new java.awt.Dimension(48, 48));
        bt12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt12ActionPerformed(evt);
            }
        });

        bt13.setBackground(Values.corBlocos);
        bt13.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt13.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt13.setPreferredSize(new java.awt.Dimension(48, 48));
        bt13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt13ActionPerformed(evt);
            }
        });

        bt14.setBackground(Values.corBlocos);
        bt14.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt14.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt14.setPreferredSize(new java.awt.Dimension(48, 48));
        bt14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt14ActionPerformed(evt);
            }
        });

        bt15.setBackground(Values.corBlocos);
        bt15.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt15.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt15.setPreferredSize(new java.awt.Dimension(48, 48));
        bt15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt15ActionPerformed(evt);
            }
        });

        bt16.setBackground(Values.corBlocos);
        bt16.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt16.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt16.setPreferredSize(new java.awt.Dimension(48, 48));
        bt16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt16ActionPerformed(evt);
            }
        });

        bt17.setBackground(Values.corBlocos);
        bt17.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt17.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt17.setPreferredSize(new java.awt.Dimension(48, 48));
        bt17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt17ActionPerformed(evt);
            }
        });

        bt18.setBackground(Values.corBlocos);
        bt18.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt18.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt18.setPreferredSize(new java.awt.Dimension(48, 48));
        bt18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt18ActionPerformed(evt);
            }
        });

        bt19.setBackground(Values.corBlocos);
        bt19.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt19.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt19.setPreferredSize(new java.awt.Dimension(48, 48));
        bt19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt19ActionPerformed(evt);
            }
        });

        bt20.setBackground(Values.corBlocos);
        bt20.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt20.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt20.setPreferredSize(new java.awt.Dimension(48, 48));
        bt20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt20ActionPerformed(evt);
            }
        });

        bt21.setBackground(Values.corBlocos);
        bt21.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt21.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt21.setPreferredSize(new java.awt.Dimension(48, 48));
        bt21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt21ActionPerformed(evt);
            }
        });

        bt22.setBackground(Values.corBlocos);
        bt22.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt22.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt22.setPreferredSize(new java.awt.Dimension(48, 48));
        bt22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt22ActionPerformed(evt);
            }
        });

        bt23.setBackground(Values.corBlocos);
        bt23.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt23.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt23.setPreferredSize(new java.awt.Dimension(48, 48));
        bt23.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt23ActionPerformed(evt);
            }
        });

        bt24.setBackground(Values.corBlocos);
        bt24.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt24.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt24.setPreferredSize(new java.awt.Dimension(48, 48));
        bt24.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt24ActionPerformed(evt);
            }
        });

        bt25.setBackground(Values.corBlocos);
        bt25.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt25.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt25.setPreferredSize(new java.awt.Dimension(48, 48));
        bt25.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt25ActionPerformed(evt);
            }
        });

        bt26.setBackground(Values.corBlocos);
        bt26.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt26.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt26.setPreferredSize(new java.awt.Dimension(48, 48));
        bt26.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt26ActionPerformed(evt);
            }
        });

        bt27.setBackground(Values.corBlocos);
        bt27.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt27.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt27.setPreferredSize(new java.awt.Dimension(48, 48));
        bt27.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt27ActionPerformed(evt);
            }
        });

        bt28.setBackground(Values.corBlocos);
        bt28.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt28.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt28.setPreferredSize(new java.awt.Dimension(48, 48));
        bt28.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt28ActionPerformed(evt);
            }
        });

        bt29.setBackground(Values.corBlocos);
        bt29.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt29.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt29.setPreferredSize(new java.awt.Dimension(48, 48));
        bt29.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt29ActionPerformed(evt);
            }
        });

        bt30.setBackground(Values.corBlocos);
        bt30.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt30.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt30.setPreferredSize(new java.awt.Dimension(48, 48));
        bt30.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt30ActionPerformed(evt);
            }
        });

        bt31.setBackground(Values.corBlocos);
        bt31.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt31.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt31.setPreferredSize(new java.awt.Dimension(48, 48));
        bt31.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt31ActionPerformed(evt);
            }
        });

        bt32.setBackground(Values.corBlocos);
        bt32.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt32.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt32.setPreferredSize(new java.awt.Dimension(48, 48));
        bt32.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt32ActionPerformed(evt);
            }
        });

        bt33.setBackground(Values.corBlocos);
        bt33.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt33.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt33.setPreferredSize(new java.awt.Dimension(48, 48));
        bt33.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt33ActionPerformed(evt);
            }
        });

        bt34.setBackground(Values.corBlocos);
        bt34.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt34.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt34.setPreferredSize(new java.awt.Dimension(48, 48));
        bt34.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt34ActionPerformed(evt);
            }
        });

        bt35.setBackground(Values.corBlocos);
        bt35.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt35.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt35.setPreferredSize(new java.awt.Dimension(48, 48));
        bt35.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt35ActionPerformed(evt);
            }
        });

        bt36.setBackground(Values.corBlocos);
        bt36.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt36.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt36.setPreferredSize(new java.awt.Dimension(48, 48));
        bt36.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt36ActionPerformed(evt);
            }
        });

        bt37.setBackground(Values.corBlocos);
        bt37.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt37.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt37.setPreferredSize(new java.awt.Dimension(48, 48));
        bt37.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt37ActionPerformed(evt);
            }
        });

        bt38.setBackground(Values.corBlocos);
        bt38.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt38.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt38.setPreferredSize(new java.awt.Dimension(48, 48));
        bt38.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt38ActionPerformed(evt);
            }
        });

        bt39.setBackground(Values.corBlocos);
        bt39.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt39.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt39.setPreferredSize(new java.awt.Dimension(48, 48));
        bt39.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt39ActionPerformed(evt);
            }
        });

        bt40.setBackground(Values.corBlocos);
        bt40.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt40.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt40.setPreferredSize(new java.awt.Dimension(48, 48));
        bt40.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt40ActionPerformed(evt);
            }
        });

        bt41.setBackground(Values.corBlocos);
        bt41.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt41.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt41.setPreferredSize(new java.awt.Dimension(48, 48));
        bt41.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt41ActionPerformed(evt);
            }
        });

        bt42.setBackground(Values.corBlocos);
        bt42.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt42.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt42.setPreferredSize(new java.awt.Dimension(48, 48));
        bt42.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt42ActionPerformed(evt);
            }
        });

        bt43.setBackground(Values.corBlocos);
        bt43.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt43.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt43.setPreferredSize(new java.awt.Dimension(48, 48));
        bt43.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt43ActionPerformed(evt);
            }
        });

        bt44.setBackground(Values.corBlocos);
        bt44.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt44.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt44.setPreferredSize(new java.awt.Dimension(48, 48));
        bt44.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt44ActionPerformed(evt);
            }
        });

        bt45.setBackground(Values.corBlocos);
        bt45.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt45.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt45.setPreferredSize(new java.awt.Dimension(48, 48));
        bt45.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt45ActionPerformed(evt);
            }
        });

        bt46.setBackground(Values.corBlocos);
        bt46.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt46.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt46.setPreferredSize(new java.awt.Dimension(48, 48));
        bt46.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt46ActionPerformed(evt);
            }
        });

        bt47.setBackground(Values.corBlocos);
        bt47.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt47.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt47.setPreferredSize(new java.awt.Dimension(48, 48));
        bt47.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt47ActionPerformed(evt);
            }
        });

        bt48.setBackground(Values.corBlocos);
        bt48.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt48.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt48.setPreferredSize(new java.awt.Dimension(48, 48));
        bt48.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt48ActionPerformed(evt);
            }
        });

        bt49.setBackground(Values.corBlocos);
        bt49.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt49.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt49.setPreferredSize(new java.awt.Dimension(48, 48));
        bt49.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt49ActionPerformed(evt);
            }
        });

        bt50.setBackground(Values.corBlocos);
        bt50.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt50.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt50.setPreferredSize(new java.awt.Dimension(48, 48));
        bt50.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt50ActionPerformed(evt);
            }
        });

        bt51.setBackground(Values.corBlocos);
        bt51.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt51.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt51.setPreferredSize(new java.awt.Dimension(48, 48));
        bt51.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt51ActionPerformed(evt);
            }
        });

        bt52.setBackground(Values.corBlocos);
        bt52.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt52.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt52.setPreferredSize(new java.awt.Dimension(48, 48));
        bt52.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt52ActionPerformed(evt);
            }
        });

        bt53.setBackground(Values.corBlocos);
        bt53.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt53.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt53.setPreferredSize(new java.awt.Dimension(48, 48));
        bt53.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt53ActionPerformed(evt);
            }
        });

        bt54.setBackground(Values.corBlocos);
        bt54.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt54.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt54.setPreferredSize(new java.awt.Dimension(48, 48));
        bt54.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt54ActionPerformed(evt);
            }
        });

        bt55.setBackground(Values.corBlocos);
        bt55.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt55.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt55.setPreferredSize(new java.awt.Dimension(48, 48));
        bt55.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt55ActionPerformed(evt);
            }
        });

        bt56.setBackground(Values.corBlocos);
        bt56.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt56.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt56.setPreferredSize(new java.awt.Dimension(48, 48));
        bt56.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt56ActionPerformed(evt);
            }
        });

        bt57.setBackground(Values.corBlocos);
        bt57.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt57.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt57.setPreferredSize(new java.awt.Dimension(48, 48));
        bt57.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt57ActionPerformed(evt);
            }
        });

        bt58.setBackground(Values.corBlocos);
        bt58.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt58.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt58.setPreferredSize(new java.awt.Dimension(48, 48));
        bt58.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt58ActionPerformed(evt);
            }
        });

        bt59.setBackground(Values.corBlocos);
        bt59.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt59.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt59.setPreferredSize(new java.awt.Dimension(48, 48));
        bt59.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt59ActionPerformed(evt);
            }
        });

        bt60.setBackground(Values.corBlocos);
        bt60.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt60.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt60.setPreferredSize(new java.awt.Dimension(48, 48));
        bt60.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt60ActionPerformed(evt);
            }
        });

        bt61.setBackground(Values.corBlocos);
        bt61.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt61.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt61.setPreferredSize(new java.awt.Dimension(48, 48));
        bt61.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt61ActionPerformed(evt);
            }
        });

        bt62.setBackground(Values.corBlocos);
        bt62.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt62.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt62.setPreferredSize(new java.awt.Dimension(48, 48));
        bt62.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt62ActionPerformed(evt);
            }
        });

        bt63.setBackground(Values.corBlocos);
        bt63.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt63.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt63.setPreferredSize(new java.awt.Dimension(48, 48));
        bt63.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt63ActionPerformed(evt);
            }
        });

        bt64.setBackground(Values.corBlocos);
        bt64.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt64.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt64.setPreferredSize(new java.awt.Dimension(48, 48));
        bt64.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt64ActionPerformed(evt);
            }
        });

        bt65.setBackground(Values.corBlocos);
        bt65.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt65.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt65.setPreferredSize(new java.awt.Dimension(48, 48));
        bt65.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt65ActionPerformed(evt);
            }
        });

        bt66.setBackground(Values.corBlocos);
        bt66.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt66.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt66.setPreferredSize(new java.awt.Dimension(48, 48));
        bt66.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt66ActionPerformed(evt);
            }
        });

        bt67.setBackground(Values.corBlocos);
        bt67.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt67.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt67.setPreferredSize(new java.awt.Dimension(48, 48));
        bt67.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt67ActionPerformed(evt);
            }
        });

        bt68.setBackground(Values.corBlocos);
        bt68.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt68.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt68.setPreferredSize(new java.awt.Dimension(48, 48));
        bt68.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt68ActionPerformed(evt);
            }
        });

        bt69.setBackground(Values.corBlocos);
        bt69.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt69.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt69.setPreferredSize(new java.awt.Dimension(48, 48));
        bt69.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt69ActionPerformed(evt);
            }
        });

        bt70.setBackground(Values.corBlocos);
        bt70.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt70.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt70.setPreferredSize(new java.awt.Dimension(48, 48));
        bt70.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt70ActionPerformed(evt);
            }
        });

        bt71.setBackground(Values.corBlocos);
        bt71.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt71.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt71.setPreferredSize(new java.awt.Dimension(48, 48));
        bt71.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt71ActionPerformed(evt);
            }
        });

        bt72.setBackground(Values.corBlocos);
        bt72.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt72.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt72.setPreferredSize(new java.awt.Dimension(48, 48));
        bt72.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt72ActionPerformed(evt);
            }
        });

        bt73.setBackground(Values.corBlocos);
        bt73.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt73.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt73.setPreferredSize(new java.awt.Dimension(48, 48));
        bt73.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt73ActionPerformed(evt);
            }
        });

        bt74.setBackground(Values.corBlocos);
        bt74.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt74.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt74.setPreferredSize(new java.awt.Dimension(48, 48));
        bt74.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt74ActionPerformed(evt);
            }
        });

        bt75.setBackground(Values.corBlocos);
        bt75.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt75.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt75.setPreferredSize(new java.awt.Dimension(48, 48));
        bt75.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt75ActionPerformed(evt);
            }
        });

        bt76.setBackground(Values.corBlocos);
        bt76.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt76.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt76.setPreferredSize(new java.awt.Dimension(48, 48));
        bt76.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt76ActionPerformed(evt);
            }
        });

        bt77.setBackground(Values.corBlocos);
        bt77.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt77.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt77.setPreferredSize(new java.awt.Dimension(48, 48));
        bt77.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt77ActionPerformed(evt);
            }
        });

        bt78.setBackground(Values.corBlocos);
        bt78.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt78.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt78.setPreferredSize(new java.awt.Dimension(48, 48));
        bt78.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt78ActionPerformed(evt);
            }
        });

        bt79.setBackground(Values.corBlocos);
        bt79.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt79.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt79.setPreferredSize(new java.awt.Dimension(48, 48));
        bt79.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt79ActionPerformed(evt);
            }
        });

        bt80.setBackground(Values.corBlocos);
        bt80.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt80.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt80.setPreferredSize(new java.awt.Dimension(48, 48));
        bt80.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt80ActionPerformed(evt);
            }
        });

        bt81.setBackground(Values.corBlocos);
        bt81.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt81.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt81.setPreferredSize(new java.awt.Dimension(48, 48));
        bt81.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt81ActionPerformed(evt);
            }
        });

        bt82.setBackground(Values.corBlocos);
        bt82.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt82.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt82.setPreferredSize(new java.awt.Dimension(48, 48));
        bt82.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt82ActionPerformed(evt);
            }
        });

        bt83.setBackground(Values.corBlocos);
        bt83.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt83.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt83.setPreferredSize(new java.awt.Dimension(48, 48));
        bt83.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt83ActionPerformed(evt);
            }
        });

        bt84.setBackground(Values.corBlocos);
        bt84.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt84.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt84.setPreferredSize(new java.awt.Dimension(48, 48));
        bt84.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt84ActionPerformed(evt);
            }
        });

        bt85.setBackground(Values.corBlocos);
        bt85.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt85.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt85.setPreferredSize(new java.awt.Dimension(48, 48));
        bt85.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt85ActionPerformed(evt);
            }
        });

        bt86.setBackground(Values.corBlocos);
        bt86.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt86.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt86.setPreferredSize(new java.awt.Dimension(48, 48));
        bt86.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt86ActionPerformed(evt);
            }
        });

        bt87.setBackground(Values.corBlocos);
        bt87.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt87.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt87.setPreferredSize(new java.awt.Dimension(48, 48));
        bt87.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt87ActionPerformed(evt);
            }
        });

        bt88.setBackground(Values.corBlocos);
        bt88.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt88.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt88.setPreferredSize(new java.awt.Dimension(48, 48));
        bt88.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt88ActionPerformed(evt);
            }
        });

        bt89.setBackground(Values.corBlocos);
        bt89.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt89.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt89.setPreferredSize(new java.awt.Dimension(48, 48));
        bt89.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt89ActionPerformed(evt);
            }
        });

        bt90.setBackground(Values.corBlocos);
        bt90.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt90.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt90.setPreferredSize(new java.awt.Dimension(48, 48));
        bt90.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt90ActionPerformed(evt);
            }
        });

        bt91.setBackground(Values.corBlocos);
        bt91.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt91.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt91.setPreferredSize(new java.awt.Dimension(48, 48));
        bt91.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt91ActionPerformed(evt);
            }
        });

        bt92.setBackground(Values.corBlocos);
        bt92.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt92.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt92.setPreferredSize(new java.awt.Dimension(48, 48));
        bt92.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt92ActionPerformed(evt);
            }
        });

        bt93.setBackground(Values.corBlocos);
        bt93.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt93.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt93.setPreferredSize(new java.awt.Dimension(48, 48));
        bt93.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt93ActionPerformed(evt);
            }
        });

        bt94.setBackground(Values.corBlocos);
        bt94.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt94.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt94.setPreferredSize(new java.awt.Dimension(48, 48));
        bt94.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt94ActionPerformed(evt);
            }
        });

        bt95.setBackground(Values.corBlocos);
        bt95.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt95.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt95.setPreferredSize(new java.awt.Dimension(48, 48));
        bt95.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt95ActionPerformed(evt);
            }
        });

        bt96.setBackground(Values.corBlocos);
        bt96.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt96.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt96.setPreferredSize(new java.awt.Dimension(48, 48));
        bt96.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt96ActionPerformed(evt);
            }
        });

        bt97.setBackground(Values.corBlocos);
        bt97.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt97.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt97.setPreferredSize(new java.awt.Dimension(48, 48));
        bt97.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt97ActionPerformed(evt);
            }
        });

        bt98.setBackground(Values.corBlocos);
        bt98.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt98.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt98.setPreferredSize(new java.awt.Dimension(48, 48));
        bt98.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt98ActionPerformed(evt);
            }
        });

        bt99.setBackground(Values.corBlocos);
        bt99.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt99.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt99.setPreferredSize(new java.awt.Dimension(48, 48));
        bt99.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt99ActionPerformed(evt);
            }
        });

        bt100.setBackground(Values.corBlocos);
        bt100.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt100.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt100.setPreferredSize(new java.awt.Dimension(48, 48));
        bt100.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt100ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout painelJogoNormalLayout = new javax.swing.GroupLayout(painelJogoNormal);
        painelJogoNormal.setLayout(painelJogoNormalLayout);
        painelJogoNormalLayout.setHorizontalGroup(
            painelJogoNormalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelJogoNormalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelJogoNormalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(painelJogoNormalLayout.createSequentialGroup()
                        .addComponent(bt1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(painelJogoNormalLayout.createSequentialGroup()
                        .addComponent(bt11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(painelJogoNormalLayout.createSequentialGroup()
                        .addComponent(bt21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(painelJogoNormalLayout.createSequentialGroup()
                        .addComponent(bt31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt36, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt38, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt40, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(painelJogoNormalLayout.createSequentialGroup()
                        .addComponent(bt41, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt42, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt43, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt44, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt45, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt46, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt47, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt48, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt49, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt50, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(painelJogoNormalLayout.createSequentialGroup()
                        .addComponent(bt51, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt52, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt53, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt54, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt55, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt56, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt57, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt58, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt59, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt60, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(painelJogoNormalLayout.createSequentialGroup()
                        .addComponent(bt61, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt62, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt63, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt64, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt65, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt66, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt67, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt68, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt69, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt70, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(painelJogoNormalLayout.createSequentialGroup()
                        .addComponent(bt71, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt72, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt73, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt74, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt75, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt76, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt77, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt78, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt79, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt80, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(painelJogoNormalLayout.createSequentialGroup()
                        .addComponent(bt81, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt82, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt83, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt84, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt85, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt86, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt87, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt88, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt89, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt90, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(painelJogoNormalLayout.createSequentialGroup()
                        .addComponent(bt91, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt92, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt93, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt94, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt95, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt96, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt97, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt98, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt99, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt100, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        painelJogoNormalLayout.setVerticalGroup(
            painelJogoNormalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelJogoNormalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelJogoNormalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelJogoNormalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelJogoNormalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelJogoNormalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt40, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt38, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt36, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelJogoNormalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt50, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt49, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt48, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt47, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt46, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt45, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt44, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt43, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt42, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt41, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelJogoNormalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt60, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt59, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt58, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt57, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt56, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt55, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt54, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt53, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt52, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt51, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelJogoNormalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt70, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt69, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt68, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt67, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt66, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt65, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt64, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt63, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt62, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt61, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelJogoNormalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt80, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt79, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt78, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt77, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt76, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt75, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt74, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt73, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt72, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt71, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelJogoNormalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt90, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt89, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt88, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt87, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt86, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt85, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt84, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt83, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt82, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt81, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(painelJogoNormalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt100, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt99, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt98, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt97, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt96, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt95, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt94, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt93, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt92, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt91, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        painelInfo.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Informações", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Comic Sans MS", 0, 16), new java.awt.Color(255, 0, 0))); // NOI18N

        painelPontuacao.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Pontuação", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Comic Sans MS", 0, 16), new java.awt.Color(0, 0, 0))); // NOI18N
        painelPontuacao.setToolTipText("Sua pontuação atual");
        painelPontuacao.setPreferredSize(new java.awt.Dimension(203, 70));

        lbPontuacao.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N

        javax.swing.GroupLayout painelPontuacaoLayout = new javax.swing.GroupLayout(painelPontuacao);
        painelPontuacao.setLayout(painelPontuacaoLayout);
        painelPontuacaoLayout.setHorizontalGroup(
            painelPontuacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelPontuacaoLayout.createSequentialGroup()
                .addGap(75, 75, 75)
                .addComponent(lbPontuacao)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        painelPontuacaoLayout.setVerticalGroup(
            painelPontuacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelPontuacaoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbPontuacao)
                .addContainerGap(28, Short.MAX_VALUE))
        );

        painelJogador.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Informações", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Comic Sans MS", 0, 16), new java.awt.Color(0, 0, 0))); // NOI18N
        painelJogador.setToolTipText("Informações extras sobre a partida");

        lbNivelDificuldade.setFont(new java.awt.Font("Comic Sans MS", 1, 15)); // NOI18N
        lbNivelDificuldade.setForeground(new java.awt.Color(255, 0, 0));
        lbNivelDificuldade.setText("Nível: Normal");

        avatarIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/blank_avatar.jpg"))); // NOI18N
        avatarIcon.setToolTipText(TabelaScores.getNickJogador());

        javax.swing.GroupLayout painelJogadorLayout = new javax.swing.GroupLayout(painelJogador);
        painelJogador.setLayout(painelJogadorLayout);
        painelJogadorLayout.setHorizontalGroup(
            painelJogadorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelJogadorLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelJogadorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(painelJogadorLayout.createSequentialGroup()
                        .addComponent(lbNivelDificuldade)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, painelJogadorLayout.createSequentialGroup()
                        .addGap(0, 52, Short.MAX_VALUE)
                        .addComponent(avatarIcon)
                        .addGap(57, 57, 57))))
        );
        painelJogadorLayout.setVerticalGroup(
            painelJogadorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, painelJogadorLayout.createSequentialGroup()
                .addGap(55, 55, 55)
                .addComponent(avatarIcon)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbNivelDificuldade)
                .addContainerGap())
        );

        painelMenuRapido.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Menu", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Comic Sans MS", 0, 16), new java.awt.Color(0, 0, 0))); // NOI18N
        painelMenuRapido.setToolTipText("Menu de opções rápidas");

        btRestart.setFont(new java.awt.Font("Comic Sans MS", 1, 16)); // NOI18N
        btRestart.setForeground(new java.awt.Color(255, 0, 0));
        btRestart.setText("Reiniciar Partida");
        btRestart.setToolTipText("Clique aqui para reiniciar a partida atual");
        btRestart.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btRestart.setPreferredSize(new java.awt.Dimension(81, 30));
        btRestart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btRestartActionPerformed(evt);
            }
        });

        btDesistir.setFont(new java.awt.Font("Comic Sans MS", 1, 16)); // NOI18N
        btDesistir.setForeground(new java.awt.Color(255, 0, 0));
        btDesistir.setText("Abandonar Partida");
        btDesistir.setToolTipText("Clique aqui para abandonar a partida atual");
        btDesistir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btDesistir.setPreferredSize(new java.awt.Dimension(81, 30));
        btDesistir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btDesistirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout painelMenuRapidoLayout = new javax.swing.GroupLayout(painelMenuRapido);
        painelMenuRapido.setLayout(painelMenuRapidoLayout);
        painelMenuRapidoLayout.setHorizontalGroup(
            painelMenuRapidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelMenuRapidoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelMenuRapidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btDesistir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btRestart, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        painelMenuRapidoLayout.setVerticalGroup(
            painelMenuRapidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelMenuRapidoLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(btRestart, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btDesistir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(29, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout painelInfoLayout = new javax.swing.GroupLayout(painelInfo);
        painelInfo.setLayout(painelInfoLayout);
        painelInfoLayout.setHorizontalGroup(
            painelInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, painelInfoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(painelJogador, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(painelMenuRapido, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(painelPontuacao, javax.swing.GroupLayout.DEFAULT_SIZE, 241, Short.MAX_VALUE))
                .addContainerGap())
        );
        painelInfoLayout.setVerticalGroup(
            painelInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelInfoLayout.createSequentialGroup()
                .addComponent(painelPontuacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(painelJogador, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(painelMenuRapido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(painelJogoNormal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(painelInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(painelInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(painelJogoNormal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

        
    private void btRestartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btRestartActionPerformed
        Values.recomecarPartida();
        dispose();
    }//GEN-LAST:event_btRestartActionPerformed

    private void btDesistirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btDesistirActionPerformed
        if(Values.voltarMenuPrincipal(TelaJogoNormal.this) == 0){
        dispose();
        }
    }//GEN-LAST:event_btDesistirActionPerformed

    private void bt1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt1ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt1, (byte)0, (byte)0);
    }//GEN-LAST:event_bt1ActionPerformed

    private void bt2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt2ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt2, (byte)0, (byte)1);
    }//GEN-LAST:event_bt2ActionPerformed

    private void bt3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt3ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt3, (byte)0, (byte)2);
    }//GEN-LAST:event_bt3ActionPerformed

    private void bt4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt4ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt4, (byte)0, (byte)3);
    }//GEN-LAST:event_bt4ActionPerformed

    private void bt5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt5ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt5, (byte)0, (byte)4);
    }//GEN-LAST:event_bt5ActionPerformed

    private void bt6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt6ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt6, (byte)0, (byte)5);
    }//GEN-LAST:event_bt6ActionPerformed

    private void bt7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt7ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt7, (byte)0, (byte)6);
    }//GEN-LAST:event_bt7ActionPerformed

    private void bt8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt8ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt8, (byte)0, (byte)7);
    }//GEN-LAST:event_bt8ActionPerformed

    private void bt10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt10ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt10, (byte)0, (byte)9);
    }//GEN-LAST:event_bt10ActionPerformed

    private void bt9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt9ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt9, (byte)0, (byte)8);
    }//GEN-LAST:event_bt9ActionPerformed

    private void bt11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt11ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt11, (byte)1, (byte)0);
    }//GEN-LAST:event_bt11ActionPerformed

    private void bt12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt12ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt12, (byte)1, (byte)1);
    }//GEN-LAST:event_bt12ActionPerformed

    private void bt13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt13ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt13, (byte)1, (byte)2);
    }//GEN-LAST:event_bt13ActionPerformed

    private void bt14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt14ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt14, (byte)1, (byte)3);
    }//GEN-LAST:event_bt14ActionPerformed

    private void bt15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt15ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt15, (byte)1, (byte)4);
    }//GEN-LAST:event_bt15ActionPerformed

    private void bt16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt16ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt16, (byte)1, (byte)5);
    }//GEN-LAST:event_bt16ActionPerformed

    private void bt17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt17ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt17, (byte)1, (byte)6);
    }//GEN-LAST:event_bt17ActionPerformed

    private void bt18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt18ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt18, (byte)1, (byte)7);
    }//GEN-LAST:event_bt18ActionPerformed

    private void bt19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt19ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt19, (byte)1, (byte)8);
    }//GEN-LAST:event_bt19ActionPerformed

    private void bt20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt20ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt20, (byte)1, (byte)9);
    }//GEN-LAST:event_bt20ActionPerformed

    private void bt21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt21ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt21, (byte)2, (byte)0);
    }//GEN-LAST:event_bt21ActionPerformed

    private void bt22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt22ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt22, (byte)2, (byte)1);
    }//GEN-LAST:event_bt22ActionPerformed

    private void bt23ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt23ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt23, (byte)2, (byte)2);
    }//GEN-LAST:event_bt23ActionPerformed

    private void bt24ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt24ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt24, (byte)2, (byte)3);
    }//GEN-LAST:event_bt24ActionPerformed

    private void bt25ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt25ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt25, (byte)2, (byte)4);
    }//GEN-LAST:event_bt25ActionPerformed

    private void bt26ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt26ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt26, (byte)2, (byte)5);
    }//GEN-LAST:event_bt26ActionPerformed

    private void bt27ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt27ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt27, (byte)2, (byte)6);
    }//GEN-LAST:event_bt27ActionPerformed

    private void bt28ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt28ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt28, (byte)2, (byte)7);
    }//GEN-LAST:event_bt28ActionPerformed

    private void bt29ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt29ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt29, (byte)2, (byte)8);
    }//GEN-LAST:event_bt29ActionPerformed

    private void bt30ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt30ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt30, (byte)2, (byte)9);
    }//GEN-LAST:event_bt30ActionPerformed

    private void bt31ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt31ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt31, (byte)3, (byte)0);
    }//GEN-LAST:event_bt31ActionPerformed

    private void bt32ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt32ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt32, (byte)3, (byte)1);
    }//GEN-LAST:event_bt32ActionPerformed

    private void bt33ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt33ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt33, (byte)3, (byte)2);
    }//GEN-LAST:event_bt33ActionPerformed

    private void bt34ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt34ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt34, (byte)3, (byte)3);
    }//GEN-LAST:event_bt34ActionPerformed

    private void bt35ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt35ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt35, (byte)3, (byte)4);
    }//GEN-LAST:event_bt35ActionPerformed

    private void bt36ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt36ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt36, (byte)3, (byte)5);
    }//GEN-LAST:event_bt36ActionPerformed

    private void bt37ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt37ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt37, (byte)3, (byte)6);
    }//GEN-LAST:event_bt37ActionPerformed

    private void bt38ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt38ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt38, (byte)3, (byte)7);
    }//GEN-LAST:event_bt38ActionPerformed

    private void bt39ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt39ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt39, (byte)3, (byte)8);
    }//GEN-LAST:event_bt39ActionPerformed

    private void bt40ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt40ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt40, (byte)3, (byte)9);
    }//GEN-LAST:event_bt40ActionPerformed

    private void bt41ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt41ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt41, (byte)4, (byte)0);
    }//GEN-LAST:event_bt41ActionPerformed

    private void bt42ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt42ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt42, (byte)4, (byte)1);
    }//GEN-LAST:event_bt42ActionPerformed

    private void bt43ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt43ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt43, (byte)4, (byte)2);
    }//GEN-LAST:event_bt43ActionPerformed

    private void bt44ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt44ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt44, (byte)4, (byte)3);
    }//GEN-LAST:event_bt44ActionPerformed

    private void bt45ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt45ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt45, (byte)4, (byte)4);
    }//GEN-LAST:event_bt45ActionPerformed

    private void bt46ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt46ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt46, (byte)4, (byte)5);
    }//GEN-LAST:event_bt46ActionPerformed

    private void bt47ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt47ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt47, (byte)4, (byte)6);
    }//GEN-LAST:event_bt47ActionPerformed

    private void bt48ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt48ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt48, (byte)4, (byte)7);
    }//GEN-LAST:event_bt48ActionPerformed

    private void bt49ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt49ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt49, (byte)4, (byte)8);
    }//GEN-LAST:event_bt49ActionPerformed

    private void bt50ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt50ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt50, (byte)4, (byte)9);
    }//GEN-LAST:event_bt50ActionPerformed

    private void bt51ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt51ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt51, (byte)5, (byte)0);
    }//GEN-LAST:event_bt51ActionPerformed

    private void bt52ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt52ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt52, (byte)5, (byte)1);
    }//GEN-LAST:event_bt52ActionPerformed

    private void bt53ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt53ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt53, (byte)5, (byte)2);
    }//GEN-LAST:event_bt53ActionPerformed

    private void bt54ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt54ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt54, (byte)5, (byte)3);
    }//GEN-LAST:event_bt54ActionPerformed

    private void bt55ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt55ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt55, (byte)5, (byte)4);
    }//GEN-LAST:event_bt55ActionPerformed

    private void bt56ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt56ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt56, (byte)5, (byte)5);
    }//GEN-LAST:event_bt56ActionPerformed

    private void bt57ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt57ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt57, (byte)5, (byte)6);
    }//GEN-LAST:event_bt57ActionPerformed

    private void bt58ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt58ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt58, (byte)5, (byte)7);
    }//GEN-LAST:event_bt58ActionPerformed

    private void bt59ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt59ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt59, (byte)5, (byte)8);
    }//GEN-LAST:event_bt59ActionPerformed

    private void bt60ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt60ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt60, (byte)5, (byte)9);
    }//GEN-LAST:event_bt60ActionPerformed

    private void bt61ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt61ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt61, (byte)6, (byte)0);
    }//GEN-LAST:event_bt61ActionPerformed

    private void bt62ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt62ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt62, (byte)6, (byte)1);
    }//GEN-LAST:event_bt62ActionPerformed

    private void bt63ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt63ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt63, (byte)6, (byte)2);
    }//GEN-LAST:event_bt63ActionPerformed

    private void bt64ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt64ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt64, (byte)6, (byte)3);
    }//GEN-LAST:event_bt64ActionPerformed

    private void bt65ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt65ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt65, (byte)6, (byte)4);
    }//GEN-LAST:event_bt65ActionPerformed

    private void bt66ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt66ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt66, (byte)6, (byte)5);
    }//GEN-LAST:event_bt66ActionPerformed

    private void bt67ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt67ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt67, (byte)6, (byte)6);
    }//GEN-LAST:event_bt67ActionPerformed

    private void bt68ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt68ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt68, (byte)6, (byte)7);
    }//GEN-LAST:event_bt68ActionPerformed

    private void bt69ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt69ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt69, (byte)6, (byte)8);
    }//GEN-LAST:event_bt69ActionPerformed

    private void bt70ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt70ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt70, (byte)6, (byte)9);
    }//GEN-LAST:event_bt70ActionPerformed

    private void bt71ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt71ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt71, (byte)7, (byte)0);
    }//GEN-LAST:event_bt71ActionPerformed

    private void bt72ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt72ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt72, (byte)7, (byte)1);
    }//GEN-LAST:event_bt72ActionPerformed

    private void bt73ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt73ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt73, (byte)7, (byte)2);
    }//GEN-LAST:event_bt73ActionPerformed

    private void bt74ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt74ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt74, (byte)7, (byte)3);
    }//GEN-LAST:event_bt74ActionPerformed

    private void bt75ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt75ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt75, (byte)7, (byte)4);
    }//GEN-LAST:event_bt75ActionPerformed

    private void bt76ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt76ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt76, (byte)7, (byte)5);
    }//GEN-LAST:event_bt76ActionPerformed

    private void bt77ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt77ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt77, (byte)7, (byte)6);
    }//GEN-LAST:event_bt77ActionPerformed

    private void bt78ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt78ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt78, (byte)7, (byte)7);
    }//GEN-LAST:event_bt78ActionPerformed

    private void bt79ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt79ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt79, (byte)7, (byte)8);
    }//GEN-LAST:event_bt79ActionPerformed

    private void bt80ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt80ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt80, (byte)7, (byte)9);
    }//GEN-LAST:event_bt80ActionPerformed

    private void bt81ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt81ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt81, (byte)8, (byte)0);
    }//GEN-LAST:event_bt81ActionPerformed

    private void bt82ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt82ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt82, (byte)8, (byte)1);
    }//GEN-LAST:event_bt82ActionPerformed

    private void bt83ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt83ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt83, (byte)8, (byte)2);
    }//GEN-LAST:event_bt83ActionPerformed

    private void bt84ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt84ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt84, (byte)8, (byte)3);
    }//GEN-LAST:event_bt84ActionPerformed

    private void bt85ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt85ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt85, (byte)8, (byte)4);
    }//GEN-LAST:event_bt85ActionPerformed

    private void bt86ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt86ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt86, (byte)8, (byte)5);
    }//GEN-LAST:event_bt86ActionPerformed

    private void bt87ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt87ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt87, (byte)8, (byte)6);
    }//GEN-LAST:event_bt87ActionPerformed

    private void bt88ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt88ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt88, (byte)8, (byte)7);
    }//GEN-LAST:event_bt88ActionPerformed

    private void bt89ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt89ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt89, (byte)8, (byte)8);
    }//GEN-LAST:event_bt89ActionPerformed

    private void bt90ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt90ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt90, (byte)8, (byte)9);
    }//GEN-LAST:event_bt90ActionPerformed

    private void bt91ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt91ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt91, (byte)9, (byte)0);
    }//GEN-LAST:event_bt91ActionPerformed

    private void bt92ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt92ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt92, (byte)9, (byte)1);
    }//GEN-LAST:event_bt92ActionPerformed

    private void bt93ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt93ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt93, (byte)9, (byte)2);
    }//GEN-LAST:event_bt93ActionPerformed

    private void bt94ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt94ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt94, (byte)9, (byte)3);
    }//GEN-LAST:event_bt94ActionPerformed

    private void bt95ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt95ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt95, (byte)9, (byte)4);
    }//GEN-LAST:event_bt95ActionPerformed

    private void bt96ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt96ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt96, (byte)9, (byte)5);
    }//GEN-LAST:event_bt96ActionPerformed

    private void bt97ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt97ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt97, (byte)9, (byte)6);
    }//GEN-LAST:event_bt97ActionPerformed

    private void bt98ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt98ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt98, (byte)9, (byte)7);
    }//GEN-LAST:event_bt98ActionPerformed

    private void bt99ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt99ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt99, (byte)9, (byte)8);
    }//GEN-LAST:event_bt99ActionPerformed

    private void bt100ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt100ActionPerformed
        Values.clickBloco(TelaJogoNormal.this, lbPontuacao, bt100, (byte)9, (byte)9);
    }//GEN-LAST:event_bt100ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel avatarIcon;
    private javax.swing.JButton bt1;
    private javax.swing.JButton bt10;
    private javax.swing.JButton bt100;
    private javax.swing.JButton bt11;
    private javax.swing.JButton bt12;
    private javax.swing.JButton bt13;
    private javax.swing.JButton bt14;
    private javax.swing.JButton bt15;
    private javax.swing.JButton bt16;
    private javax.swing.JButton bt17;
    private javax.swing.JButton bt18;
    private javax.swing.JButton bt19;
    private javax.swing.JButton bt2;
    private javax.swing.JButton bt20;
    private javax.swing.JButton bt21;
    private javax.swing.JButton bt22;
    private javax.swing.JButton bt23;
    private javax.swing.JButton bt24;
    private javax.swing.JButton bt25;
    private javax.swing.JButton bt26;
    private javax.swing.JButton bt27;
    private javax.swing.JButton bt28;
    private javax.swing.JButton bt29;
    private javax.swing.JButton bt3;
    private javax.swing.JButton bt30;
    private javax.swing.JButton bt31;
    private javax.swing.JButton bt32;
    private javax.swing.JButton bt33;
    private javax.swing.JButton bt34;
    private javax.swing.JButton bt35;
    private javax.swing.JButton bt36;
    private javax.swing.JButton bt37;
    private javax.swing.JButton bt38;
    private javax.swing.JButton bt39;
    private javax.swing.JButton bt4;
    private javax.swing.JButton bt40;
    private javax.swing.JButton bt41;
    private javax.swing.JButton bt42;
    private javax.swing.JButton bt43;
    private javax.swing.JButton bt44;
    private javax.swing.JButton bt45;
    private javax.swing.JButton bt46;
    private javax.swing.JButton bt47;
    private javax.swing.JButton bt48;
    private javax.swing.JButton bt49;
    private javax.swing.JButton bt5;
    private javax.swing.JButton bt50;
    private javax.swing.JButton bt51;
    private javax.swing.JButton bt52;
    private javax.swing.JButton bt53;
    private javax.swing.JButton bt54;
    private javax.swing.JButton bt55;
    private javax.swing.JButton bt56;
    private javax.swing.JButton bt57;
    private javax.swing.JButton bt58;
    private javax.swing.JButton bt59;
    private javax.swing.JButton bt6;
    private javax.swing.JButton bt60;
    private javax.swing.JButton bt61;
    private javax.swing.JButton bt62;
    private javax.swing.JButton bt63;
    private javax.swing.JButton bt64;
    private javax.swing.JButton bt65;
    private javax.swing.JButton bt66;
    private javax.swing.JButton bt67;
    private javax.swing.JButton bt68;
    private javax.swing.JButton bt69;
    private javax.swing.JButton bt7;
    private javax.swing.JButton bt70;
    private javax.swing.JButton bt71;
    private javax.swing.JButton bt72;
    private javax.swing.JButton bt73;
    private javax.swing.JButton bt74;
    private javax.swing.JButton bt75;
    private javax.swing.JButton bt76;
    private javax.swing.JButton bt77;
    private javax.swing.JButton bt78;
    private javax.swing.JButton bt79;
    private javax.swing.JButton bt8;
    private javax.swing.JButton bt80;
    private javax.swing.JButton bt81;
    private javax.swing.JButton bt82;
    private javax.swing.JButton bt83;
    private javax.swing.JButton bt84;
    private javax.swing.JButton bt85;
    private javax.swing.JButton bt86;
    private javax.swing.JButton bt87;
    private javax.swing.JButton bt88;
    private javax.swing.JButton bt89;
    private javax.swing.JButton bt9;
    private javax.swing.JButton bt90;
    private javax.swing.JButton bt91;
    private javax.swing.JButton bt92;
    private javax.swing.JButton bt93;
    private javax.swing.JButton bt94;
    private javax.swing.JButton bt95;
    private javax.swing.JButton bt96;
    private javax.swing.JButton bt97;
    private javax.swing.JButton bt98;
    private javax.swing.JButton bt99;
    private javax.swing.JButton btDesistir;
    private javax.swing.JButton btRestart;
    private javax.swing.JLabel lbNivelDificuldade;
    private javax.swing.JLabel lbPontuacao;
    private javax.swing.JPanel painelInfo;
    private javax.swing.JPanel painelJogador;
    private javax.swing.JPanel painelJogoNormal;
    private javax.swing.JPanel painelMenuRapido;
    private javax.swing.JPanel painelPontuacao;
    // End of variables declaration//GEN-END:variables
}
