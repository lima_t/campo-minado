package CampoMinado.Telas;

import CampoMinado.Classes.*;
import CampoMinado.Classes.Tabelas.TabelaScores;

/**
 * @author Tarcísio de Lima
 * @version 1.0.0
 * 
 * Essa Tela é chamada apenas se a opção do nível fácil do jogo estiver selecionada, nela temos um campo 6x6 onde
 * o jogador deverá tentar captaurar todos pontos
 */
public class TelaJogoFacil extends javax.swing.JFrame {

    public TelaJogoFacil() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.setIconImage(new Values().iconeJanela(DataImage.ICONE_NOVO_JOGO));
        new Values().iconeAvatarJogador(TelaJogoFacil.this, avatarIcon);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        painelJogoFacil = new javax.swing.JPanel();
        bt1 = new javax.swing.JButton();
        bt2 = new javax.swing.JButton();
        bt3 = new javax.swing.JButton();
        bt4 = new javax.swing.JButton();
        bt5 = new javax.swing.JButton();
        bt6 = new javax.swing.JButton();
        bt7 = new javax.swing.JButton();
        bt8 = new javax.swing.JButton();
        bt9 = new javax.swing.JButton();
        bt10 = new javax.swing.JButton();
        bt11 = new javax.swing.JButton();
        bt12 = new javax.swing.JButton();
        bt13 = new javax.swing.JButton();
        bt14 = new javax.swing.JButton();
        bt15 = new javax.swing.JButton();
        bt16 = new javax.swing.JButton();
        bt17 = new javax.swing.JButton();
        bt18 = new javax.swing.JButton();
        bt19 = new javax.swing.JButton();
        bt20 = new javax.swing.JButton();
        bt21 = new javax.swing.JButton();
        bt22 = new javax.swing.JButton();
        bt23 = new javax.swing.JButton();
        bt24 = new javax.swing.JButton();
        bt25 = new javax.swing.JButton();
        bt26 = new javax.swing.JButton();
        bt27 = new javax.swing.JButton();
        bt28 = new javax.swing.JButton();
        bt29 = new javax.swing.JButton();
        bt30 = new javax.swing.JButton();
        bt31 = new javax.swing.JButton();
        bt32 = new javax.swing.JButton();
        bt33 = new javax.swing.JButton();
        bt34 = new javax.swing.JButton();
        bt35 = new javax.swing.JButton();
        bt36 = new javax.swing.JButton();
        painelInfo = new javax.swing.JPanel();
        painelPontuacao = new javax.swing.JPanel();
        lbPontuacao = new javax.swing.JLabel();
        painelJogador = new javax.swing.JPanel();
        lbNivelDificuldade = new javax.swing.JLabel();
        avatarIcon = new javax.swing.JLabel();
        painelMenuRapido = new javax.swing.JPanel();
        btRestart = new javax.swing.JButton();
        btDesistir = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Minas Nível Fácil");
        setResizable(false);

        painelJogoFacil.setBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(255, 0, 0), new java.awt.Color(102, 102, 102)));

        bt1.setBackground(Values.corBlocos);
        bt1.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt1.setPreferredSize(new java.awt.Dimension(48, 48));
        bt1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt1ActionPerformed(evt);
            }
        });

        bt2.setBackground(Values.corBlocos);
        bt2.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt2.setPreferredSize(new java.awt.Dimension(48, 48));
        bt2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt2ActionPerformed(evt);
            }
        });

        bt3.setBackground(Values.corBlocos);
        bt3.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt3.setPreferredSize(new java.awt.Dimension(48, 48));
        bt3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt3ActionPerformed(evt);
            }
        });

        bt4.setBackground(Values.corBlocos);
        bt4.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt4.setPreferredSize(new java.awt.Dimension(48, 48));
        bt4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt4ActionPerformed(evt);
            }
        });

        bt5.setBackground(Values.corBlocos);
        bt5.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt5.setPreferredSize(new java.awt.Dimension(48, 48));
        bt5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt5ActionPerformed(evt);
            }
        });

        bt6.setBackground(Values.corBlocos);
        bt6.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt6.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt6.setPreferredSize(new java.awt.Dimension(48, 48));
        bt6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt6ActionPerformed(evt);
            }
        });

        bt7.setBackground(Values.corBlocos);
        bt7.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt7.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt7.setPreferredSize(new java.awt.Dimension(48, 48));
        bt7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt7ActionPerformed(evt);
            }
        });

        bt8.setBackground(Values.corBlocos);
        bt8.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt8.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt8.setPreferredSize(new java.awt.Dimension(48, 48));
        bt8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt8ActionPerformed(evt);
            }
        });

        bt9.setBackground(Values.corBlocos);
        bt9.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt9.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt9.setPreferredSize(new java.awt.Dimension(48, 48));
        bt9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt9ActionPerformed(evt);
            }
        });

        bt10.setBackground(Values.corBlocos);
        bt10.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt10.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt10.setPreferredSize(new java.awt.Dimension(48, 48));
        bt10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt10ActionPerformed(evt);
            }
        });

        bt11.setBackground(Values.corBlocos);
        bt11.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt11.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt11.setPreferredSize(new java.awt.Dimension(48, 48));
        bt11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt11ActionPerformed(evt);
            }
        });

        bt12.setBackground(Values.corBlocos);
        bt12.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt12.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt12.setPreferredSize(new java.awt.Dimension(48, 48));
        bt12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt12ActionPerformed(evt);
            }
        });

        bt13.setBackground(Values.corBlocos);
        bt13.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt13.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt13.setPreferredSize(new java.awt.Dimension(48, 48));
        bt13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt13ActionPerformed(evt);
            }
        });

        bt14.setBackground(Values.corBlocos);
        bt14.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt14.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt14.setPreferredSize(new java.awt.Dimension(48, 48));
        bt14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt14ActionPerformed(evt);
            }
        });

        bt15.setBackground(Values.corBlocos);
        bt15.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt15.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt15.setPreferredSize(new java.awt.Dimension(48, 48));
        bt15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt15ActionPerformed(evt);
            }
        });

        bt16.setBackground(Values.corBlocos);
        bt16.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt16.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt16.setPreferredSize(new java.awt.Dimension(48, 48));
        bt16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt16ActionPerformed(evt);
            }
        });

        bt17.setBackground(Values.corBlocos);
        bt17.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt17.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt17.setPreferredSize(new java.awt.Dimension(48, 48));
        bt17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt17ActionPerformed(evt);
            }
        });

        bt18.setBackground(Values.corBlocos);
        bt18.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt18.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt18.setPreferredSize(new java.awt.Dimension(48, 48));
        bt18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt18ActionPerformed(evt);
            }
        });

        bt19.setBackground(Values.corBlocos);
        bt19.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt19.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt19.setPreferredSize(new java.awt.Dimension(48, 48));
        bt19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt19ActionPerformed(evt);
            }
        });

        bt20.setBackground(Values.corBlocos);
        bt20.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt20.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt20.setPreferredSize(new java.awt.Dimension(48, 48));
        bt20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt20ActionPerformed(evt);
            }
        });

        bt21.setBackground(Values.corBlocos);
        bt21.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt21.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt21.setPreferredSize(new java.awt.Dimension(48, 48));
        bt21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt21ActionPerformed(evt);
            }
        });

        bt22.setBackground(Values.corBlocos);
        bt22.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt22.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt22.setPreferredSize(new java.awt.Dimension(48, 48));
        bt22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt22ActionPerformed(evt);
            }
        });

        bt23.setBackground(Values.corBlocos);
        bt23.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt23.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt23.setPreferredSize(new java.awt.Dimension(48, 48));
        bt23.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt23ActionPerformed(evt);
            }
        });

        bt24.setBackground(Values.corBlocos);
        bt24.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt24.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt24.setPreferredSize(new java.awt.Dimension(48, 48));
        bt24.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt24ActionPerformed(evt);
            }
        });

        bt25.setBackground(Values.corBlocos);
        bt25.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt25.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt25.setPreferredSize(new java.awt.Dimension(48, 48));
        bt25.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt25ActionPerformed(evt);
            }
        });

        bt26.setBackground(Values.corBlocos);
        bt26.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt26.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt26.setPreferredSize(new java.awt.Dimension(48, 48));
        bt26.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt26ActionPerformed(evt);
            }
        });

        bt27.setBackground(Values.corBlocos);
        bt27.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt27.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt27.setPreferredSize(new java.awt.Dimension(48, 48));
        bt27.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt27ActionPerformed(evt);
            }
        });

        bt28.setBackground(Values.corBlocos);
        bt28.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt28.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt28.setPreferredSize(new java.awt.Dimension(48, 48));
        bt28.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt28ActionPerformed(evt);
            }
        });

        bt29.setBackground(Values.corBlocos);
        bt29.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt29.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt29.setPreferredSize(new java.awt.Dimension(48, 48));
        bt29.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt29ActionPerformed(evt);
            }
        });

        bt30.setBackground(Values.corBlocos);
        bt30.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt30.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt30.setPreferredSize(new java.awt.Dimension(48, 48));
        bt30.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt30ActionPerformed(evt);
            }
        });

        bt31.setBackground(Values.corBlocos);
        bt31.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt31.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt31.setPreferredSize(new java.awt.Dimension(48, 48));
        bt31.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt31ActionPerformed(evt);
            }
        });

        bt32.setBackground(Values.corBlocos);
        bt32.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt32.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt32.setPreferredSize(new java.awt.Dimension(48, 48));
        bt32.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt32ActionPerformed(evt);
            }
        });

        bt33.setBackground(Values.corBlocos);
        bt33.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt33.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt33.setPreferredSize(new java.awt.Dimension(48, 48));
        bt33.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt33ActionPerformed(evt);
            }
        });

        bt34.setBackground(Values.corBlocos);
        bt34.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt34.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt34.setPreferredSize(new java.awt.Dimension(48, 48));
        bt34.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt34ActionPerformed(evt);
            }
        });

        bt35.setBackground(Values.corBlocos);
        bt35.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt35.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt35.setPreferredSize(new java.awt.Dimension(48, 48));
        bt35.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt35ActionPerformed(evt);
            }
        });

        bt36.setBackground(Values.corBlocos);
        bt36.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt36.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt36.setPreferredSize(new java.awt.Dimension(48, 48));
        bt36.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt36ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout painelJogoFacilLayout = new javax.swing.GroupLayout(painelJogoFacil);
        painelJogoFacil.setLayout(painelJogoFacilLayout);
        painelJogoFacilLayout.setHorizontalGroup(
            painelJogoFacilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelJogoFacilLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelJogoFacilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(painelJogoFacilLayout.createSequentialGroup()
                        .addComponent(bt1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(painelJogoFacilLayout.createSequentialGroup()
                        .addComponent(bt7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(painelJogoFacilLayout.createSequentialGroup()
                        .addComponent(bt13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(painelJogoFacilLayout.createSequentialGroup()
                        .addComponent(bt19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(painelJogoFacilLayout.createSequentialGroup()
                        .addComponent(bt25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(painelJogoFacilLayout.createSequentialGroup()
                        .addComponent(bt31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(bt36, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        painelJogoFacilLayout.setVerticalGroup(
            painelJogoFacilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelJogoFacilLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelJogoFacilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelJogoFacilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelJogoFacilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelJogoFacilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelJogoFacilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelJogoFacilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt36, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        painelInfo.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Informações", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Comic Sans MS", 0, 16), new java.awt.Color(255, 0, 0))); // NOI18N

        painelPontuacao.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Pontuação", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Comic Sans MS", 0, 16), new java.awt.Color(0, 0, 0))); // NOI18N
        painelPontuacao.setToolTipText("Sua pontuação atual");
        painelPontuacao.setPreferredSize(new java.awt.Dimension(203, 70));

        lbPontuacao.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N

        javax.swing.GroupLayout painelPontuacaoLayout = new javax.swing.GroupLayout(painelPontuacao);
        painelPontuacao.setLayout(painelPontuacaoLayout);
        painelPontuacaoLayout.setHorizontalGroup(
            painelPontuacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelPontuacaoLayout.createSequentialGroup()
                .addGap(75, 75, 75)
                .addComponent(lbPontuacao)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        painelPontuacaoLayout.setVerticalGroup(
            painelPontuacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelPontuacaoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbPontuacao)
                .addContainerGap(28, Short.MAX_VALUE))
        );

        painelJogador.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Informações", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Comic Sans MS", 0, 16), new java.awt.Color(0, 0, 0))); // NOI18N
        painelJogador.setToolTipText("Informações extras sobre a partida");

        lbNivelDificuldade.setFont(new java.awt.Font("Comic Sans MS", 1, 15)); // NOI18N
        lbNivelDificuldade.setForeground(new java.awt.Color(255, 0, 0));
        lbNivelDificuldade.setText("Nível: Fácil");

        avatarIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/blank_avatar.jpg"))); // NOI18N
        avatarIcon.setToolTipText(TabelaScores.getNickJogador());

        javax.swing.GroupLayout painelJogadorLayout = new javax.swing.GroupLayout(painelJogador);
        painelJogador.setLayout(painelJogadorLayout);
        painelJogadorLayout.setHorizontalGroup(
            painelJogadorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelJogadorLayout.createSequentialGroup()
                .addGroup(painelJogadorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(painelJogadorLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(lbNivelDificuldade))
                    .addGroup(painelJogadorLayout.createSequentialGroup()
                        .addGap(59, 59, 59)
                        .addComponent(avatarIcon)))
                .addContainerGap(62, Short.MAX_VALUE))
        );
        painelJogadorLayout.setVerticalGroup(
            painelJogadorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, painelJogadorLayout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addComponent(avatarIcon)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbNivelDificuldade)
                .addContainerGap())
        );

        painelMenuRapido.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Menu", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Comic Sans MS", 0, 16), new java.awt.Color(0, 0, 0))); // NOI18N
        painelMenuRapido.setToolTipText("Menu de opções rápidas");

        btRestart.setFont(new java.awt.Font("Comic Sans MS", 1, 16)); // NOI18N
        btRestart.setForeground(new java.awt.Color(255, 0, 0));
        btRestart.setText("Reiniciar Partida");
        btRestart.setToolTipText("Clique aqui para reiniciar a partida atual");
        btRestart.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btRestart.setPreferredSize(new java.awt.Dimension(81, 30));
        btRestart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btRestartActionPerformed(evt);
            }
        });

        btDesistir.setFont(new java.awt.Font("Comic Sans MS", 1, 16)); // NOI18N
        btDesistir.setForeground(new java.awt.Color(255, 0, 0));
        btDesistir.setText("Abandonar Partida");
        btDesistir.setToolTipText("Clique aqui para abandonar a partida atual");
        btDesistir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btDesistir.setPreferredSize(new java.awt.Dimension(81, 30));
        btDesistir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btDesistirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout painelMenuRapidoLayout = new javax.swing.GroupLayout(painelMenuRapido);
        painelMenuRapido.setLayout(painelMenuRapidoLayout);
        painelMenuRapidoLayout.setHorizontalGroup(
            painelMenuRapidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelMenuRapidoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelMenuRapidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btDesistir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btRestart, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        painelMenuRapidoLayout.setVerticalGroup(
            painelMenuRapidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelMenuRapidoLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(btRestart, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btDesistir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(29, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout painelInfoLayout = new javax.swing.GroupLayout(painelInfo);
        painelInfo.setLayout(painelInfoLayout);
        painelInfoLayout.setHorizontalGroup(
            painelInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, painelInfoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(painelJogador, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(painelMenuRapido, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(painelPontuacao, javax.swing.GroupLayout.DEFAULT_SIZE, 241, Short.MAX_VALUE))
                .addContainerGap())
        );
        painelInfoLayout.setVerticalGroup(
            painelInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelInfoLayout.createSequentialGroup()
                .addComponent(painelPontuacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(painelJogador, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(painelMenuRapido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(painelJogoFacil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(painelInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(82, 82, 82)
                        .addComponent(painelJogoFacil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 88, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(painelInfo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btRestartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btRestartActionPerformed
        Values.recomecarPartida();
        dispose();
    }//GEN-LAST:event_btRestartActionPerformed

    private void btDesistirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btDesistirActionPerformed
        if(Values.voltarMenuPrincipal(TelaJogoFacil.this) == 0){
        dispose();
        }
    }//GEN-LAST:event_btDesistirActionPerformed

    private void bt1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt1ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt1, (byte)0, (byte)0);
    }//GEN-LAST:event_bt1ActionPerformed

    private void bt2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt2ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt2, (byte)0, (byte)1);
    }//GEN-LAST:event_bt2ActionPerformed

    private void bt3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt3ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt3, (byte)0, (byte)2);
    }//GEN-LAST:event_bt3ActionPerformed

    private void bt4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt4ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt4, (byte)0, (byte)3);
    }//GEN-LAST:event_bt4ActionPerformed

    private void bt5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt5ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt5, (byte)0, (byte)4);
    }//GEN-LAST:event_bt5ActionPerformed

    private void bt6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt6ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt6, (byte)0, (byte)5);
    }//GEN-LAST:event_bt6ActionPerformed

    private void bt7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt7ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt7, (byte)1, (byte)0);
    }//GEN-LAST:event_bt7ActionPerformed

    private void bt8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt8ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt8, (byte)1, (byte)1);
    }//GEN-LAST:event_bt8ActionPerformed

    private void bt9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt9ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt9, (byte)1, (byte)2);
    }//GEN-LAST:event_bt9ActionPerformed

    private void bt10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt10ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt10, (byte)1, (byte)3);
    }//GEN-LAST:event_bt10ActionPerformed

    private void bt11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt11ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt11, (byte)1, (byte)4);
    }//GEN-LAST:event_bt11ActionPerformed

    private void bt12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt12ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt12, (byte)1, (byte)5);
    }//GEN-LAST:event_bt12ActionPerformed

    private void bt13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt13ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt13, (byte)2, (byte)0);
    }//GEN-LAST:event_bt13ActionPerformed

    private void bt14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt14ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt14, (byte)2, (byte)1);
    }//GEN-LAST:event_bt14ActionPerformed

    private void bt15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt15ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt15, (byte)2, (byte)2);
    }//GEN-LAST:event_bt15ActionPerformed

    private void bt16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt16ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt16, (byte)2, (byte)3);
    }//GEN-LAST:event_bt16ActionPerformed

    private void bt17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt17ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt17, (byte)2, (byte)4);
    }//GEN-LAST:event_bt17ActionPerformed

    private void bt18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt18ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt18, (byte)2, (byte)5);
    }//GEN-LAST:event_bt18ActionPerformed

    private void bt19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt19ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt19, (byte)3, (byte)0);
    }//GEN-LAST:event_bt19ActionPerformed

    private void bt20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt20ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt20, (byte)3, (byte)1);
    }//GEN-LAST:event_bt20ActionPerformed

    private void bt21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt21ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt21, (byte)3, (byte)2);
    }//GEN-LAST:event_bt21ActionPerformed

    private void bt22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt22ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt22, (byte)3, (byte)3);
    }//GEN-LAST:event_bt22ActionPerformed

    private void bt23ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt23ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt23, (byte)3, (byte)4);
    }//GEN-LAST:event_bt23ActionPerformed

    private void bt24ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt24ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt24, (byte)3, (byte)5);
    }//GEN-LAST:event_bt24ActionPerformed

    private void bt25ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt25ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt25, (byte)4, (byte)0);
    }//GEN-LAST:event_bt25ActionPerformed

    private void bt26ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt26ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt26, (byte)4, (byte)1);
    }//GEN-LAST:event_bt26ActionPerformed

    private void bt27ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt27ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt27, (byte)4, (byte)2);
    }//GEN-LAST:event_bt27ActionPerformed

    private void bt28ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt28ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt28, (byte)4, (byte)3);
    }//GEN-LAST:event_bt28ActionPerformed

    private void bt29ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt29ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt29, (byte)4, (byte)4);
    }//GEN-LAST:event_bt29ActionPerformed

    private void bt30ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt30ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt30, (byte)4, (byte)5);
    }//GEN-LAST:event_bt30ActionPerformed

    private void bt31ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt31ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt31, (byte)5, (byte)0);
    }//GEN-LAST:event_bt31ActionPerformed

    private void bt32ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt32ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt32, (byte)5, (byte)1);
    }//GEN-LAST:event_bt32ActionPerformed

    private void bt33ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt33ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt33, (byte)5, (byte)2);
    }//GEN-LAST:event_bt33ActionPerformed

    private void bt34ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt34ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt34, (byte)5, (byte)3);
    }//GEN-LAST:event_bt34ActionPerformed

    private void bt35ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt35ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt35, (byte)5, (byte)4);
    }//GEN-LAST:event_bt35ActionPerformed

    private void bt36ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt36ActionPerformed
        Values.clickBloco(TelaJogoFacil.this, lbPontuacao, bt36, (byte)5, (byte)5);
    }//GEN-LAST:event_bt36ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel avatarIcon;
    private javax.swing.JButton bt1;
    private javax.swing.JButton bt10;
    private javax.swing.JButton bt11;
    private javax.swing.JButton bt12;
    private javax.swing.JButton bt13;
    private javax.swing.JButton bt14;
    private javax.swing.JButton bt15;
    private javax.swing.JButton bt16;
    private javax.swing.JButton bt17;
    private javax.swing.JButton bt18;
    private javax.swing.JButton bt19;
    private javax.swing.JButton bt2;
    private javax.swing.JButton bt20;
    private javax.swing.JButton bt21;
    private javax.swing.JButton bt22;
    private javax.swing.JButton bt23;
    private javax.swing.JButton bt24;
    private javax.swing.JButton bt25;
    private javax.swing.JButton bt26;
    private javax.swing.JButton bt27;
    private javax.swing.JButton bt28;
    private javax.swing.JButton bt29;
    private javax.swing.JButton bt3;
    private javax.swing.JButton bt30;
    private javax.swing.JButton bt31;
    private javax.swing.JButton bt32;
    private javax.swing.JButton bt33;
    private javax.swing.JButton bt34;
    private javax.swing.JButton bt35;
    private javax.swing.JButton bt36;
    private javax.swing.JButton bt4;
    private javax.swing.JButton bt5;
    private javax.swing.JButton bt6;
    private javax.swing.JButton bt7;
    private javax.swing.JButton bt8;
    private javax.swing.JButton bt9;
    private javax.swing.JButton btDesistir;
    private javax.swing.JButton btRestart;
    private javax.swing.JLabel lbNivelDificuldade;
    private javax.swing.JLabel lbPontuacao;
    private javax.swing.JPanel painelInfo;
    private javax.swing.JPanel painelJogador;
    private javax.swing.JPanel painelJogoFacil;
    private javax.swing.JPanel painelMenuRapido;
    private javax.swing.JPanel painelPontuacao;
    // End of variables declaration//GEN-END:variables
}
