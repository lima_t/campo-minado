package CampoMinado.Telas;

import CampoMinado.Classes.*;
import CampoMinado.Classes.Tabelas.TabelaScores;

/**
 * @author Tarcísio de Lima
 * @version 1.0.0
 * 
 * Essa Tela é chamada apenas se a opção do nível fácil do jogo estiver selecionada, nela temos um campo 6x6 onde
 * o jogador deverá tentar captaurar todos pontos
 */
public class TelaJogoDificil extends javax.swing.JFrame {

    public TelaJogoDificil() {
        initComponents();
        this.setLocationRelativeTo(null);
        this.setIconImage(new Values().iconeJanela(DataImage.ICONE_NOVO_JOGO));
        new Values().iconeAvatarJogador(TelaJogoDificil.this, avatarIcon);
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        painelJogoNormal = new javax.swing.JPanel();
        painelJogoDificil = new javax.swing.JPanel();
        bt1 = new javax.swing.JButton();
        bt2 = new javax.swing.JButton();
        bt3 = new javax.swing.JButton();
        bt4 = new javax.swing.JButton();
        bt5 = new javax.swing.JButton();
        bt6 = new javax.swing.JButton();
        bt7 = new javax.swing.JButton();
        bt8 = new javax.swing.JButton();
        bt9 = new javax.swing.JButton();
        bt10 = new javax.swing.JButton();
        bt11 = new javax.swing.JButton();
        bt12 = new javax.swing.JButton();
        bt13 = new javax.swing.JButton();
        bt14 = new javax.swing.JButton();
        bt15 = new javax.swing.JButton();
        bt16 = new javax.swing.JButton();
        bt17 = new javax.swing.JButton();
        bt18 = new javax.swing.JButton();
        bt19 = new javax.swing.JButton();
        bt20 = new javax.swing.JButton();
        bt21 = new javax.swing.JButton();
        bt22 = new javax.swing.JButton();
        bt23 = new javax.swing.JButton();
        bt24 = new javax.swing.JButton();
        bt25 = new javax.swing.JButton();
        bt26 = new javax.swing.JButton();
        bt27 = new javax.swing.JButton();
        bt28 = new javax.swing.JButton();
        bt29 = new javax.swing.JButton();
        bt30 = new javax.swing.JButton();
        bt31 = new javax.swing.JButton();
        bt32 = new javax.swing.JButton();
        bt33 = new javax.swing.JButton();
        bt34 = new javax.swing.JButton();
        bt35 = new javax.swing.JButton();
        bt36 = new javax.swing.JButton();
        bt37 = new javax.swing.JButton();
        bt38 = new javax.swing.JButton();
        bt39 = new javax.swing.JButton();
        bt40 = new javax.swing.JButton();
        bt41 = new javax.swing.JButton();
        bt42 = new javax.swing.JButton();
        bt43 = new javax.swing.JButton();
        bt44 = new javax.swing.JButton();
        bt45 = new javax.swing.JButton();
        bt46 = new javax.swing.JButton();
        bt47 = new javax.swing.JButton();
        bt48 = new javax.swing.JButton();
        bt49 = new javax.swing.JButton();
        bt50 = new javax.swing.JButton();
        bt51 = new javax.swing.JButton();
        bt52 = new javax.swing.JButton();
        bt53 = new javax.swing.JButton();
        bt54 = new javax.swing.JButton();
        bt55 = new javax.swing.JButton();
        bt56 = new javax.swing.JButton();
        bt57 = new javax.swing.JButton();
        bt58 = new javax.swing.JButton();
        bt59 = new javax.swing.JButton();
        bt60 = new javax.swing.JButton();
        bt61 = new javax.swing.JButton();
        bt62 = new javax.swing.JButton();
        bt63 = new javax.swing.JButton();
        bt64 = new javax.swing.JButton();
        bt65 = new javax.swing.JButton();
        bt66 = new javax.swing.JButton();
        bt67 = new javax.swing.JButton();
        bt68 = new javax.swing.JButton();
        bt69 = new javax.swing.JButton();
        bt70 = new javax.swing.JButton();
        bt71 = new javax.swing.JButton();
        bt72 = new javax.swing.JButton();
        bt73 = new javax.swing.JButton();
        bt74 = new javax.swing.JButton();
        bt75 = new javax.swing.JButton();
        bt76 = new javax.swing.JButton();
        bt77 = new javax.swing.JButton();
        bt78 = new javax.swing.JButton();
        bt79 = new javax.swing.JButton();
        bt80 = new javax.swing.JButton();
        bt81 = new javax.swing.JButton();
        bt82 = new javax.swing.JButton();
        bt83 = new javax.swing.JButton();
        bt84 = new javax.swing.JButton();
        bt85 = new javax.swing.JButton();
        bt86 = new javax.swing.JButton();
        bt87 = new javax.swing.JButton();
        bt88 = new javax.swing.JButton();
        bt89 = new javax.swing.JButton();
        bt90 = new javax.swing.JButton();
        bt91 = new javax.swing.JButton();
        bt92 = new javax.swing.JButton();
        bt93 = new javax.swing.JButton();
        bt94 = new javax.swing.JButton();
        bt95 = new javax.swing.JButton();
        bt96 = new javax.swing.JButton();
        bt97 = new javax.swing.JButton();
        bt98 = new javax.swing.JButton();
        bt99 = new javax.swing.JButton();
        bt100 = new javax.swing.JButton();
        bt101 = new javax.swing.JButton();
        bt102 = new javax.swing.JButton();
        bt103 = new javax.swing.JButton();
        bt104 = new javax.swing.JButton();
        bt105 = new javax.swing.JButton();
        bt106 = new javax.swing.JButton();
        bt107 = new javax.swing.JButton();
        bt108 = new javax.swing.JButton();
        bt109 = new javax.swing.JButton();
        bt110 = new javax.swing.JButton();
        bt111 = new javax.swing.JButton();
        bt112 = new javax.swing.JButton();
        bt113 = new javax.swing.JButton();
        bt114 = new javax.swing.JButton();
        bt115 = new javax.swing.JButton();
        bt116 = new javax.swing.JButton();
        bt117 = new javax.swing.JButton();
        bt118 = new javax.swing.JButton();
        bt119 = new javax.swing.JButton();
        bt120 = new javax.swing.JButton();
        bt121 = new javax.swing.JButton();
        bt122 = new javax.swing.JButton();
        bt123 = new javax.swing.JButton();
        bt124 = new javax.swing.JButton();
        bt125 = new javax.swing.JButton();
        bt126 = new javax.swing.JButton();
        bt127 = new javax.swing.JButton();
        bt128 = new javax.swing.JButton();
        bt129 = new javax.swing.JButton();
        bt130 = new javax.swing.JButton();
        bt131 = new javax.swing.JButton();
        bt132 = new javax.swing.JButton();
        bt133 = new javax.swing.JButton();
        bt134 = new javax.swing.JButton();
        bt135 = new javax.swing.JButton();
        bt136 = new javax.swing.JButton();
        bt137 = new javax.swing.JButton();
        bt138 = new javax.swing.JButton();
        bt139 = new javax.swing.JButton();
        bt140 = new javax.swing.JButton();
        bt141 = new javax.swing.JButton();
        bt142 = new javax.swing.JButton();
        bt143 = new javax.swing.JButton();
        bt144 = new javax.swing.JButton();
        bt145 = new javax.swing.JButton();
        bt146 = new javax.swing.JButton();
        bt147 = new javax.swing.JButton();
        bt148 = new javax.swing.JButton();
        bt149 = new javax.swing.JButton();
        bt150 = new javax.swing.JButton();
        bt151 = new javax.swing.JButton();
        bt152 = new javax.swing.JButton();
        bt153 = new javax.swing.JButton();
        bt154 = new javax.swing.JButton();
        bt155 = new javax.swing.JButton();
        bt156 = new javax.swing.JButton();
        bt157 = new javax.swing.JButton();
        bt158 = new javax.swing.JButton();
        bt159 = new javax.swing.JButton();
        bt160 = new javax.swing.JButton();
        bt161 = new javax.swing.JButton();
        bt162 = new javax.swing.JButton();
        bt163 = new javax.swing.JButton();
        bt164 = new javax.swing.JButton();
        bt165 = new javax.swing.JButton();
        bt166 = new javax.swing.JButton();
        bt167 = new javax.swing.JButton();
        bt168 = new javax.swing.JButton();
        bt169 = new javax.swing.JButton();
        bt170 = new javax.swing.JButton();
        bt171 = new javax.swing.JButton();
        bt172 = new javax.swing.JButton();
        bt173 = new javax.swing.JButton();
        bt174 = new javax.swing.JButton();
        bt175 = new javax.swing.JButton();
        bt176 = new javax.swing.JButton();
        bt177 = new javax.swing.JButton();
        bt178 = new javax.swing.JButton();
        bt179 = new javax.swing.JButton();
        bt180 = new javax.swing.JButton();
        bt181 = new javax.swing.JButton();
        bt182 = new javax.swing.JButton();
        bt183 = new javax.swing.JButton();
        bt184 = new javax.swing.JButton();
        bt185 = new javax.swing.JButton();
        bt186 = new javax.swing.JButton();
        bt187 = new javax.swing.JButton();
        bt188 = new javax.swing.JButton();
        bt189 = new javax.swing.JButton();
        bt190 = new javax.swing.JButton();
        bt191 = new javax.swing.JButton();
        bt192 = new javax.swing.JButton();
        bt193 = new javax.swing.JButton();
        bt194 = new javax.swing.JButton();
        bt195 = new javax.swing.JButton();
        bt196 = new javax.swing.JButton();
        painelInfo = new javax.swing.JPanel();
        painelPontuacao = new javax.swing.JPanel();
        lbPontuacao = new javax.swing.JLabel();
        painelJogador = new javax.swing.JPanel();
        lbNivelDificuldade = new javax.swing.JLabel();
        avatarIcon = new javax.swing.JLabel();
        painelMenuRapido = new javax.swing.JPanel();
        btRestart = new javax.swing.JButton();
        btDesistir = new javax.swing.JButton();

        painelJogoNormal.setBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(255, 0, 0), new java.awt.Color(102, 102, 102)));
        painelJogoNormal.setToolTipText("Blocos");

        javax.swing.GroupLayout painelJogoNormalLayout = new javax.swing.GroupLayout(painelJogoNormal);
        painelJogoNormal.setLayout(painelJogoNormalLayout);
        painelJogoNormalLayout.setHorizontalGroup(
            painelJogoNormalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 320, Short.MAX_VALUE)
        );
        painelJogoNormalLayout.setVerticalGroup(
            painelJogoNormalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 268, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Minas Nível Difícil");
        setResizable(false);

        painelJogoDificil.setBorder(javax.swing.BorderFactory.createEtchedBorder(new java.awt.Color(255, 0, 0), new java.awt.Color(102, 102, 102)));
        painelJogoDificil.setToolTipText("Blocos");

        bt1.setBackground(Values.corBlocos);
        bt1.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt1.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt1.setPreferredSize(new java.awt.Dimension(40, 40));
        bt1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt1ActionPerformed(evt);
            }
        });

        bt2.setBackground(Values.corBlocos);
        bt2.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt2.setPreferredSize(new java.awt.Dimension(40, 40));
        bt2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt2ActionPerformed(evt);
            }
        });

        bt3.setBackground(Values.corBlocos);
        bt3.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt3.setPreferredSize(new java.awt.Dimension(40, 40));
        bt3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt3ActionPerformed(evt);
            }
        });

        bt4.setBackground(Values.corBlocos);
        bt4.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt4.setPreferredSize(new java.awt.Dimension(40, 40));
        bt4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt4ActionPerformed(evt);
            }
        });

        bt5.setBackground(Values.corBlocos);
        bt5.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt5.setPreferredSize(new java.awt.Dimension(40, 40));
        bt5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt5ActionPerformed(evt);
            }
        });

        bt6.setBackground(Values.corBlocos);
        bt6.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt6.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt6.setPreferredSize(new java.awt.Dimension(40, 40));
        bt6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt6ActionPerformed(evt);
            }
        });

        bt7.setBackground(Values.corBlocos);
        bt7.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt7.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt7.setPreferredSize(new java.awt.Dimension(40, 40));
        bt7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt7ActionPerformed(evt);
            }
        });

        bt8.setBackground(Values.corBlocos);
        bt8.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt8.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt8.setPreferredSize(new java.awt.Dimension(40, 40));
        bt8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt8ActionPerformed(evt);
            }
        });

        bt9.setBackground(Values.corBlocos);
        bt9.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt9.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt9.setPreferredSize(new java.awt.Dimension(40, 40));
        bt9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt9ActionPerformed(evt);
            }
        });

        bt10.setBackground(Values.corBlocos);
        bt10.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt10.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt10.setPreferredSize(new java.awt.Dimension(40, 40));
        bt10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt10ActionPerformed(evt);
            }
        });

        bt11.setBackground(Values.corBlocos);
        bt11.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt11.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt11.setPreferredSize(new java.awt.Dimension(40, 40));
        bt11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt11ActionPerformed(evt);
            }
        });

        bt12.setBackground(Values.corBlocos);
        bt12.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt12.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt12.setPreferredSize(new java.awt.Dimension(40, 40));
        bt12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt12ActionPerformed(evt);
            }
        });

        bt13.setBackground(Values.corBlocos);
        bt13.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt13.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt13.setPreferredSize(new java.awt.Dimension(40, 40));
        bt13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt13ActionPerformed(evt);
            }
        });

        bt14.setBackground(Values.corBlocos);
        bt14.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt14.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt14.setPreferredSize(new java.awt.Dimension(40, 40));
        bt14.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt14ActionPerformed(evt);
            }
        });

        bt15.setBackground(Values.corBlocos);
        bt15.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt15.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt15.setPreferredSize(new java.awt.Dimension(40, 40));
        bt15.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt15ActionPerformed(evt);
            }
        });

        bt16.setBackground(Values.corBlocos);
        bt16.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt16.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt16.setPreferredSize(new java.awt.Dimension(40, 40));
        bt16.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt16ActionPerformed(evt);
            }
        });

        bt17.setBackground(Values.corBlocos);
        bt17.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt17.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt17.setPreferredSize(new java.awt.Dimension(40, 40));
        bt17.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt17ActionPerformed(evt);
            }
        });

        bt18.setBackground(Values.corBlocos);
        bt18.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt18.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt18.setPreferredSize(new java.awt.Dimension(40, 40));
        bt18.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt18ActionPerformed(evt);
            }
        });

        bt19.setBackground(Values.corBlocos);
        bt19.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt19.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt19.setPreferredSize(new java.awt.Dimension(40, 40));
        bt19.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt19ActionPerformed(evt);
            }
        });

        bt20.setBackground(Values.corBlocos);
        bt20.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt20.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt20.setPreferredSize(new java.awt.Dimension(40, 40));
        bt20.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt20ActionPerformed(evt);
            }
        });

        bt21.setBackground(Values.corBlocos);
        bt21.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt21.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt21.setPreferredSize(new java.awt.Dimension(40, 40));
        bt21.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt21ActionPerformed(evt);
            }
        });

        bt22.setBackground(Values.corBlocos);
        bt22.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt22.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt22.setPreferredSize(new java.awt.Dimension(40, 40));
        bt22.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt22ActionPerformed(evt);
            }
        });

        bt23.setBackground(Values.corBlocos);
        bt23.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt23.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt23.setPreferredSize(new java.awt.Dimension(40, 40));
        bt23.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt23ActionPerformed(evt);
            }
        });

        bt24.setBackground(Values.corBlocos);
        bt24.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt24.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt24.setPreferredSize(new java.awt.Dimension(40, 40));
        bt24.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt24ActionPerformed(evt);
            }
        });

        bt25.setBackground(Values.corBlocos);
        bt25.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt25.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt25.setPreferredSize(new java.awt.Dimension(40, 40));
        bt25.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt25ActionPerformed(evt);
            }
        });

        bt26.setBackground(Values.corBlocos);
        bt26.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt26.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt26.setPreferredSize(new java.awt.Dimension(40, 40));
        bt26.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt26ActionPerformed(evt);
            }
        });

        bt27.setBackground(Values.corBlocos);
        bt27.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt27.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt27.setPreferredSize(new java.awt.Dimension(40, 40));
        bt27.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt27ActionPerformed(evt);
            }
        });

        bt28.setBackground(Values.corBlocos);
        bt28.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt28.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt28.setPreferredSize(new java.awt.Dimension(40, 40));
        bt28.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt28ActionPerformed(evt);
            }
        });

        bt29.setBackground(Values.corBlocos);
        bt29.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt29.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt29.setPreferredSize(new java.awt.Dimension(40, 40));
        bt29.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt29ActionPerformed(evt);
            }
        });

        bt30.setBackground(Values.corBlocos);
        bt30.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt30.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt30.setPreferredSize(new java.awt.Dimension(40, 40));
        bt30.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt30ActionPerformed(evt);
            }
        });

        bt31.setBackground(Values.corBlocos);
        bt31.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt31.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt31.setPreferredSize(new java.awt.Dimension(40, 40));
        bt31.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt31ActionPerformed(evt);
            }
        });

        bt32.setBackground(Values.corBlocos);
        bt32.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt32.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt32.setPreferredSize(new java.awt.Dimension(40, 40));
        bt32.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt32ActionPerformed(evt);
            }
        });

        bt33.setBackground(Values.corBlocos);
        bt33.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt33.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt33.setPreferredSize(new java.awt.Dimension(40, 40));
        bt33.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt33ActionPerformed(evt);
            }
        });

        bt34.setBackground(Values.corBlocos);
        bt34.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt34.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt34.setPreferredSize(new java.awt.Dimension(40, 40));
        bt34.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt34ActionPerformed(evt);
            }
        });

        bt35.setBackground(Values.corBlocos);
        bt35.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt35.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt35.setPreferredSize(new java.awt.Dimension(40, 40));
        bt35.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt35ActionPerformed(evt);
            }
        });

        bt36.setBackground(Values.corBlocos);
        bt36.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt36.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt36.setPreferredSize(new java.awt.Dimension(40, 40));
        bt36.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt36ActionPerformed(evt);
            }
        });

        bt37.setBackground(Values.corBlocos);
        bt37.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt37.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt37.setPreferredSize(new java.awt.Dimension(40, 40));
        bt37.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt37ActionPerformed(evt);
            }
        });

        bt38.setBackground(Values.corBlocos);
        bt38.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt38.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt38.setPreferredSize(new java.awt.Dimension(40, 40));
        bt38.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt38ActionPerformed(evt);
            }
        });

        bt39.setBackground(Values.corBlocos);
        bt39.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt39.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt39.setPreferredSize(new java.awt.Dimension(40, 40));
        bt39.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt39ActionPerformed(evt);
            }
        });

        bt40.setBackground(Values.corBlocos);
        bt40.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt40.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt40.setPreferredSize(new java.awt.Dimension(40, 40));
        bt40.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt40ActionPerformed(evt);
            }
        });

        bt41.setBackground(Values.corBlocos);
        bt41.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt41.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt41.setPreferredSize(new java.awt.Dimension(40, 40));
        bt41.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt41ActionPerformed(evt);
            }
        });

        bt42.setBackground(Values.corBlocos);
        bt42.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt42.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt42.setPreferredSize(new java.awt.Dimension(40, 40));
        bt42.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt42ActionPerformed(evt);
            }
        });

        bt43.setBackground(Values.corBlocos);
        bt43.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt43.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt43.setPreferredSize(new java.awt.Dimension(40, 40));
        bt43.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt43ActionPerformed(evt);
            }
        });

        bt44.setBackground(Values.corBlocos);
        bt44.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt44.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt44.setPreferredSize(new java.awt.Dimension(40, 40));
        bt44.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt44ActionPerformed(evt);
            }
        });

        bt45.setBackground(Values.corBlocos);
        bt45.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt45.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt45.setPreferredSize(new java.awt.Dimension(40, 40));
        bt45.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt45ActionPerformed(evt);
            }
        });

        bt46.setBackground(Values.corBlocos);
        bt46.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt46.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt46.setPreferredSize(new java.awt.Dimension(40, 40));
        bt46.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt46ActionPerformed(evt);
            }
        });

        bt47.setBackground(Values.corBlocos);
        bt47.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt47.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt47.setPreferredSize(new java.awt.Dimension(40, 40));
        bt47.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt47ActionPerformed(evt);
            }
        });

        bt48.setBackground(Values.corBlocos);
        bt48.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt48.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt48.setPreferredSize(new java.awt.Dimension(40, 40));
        bt48.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt48ActionPerformed(evt);
            }
        });

        bt49.setBackground(Values.corBlocos);
        bt49.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt49.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt49.setPreferredSize(new java.awt.Dimension(40, 40));
        bt49.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt49ActionPerformed(evt);
            }
        });

        bt50.setBackground(Values.corBlocos);
        bt50.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt50.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt50.setPreferredSize(new java.awt.Dimension(40, 40));
        bt50.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt50ActionPerformed(evt);
            }
        });

        bt51.setBackground(Values.corBlocos);
        bt51.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt51.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt51.setPreferredSize(new java.awt.Dimension(40, 40));
        bt51.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt51ActionPerformed(evt);
            }
        });

        bt52.setBackground(Values.corBlocos);
        bt52.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt52.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt52.setPreferredSize(new java.awt.Dimension(40, 40));
        bt52.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt52ActionPerformed(evt);
            }
        });

        bt53.setBackground(Values.corBlocos);
        bt53.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt53.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt53.setPreferredSize(new java.awt.Dimension(40, 40));
        bt53.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt53ActionPerformed(evt);
            }
        });

        bt54.setBackground(Values.corBlocos);
        bt54.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt54.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt54.setPreferredSize(new java.awt.Dimension(40, 40));
        bt54.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt54ActionPerformed(evt);
            }
        });

        bt55.setBackground(Values.corBlocos);
        bt55.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt55.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt55.setPreferredSize(new java.awt.Dimension(40, 40));
        bt55.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt55ActionPerformed(evt);
            }
        });

        bt56.setBackground(Values.corBlocos);
        bt56.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt56.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt56.setPreferredSize(new java.awt.Dimension(40, 40));
        bt56.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt56ActionPerformed(evt);
            }
        });

        bt57.setBackground(Values.corBlocos);
        bt57.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt57.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt57.setPreferredSize(new java.awt.Dimension(40, 40));
        bt57.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt57ActionPerformed(evt);
            }
        });

        bt58.setBackground(Values.corBlocos);
        bt58.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt58.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt58.setPreferredSize(new java.awt.Dimension(40, 40));
        bt58.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt58ActionPerformed(evt);
            }
        });

        bt59.setBackground(Values.corBlocos);
        bt59.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt59.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt59.setPreferredSize(new java.awt.Dimension(40, 40));
        bt59.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt59ActionPerformed(evt);
            }
        });

        bt60.setBackground(Values.corBlocos);
        bt60.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt60.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt60.setPreferredSize(new java.awt.Dimension(40, 40));
        bt60.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt60ActionPerformed(evt);
            }
        });

        bt61.setBackground(Values.corBlocos);
        bt61.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt61.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt61.setPreferredSize(new java.awt.Dimension(40, 40));
        bt61.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt61ActionPerformed(evt);
            }
        });

        bt62.setBackground(Values.corBlocos);
        bt62.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt62.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt62.setPreferredSize(new java.awt.Dimension(40, 40));
        bt62.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt62ActionPerformed(evt);
            }
        });

        bt63.setBackground(Values.corBlocos);
        bt63.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt63.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt63.setPreferredSize(new java.awt.Dimension(40, 40));
        bt63.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt63ActionPerformed(evt);
            }
        });

        bt64.setBackground(Values.corBlocos);
        bt64.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt64.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt64.setPreferredSize(new java.awt.Dimension(40, 40));
        bt64.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt64ActionPerformed(evt);
            }
        });

        bt65.setBackground(Values.corBlocos);
        bt65.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt65.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt65.setPreferredSize(new java.awt.Dimension(40, 40));
        bt65.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt65ActionPerformed(evt);
            }
        });

        bt66.setBackground(Values.corBlocos);
        bt66.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt66.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt66.setPreferredSize(new java.awt.Dimension(40, 40));
        bt66.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt66ActionPerformed(evt);
            }
        });

        bt67.setBackground(Values.corBlocos);
        bt67.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt67.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt67.setPreferredSize(new java.awt.Dimension(40, 40));
        bt67.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt67ActionPerformed(evt);
            }
        });

        bt68.setBackground(Values.corBlocos);
        bt68.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt68.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt68.setPreferredSize(new java.awt.Dimension(40, 40));
        bt68.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt68ActionPerformed(evt);
            }
        });

        bt69.setBackground(Values.corBlocos);
        bt69.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt69.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt69.setPreferredSize(new java.awt.Dimension(40, 40));
        bt69.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt69ActionPerformed(evt);
            }
        });

        bt70.setBackground(Values.corBlocos);
        bt70.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt70.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt70.setPreferredSize(new java.awt.Dimension(40, 40));
        bt70.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt70ActionPerformed(evt);
            }
        });

        bt71.setBackground(Values.corBlocos);
        bt71.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt71.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt71.setPreferredSize(new java.awt.Dimension(40, 40));
        bt71.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt71ActionPerformed(evt);
            }
        });

        bt72.setBackground(Values.corBlocos);
        bt72.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt72.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt72.setPreferredSize(new java.awt.Dimension(40, 40));
        bt72.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt72ActionPerformed(evt);
            }
        });

        bt73.setBackground(Values.corBlocos);
        bt73.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt73.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt73.setPreferredSize(new java.awt.Dimension(40, 40));
        bt73.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt73ActionPerformed(evt);
            }
        });

        bt74.setBackground(Values.corBlocos);
        bt74.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt74.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt74.setPreferredSize(new java.awt.Dimension(40, 40));
        bt74.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt74ActionPerformed(evt);
            }
        });

        bt75.setBackground(Values.corBlocos);
        bt75.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt75.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt75.setPreferredSize(new java.awt.Dimension(40, 40));
        bt75.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt75ActionPerformed(evt);
            }
        });

        bt76.setBackground(Values.corBlocos);
        bt76.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt76.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt76.setPreferredSize(new java.awt.Dimension(40, 40));
        bt76.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt76ActionPerformed(evt);
            }
        });

        bt77.setBackground(Values.corBlocos);
        bt77.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt77.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt77.setPreferredSize(new java.awt.Dimension(40, 40));
        bt77.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt77ActionPerformed(evt);
            }
        });

        bt78.setBackground(Values.corBlocos);
        bt78.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt78.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt78.setPreferredSize(new java.awt.Dimension(40, 40));
        bt78.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt78ActionPerformed(evt);
            }
        });

        bt79.setBackground(Values.corBlocos);
        bt79.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt79.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt79.setPreferredSize(new java.awt.Dimension(40, 40));
        bt79.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt79ActionPerformed(evt);
            }
        });

        bt80.setBackground(Values.corBlocos);
        bt80.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt80.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt80.setPreferredSize(new java.awt.Dimension(40, 40));
        bt80.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt80ActionPerformed(evt);
            }
        });

        bt81.setBackground(Values.corBlocos);
        bt81.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt81.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt81.setPreferredSize(new java.awt.Dimension(40, 40));
        bt81.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt81ActionPerformed(evt);
            }
        });

        bt82.setBackground(Values.corBlocos);
        bt82.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt82.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt82.setPreferredSize(new java.awt.Dimension(40, 40));
        bt82.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt82ActionPerformed(evt);
            }
        });

        bt83.setBackground(Values.corBlocos);
        bt83.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt83.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt83.setPreferredSize(new java.awt.Dimension(40, 40));
        bt83.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt83ActionPerformed(evt);
            }
        });

        bt84.setBackground(Values.corBlocos);
        bt84.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt84.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt84.setPreferredSize(new java.awt.Dimension(40, 40));
        bt84.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt84ActionPerformed(evt);
            }
        });

        bt85.setBackground(Values.corBlocos);
        bt85.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt85.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt85.setPreferredSize(new java.awt.Dimension(40, 40));
        bt85.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt85ActionPerformed(evt);
            }
        });

        bt86.setBackground(Values.corBlocos);
        bt86.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt86.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt86.setPreferredSize(new java.awt.Dimension(40, 40));
        bt86.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt86ActionPerformed(evt);
            }
        });

        bt87.setBackground(Values.corBlocos);
        bt87.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt87.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt87.setPreferredSize(new java.awt.Dimension(40, 40));
        bt87.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt87ActionPerformed(evt);
            }
        });

        bt88.setBackground(Values.corBlocos);
        bt88.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt88.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt88.setPreferredSize(new java.awt.Dimension(40, 40));
        bt88.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt88ActionPerformed(evt);
            }
        });

        bt89.setBackground(Values.corBlocos);
        bt89.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt89.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt89.setPreferredSize(new java.awt.Dimension(40, 40));
        bt89.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt89ActionPerformed(evt);
            }
        });

        bt90.setBackground(Values.corBlocos);
        bt90.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt90.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt90.setPreferredSize(new java.awt.Dimension(40, 40));
        bt90.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt90ActionPerformed(evt);
            }
        });

        bt91.setBackground(Values.corBlocos);
        bt91.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt91.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt91.setPreferredSize(new java.awt.Dimension(40, 40));
        bt91.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt91ActionPerformed(evt);
            }
        });

        bt92.setBackground(Values.corBlocos);
        bt92.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt92.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt92.setPreferredSize(new java.awt.Dimension(40, 40));
        bt92.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt92ActionPerformed(evt);
            }
        });

        bt93.setBackground(Values.corBlocos);
        bt93.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt93.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt93.setPreferredSize(new java.awt.Dimension(40, 40));
        bt93.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt93ActionPerformed(evt);
            }
        });

        bt94.setBackground(Values.corBlocos);
        bt94.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt94.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt94.setPreferredSize(new java.awt.Dimension(40, 40));
        bt94.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt94ActionPerformed(evt);
            }
        });

        bt95.setBackground(Values.corBlocos);
        bt95.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt95.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt95.setPreferredSize(new java.awt.Dimension(40, 40));
        bt95.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt95ActionPerformed(evt);
            }
        });

        bt96.setBackground(Values.corBlocos);
        bt96.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt96.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt96.setPreferredSize(new java.awt.Dimension(40, 40));
        bt96.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt96ActionPerformed(evt);
            }
        });

        bt97.setBackground(Values.corBlocos);
        bt97.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt97.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt97.setPreferredSize(new java.awt.Dimension(40, 40));
        bt97.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt97ActionPerformed(evt);
            }
        });

        bt98.setBackground(Values.corBlocos);
        bt98.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt98.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt98.setPreferredSize(new java.awt.Dimension(40, 40));
        bt98.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt98ActionPerformed(evt);
            }
        });

        bt99.setBackground(Values.corBlocos);
        bt99.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt99.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt99.setPreferredSize(new java.awt.Dimension(40, 40));
        bt99.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt99ActionPerformed(evt);
            }
        });

        bt100.setBackground(Values.corBlocos);
        bt100.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt100.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt100.setPreferredSize(new java.awt.Dimension(40, 40));
        bt100.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt100ActionPerformed(evt);
            }
        });

        bt101.setBackground(Values.corBlocos);
        bt101.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt101.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt101.setPreferredSize(new java.awt.Dimension(40, 40));
        bt101.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt101ActionPerformed(evt);
            }
        });

        bt102.setBackground(Values.corBlocos);
        bt102.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt102.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt102.setPreferredSize(new java.awt.Dimension(40, 40));
        bt102.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt102ActionPerformed(evt);
            }
        });

        bt103.setBackground(Values.corBlocos);
        bt103.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt103.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt103.setPreferredSize(new java.awt.Dimension(40, 40));
        bt103.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt103ActionPerformed(evt);
            }
        });

        bt104.setBackground(Values.corBlocos);
        bt104.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt104.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt104.setPreferredSize(new java.awt.Dimension(40, 40));
        bt104.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt104ActionPerformed(evt);
            }
        });

        bt105.setBackground(Values.corBlocos);
        bt105.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt105.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt105.setPreferredSize(new java.awt.Dimension(40, 40));
        bt105.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt105ActionPerformed(evt);
            }
        });

        bt106.setBackground(Values.corBlocos);
        bt106.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt106.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt106.setPreferredSize(new java.awt.Dimension(40, 40));
        bt106.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt106ActionPerformed(evt);
            }
        });

        bt107.setBackground(Values.corBlocos);
        bt107.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt107.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt107.setPreferredSize(new java.awt.Dimension(40, 40));
        bt107.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt107ActionPerformed(evt);
            }
        });

        bt108.setBackground(Values.corBlocos);
        bt108.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt108.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt108.setPreferredSize(new java.awt.Dimension(40, 40));
        bt108.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt108ActionPerformed(evt);
            }
        });

        bt109.setBackground(Values.corBlocos);
        bt109.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt109.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt109.setPreferredSize(new java.awt.Dimension(40, 40));
        bt109.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt109ActionPerformed(evt);
            }
        });

        bt110.setBackground(Values.corBlocos);
        bt110.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt110.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt110.setPreferredSize(new java.awt.Dimension(40, 40));
        bt110.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt110ActionPerformed(evt);
            }
        });

        bt111.setBackground(Values.corBlocos);
        bt111.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt111.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt111.setPreferredSize(new java.awt.Dimension(40, 40));
        bt111.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt111ActionPerformed(evt);
            }
        });

        bt112.setBackground(Values.corBlocos);
        bt112.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt112.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt112.setPreferredSize(new java.awt.Dimension(40, 40));
        bt112.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt112ActionPerformed(evt);
            }
        });

        bt113.setBackground(Values.corBlocos);
        bt113.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt113.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt113.setPreferredSize(new java.awt.Dimension(40, 40));
        bt113.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt113ActionPerformed(evt);
            }
        });

        bt114.setBackground(Values.corBlocos);
        bt114.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt114.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt114.setPreferredSize(new java.awt.Dimension(40, 40));
        bt114.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt114ActionPerformed(evt);
            }
        });

        bt115.setBackground(Values.corBlocos);
        bt115.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt115.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt115.setPreferredSize(new java.awt.Dimension(40, 40));
        bt115.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt115ActionPerformed(evt);
            }
        });

        bt116.setBackground(Values.corBlocos);
        bt116.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt116.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt116.setPreferredSize(new java.awt.Dimension(40, 40));
        bt116.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt116ActionPerformed(evt);
            }
        });

        bt117.setBackground(Values.corBlocos);
        bt117.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt117.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt117.setPreferredSize(new java.awt.Dimension(40, 40));
        bt117.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt117ActionPerformed(evt);
            }
        });

        bt118.setBackground(Values.corBlocos);
        bt118.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt118.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt118.setPreferredSize(new java.awt.Dimension(40, 40));
        bt118.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt118ActionPerformed(evt);
            }
        });

        bt119.setBackground(Values.corBlocos);
        bt119.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt119.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt119.setPreferredSize(new java.awt.Dimension(40, 40));
        bt119.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt119ActionPerformed(evt);
            }
        });

        bt120.setBackground(Values.corBlocos);
        bt120.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt120.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt120.setPreferredSize(new java.awt.Dimension(40, 40));
        bt120.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt120ActionPerformed(evt);
            }
        });

        bt121.setBackground(Values.corBlocos);
        bt121.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt121.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt121.setPreferredSize(new java.awt.Dimension(40, 40));
        bt121.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt121ActionPerformed(evt);
            }
        });

        bt122.setBackground(Values.corBlocos);
        bt122.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt122.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt122.setPreferredSize(new java.awt.Dimension(40, 40));
        bt122.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt122ActionPerformed(evt);
            }
        });

        bt123.setBackground(Values.corBlocos);
        bt123.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt123.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt123.setPreferredSize(new java.awt.Dimension(40, 40));
        bt123.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt123ActionPerformed(evt);
            }
        });

        bt124.setBackground(Values.corBlocos);
        bt124.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt124.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt124.setPreferredSize(new java.awt.Dimension(40, 40));
        bt124.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt124ActionPerformed(evt);
            }
        });

        bt125.setBackground(Values.corBlocos);
        bt125.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt125.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt125.setPreferredSize(new java.awt.Dimension(40, 40));
        bt125.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt125ActionPerformed(evt);
            }
        });

        bt126.setBackground(Values.corBlocos);
        bt126.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt126.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt126.setPreferredSize(new java.awt.Dimension(40, 40));
        bt126.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt126ActionPerformed(evt);
            }
        });

        bt127.setBackground(Values.corBlocos);
        bt127.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt127.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt127.setPreferredSize(new java.awt.Dimension(40, 40));
        bt127.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt127ActionPerformed(evt);
            }
        });

        bt128.setBackground(Values.corBlocos);
        bt128.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt128.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt128.setPreferredSize(new java.awt.Dimension(40, 40));
        bt128.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt128ActionPerformed(evt);
            }
        });

        bt129.setBackground(Values.corBlocos);
        bt129.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt129.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt129.setPreferredSize(new java.awt.Dimension(40, 40));
        bt129.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt129ActionPerformed(evt);
            }
        });

        bt130.setBackground(Values.corBlocos);
        bt130.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt130.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt130.setPreferredSize(new java.awt.Dimension(40, 40));
        bt130.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt130ActionPerformed(evt);
            }
        });

        bt131.setBackground(Values.corBlocos);
        bt131.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt131.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt131.setPreferredSize(new java.awt.Dimension(40, 40));
        bt131.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt131ActionPerformed(evt);
            }
        });

        bt132.setBackground(Values.corBlocos);
        bt132.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt132.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt132.setPreferredSize(new java.awt.Dimension(40, 40));
        bt132.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt132ActionPerformed(evt);
            }
        });

        bt133.setBackground(Values.corBlocos);
        bt133.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt133.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt133.setPreferredSize(new java.awt.Dimension(40, 40));
        bt133.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt133ActionPerformed(evt);
            }
        });

        bt134.setBackground(Values.corBlocos);
        bt134.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt134.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt134.setPreferredSize(new java.awt.Dimension(40, 40));
        bt134.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt134ActionPerformed(evt);
            }
        });

        bt135.setBackground(Values.corBlocos);
        bt135.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt135.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt135.setPreferredSize(new java.awt.Dimension(40, 40));
        bt135.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt135ActionPerformed(evt);
            }
        });

        bt136.setBackground(Values.corBlocos);
        bt136.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt136.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt136.setPreferredSize(new java.awt.Dimension(40, 40));
        bt136.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt136ActionPerformed(evt);
            }
        });

        bt137.setBackground(Values.corBlocos);
        bt137.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt137.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt137.setPreferredSize(new java.awt.Dimension(40, 40));
        bt137.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt137ActionPerformed(evt);
            }
        });

        bt138.setBackground(Values.corBlocos);
        bt138.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt138.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt138.setPreferredSize(new java.awt.Dimension(40, 40));
        bt138.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt138ActionPerformed(evt);
            }
        });

        bt139.setBackground(Values.corBlocos);
        bt139.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt139.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt139.setPreferredSize(new java.awt.Dimension(40, 40));
        bt139.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt139ActionPerformed(evt);
            }
        });

        bt140.setBackground(Values.corBlocos);
        bt140.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt140.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt140.setPreferredSize(new java.awt.Dimension(40, 40));
        bt140.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt140ActionPerformed(evt);
            }
        });

        bt141.setBackground(Values.corBlocos);
        bt141.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt141.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt141.setPreferredSize(new java.awt.Dimension(40, 40));
        bt141.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt141ActionPerformed(evt);
            }
        });

        bt142.setBackground(Values.corBlocos);
        bt142.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt142.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt142.setPreferredSize(new java.awt.Dimension(40, 40));
        bt142.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt142ActionPerformed(evt);
            }
        });

        bt143.setBackground(Values.corBlocos);
        bt143.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt143.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt143.setPreferredSize(new java.awt.Dimension(40, 40));
        bt143.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt143ActionPerformed(evt);
            }
        });

        bt144.setBackground(Values.corBlocos);
        bt144.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt144.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt144.setPreferredSize(new java.awt.Dimension(40, 40));
        bt144.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt144ActionPerformed(evt);
            }
        });

        bt145.setBackground(Values.corBlocos);
        bt145.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt145.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt145.setPreferredSize(new java.awt.Dimension(40, 40));
        bt145.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt145ActionPerformed(evt);
            }
        });

        bt146.setBackground(Values.corBlocos);
        bt146.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt146.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt146.setPreferredSize(new java.awt.Dimension(40, 40));
        bt146.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt146ActionPerformed(evt);
            }
        });

        bt147.setBackground(Values.corBlocos);
        bt147.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt147.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt147.setPreferredSize(new java.awt.Dimension(40, 40));
        bt147.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt147ActionPerformed(evt);
            }
        });

        bt148.setBackground(Values.corBlocos);
        bt148.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt148.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt148.setPreferredSize(new java.awt.Dimension(40, 40));
        bt148.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt148ActionPerformed(evt);
            }
        });

        bt149.setBackground(Values.corBlocos);
        bt149.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt149.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt149.setPreferredSize(new java.awt.Dimension(40, 40));
        bt149.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt149ActionPerformed(evt);
            }
        });

        bt150.setBackground(Values.corBlocos);
        bt150.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt150.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt150.setPreferredSize(new java.awt.Dimension(40, 40));
        bt150.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt150ActionPerformed(evt);
            }
        });

        bt151.setBackground(Values.corBlocos);
        bt151.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt151.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt151.setPreferredSize(new java.awt.Dimension(40, 40));
        bt151.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt151ActionPerformed(evt);
            }
        });

        bt152.setBackground(Values.corBlocos);
        bt152.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt152.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt152.setPreferredSize(new java.awt.Dimension(40, 40));
        bt152.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt152ActionPerformed(evt);
            }
        });

        bt153.setBackground(Values.corBlocos);
        bt153.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt153.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt153.setPreferredSize(new java.awt.Dimension(40, 40));
        bt153.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt153ActionPerformed(evt);
            }
        });

        bt154.setBackground(Values.corBlocos);
        bt154.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt154.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt154.setPreferredSize(new java.awt.Dimension(40, 40));
        bt154.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt154ActionPerformed(evt);
            }
        });

        bt155.setBackground(Values.corBlocos);
        bt155.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt155.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt155.setPreferredSize(new java.awt.Dimension(40, 40));
        bt155.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt155ActionPerformed(evt);
            }
        });

        bt156.setBackground(Values.corBlocos);
        bt156.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt156.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt156.setPreferredSize(new java.awt.Dimension(40, 40));
        bt156.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt156ActionPerformed(evt);
            }
        });

        bt157.setBackground(Values.corBlocos);
        bt157.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt157.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt157.setPreferredSize(new java.awt.Dimension(40, 40));
        bt157.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt157ActionPerformed(evt);
            }
        });

        bt158.setBackground(Values.corBlocos);
        bt158.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt158.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt158.setPreferredSize(new java.awt.Dimension(40, 40));
        bt158.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt158ActionPerformed(evt);
            }
        });

        bt159.setBackground(Values.corBlocos);
        bt159.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt159.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt159.setPreferredSize(new java.awt.Dimension(40, 40));
        bt159.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt159ActionPerformed(evt);
            }
        });

        bt160.setBackground(Values.corBlocos);
        bt160.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt160.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt160.setPreferredSize(new java.awt.Dimension(40, 40));
        bt160.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt160ActionPerformed(evt);
            }
        });

        bt161.setBackground(Values.corBlocos);
        bt161.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt161.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt161.setPreferredSize(new java.awt.Dimension(40, 40));
        bt161.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt161ActionPerformed(evt);
            }
        });

        bt162.setBackground(Values.corBlocos);
        bt162.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt162.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt162.setPreferredSize(new java.awt.Dimension(40, 40));
        bt162.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt162ActionPerformed(evt);
            }
        });

        bt163.setBackground(Values.corBlocos);
        bt163.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt163.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt163.setPreferredSize(new java.awt.Dimension(40, 40));
        bt163.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt163ActionPerformed(evt);
            }
        });

        bt164.setBackground(Values.corBlocos);
        bt164.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt164.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt164.setPreferredSize(new java.awt.Dimension(40, 40));
        bt164.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt164ActionPerformed(evt);
            }
        });

        bt165.setBackground(Values.corBlocos);
        bt165.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt165.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt165.setPreferredSize(new java.awt.Dimension(40, 40));
        bt165.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt165ActionPerformed(evt);
            }
        });

        bt166.setBackground(Values.corBlocos);
        bt166.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt166.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt166.setPreferredSize(new java.awt.Dimension(40, 40));
        bt166.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt166ActionPerformed(evt);
            }
        });

        bt167.setBackground(Values.corBlocos);
        bt167.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt167.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt167.setPreferredSize(new java.awt.Dimension(40, 40));
        bt167.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt167ActionPerformed(evt);
            }
        });

        bt168.setBackground(Values.corBlocos);
        bt168.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt168.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt168.setPreferredSize(new java.awt.Dimension(40, 40));
        bt168.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt168ActionPerformed(evt);
            }
        });

        bt169.setBackground(Values.corBlocos);
        bt169.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt169.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt169.setPreferredSize(new java.awt.Dimension(40, 40));
        bt169.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt169ActionPerformed(evt);
            }
        });

        bt170.setBackground(Values.corBlocos);
        bt170.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt170.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt170.setPreferredSize(new java.awt.Dimension(40, 40));
        bt170.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt170ActionPerformed(evt);
            }
        });

        bt171.setBackground(Values.corBlocos);
        bt171.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt171.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt171.setPreferredSize(new java.awt.Dimension(40, 40));
        bt171.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt171ActionPerformed(evt);
            }
        });

        bt172.setBackground(Values.corBlocos);
        bt172.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt172.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt172.setPreferredSize(new java.awt.Dimension(40, 40));
        bt172.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt172ActionPerformed(evt);
            }
        });

        bt173.setBackground(Values.corBlocos);
        bt173.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt173.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt173.setPreferredSize(new java.awt.Dimension(40, 40));
        bt173.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt173ActionPerformed(evt);
            }
        });

        bt174.setBackground(Values.corBlocos);
        bt174.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt174.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt174.setPreferredSize(new java.awt.Dimension(40, 40));
        bt174.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt174ActionPerformed(evt);
            }
        });

        bt175.setBackground(Values.corBlocos);
        bt175.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt175.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt175.setPreferredSize(new java.awt.Dimension(40, 40));
        bt175.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt175ActionPerformed(evt);
            }
        });

        bt176.setBackground(Values.corBlocos);
        bt176.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt176.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt176.setPreferredSize(new java.awt.Dimension(40, 40));
        bt176.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt176ActionPerformed(evt);
            }
        });

        bt177.setBackground(Values.corBlocos);
        bt177.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt177.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt177.setPreferredSize(new java.awt.Dimension(40, 40));
        bt177.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt177ActionPerformed(evt);
            }
        });

        bt178.setBackground(Values.corBlocos);
        bt178.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt178.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt178.setPreferredSize(new java.awt.Dimension(40, 40));
        bt178.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt178ActionPerformed(evt);
            }
        });

        bt179.setBackground(Values.corBlocos);
        bt179.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt179.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt179.setPreferredSize(new java.awt.Dimension(40, 40));
        bt179.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt179ActionPerformed(evt);
            }
        });

        bt180.setBackground(Values.corBlocos);
        bt180.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt180.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt180.setPreferredSize(new java.awt.Dimension(40, 40));
        bt180.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt180ActionPerformed(evt);
            }
        });

        bt181.setBackground(Values.corBlocos);
        bt181.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt181.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt181.setPreferredSize(new java.awt.Dimension(40, 40));
        bt181.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt181ActionPerformed(evt);
            }
        });

        bt182.setBackground(Values.corBlocos);
        bt182.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt182.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt182.setPreferredSize(new java.awt.Dimension(40, 40));
        bt182.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt182ActionPerformed(evt);
            }
        });

        bt183.setBackground(Values.corBlocos);
        bt183.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt183.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt183.setPreferredSize(new java.awt.Dimension(40, 40));
        bt183.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt183ActionPerformed(evt);
            }
        });

        bt184.setBackground(Values.corBlocos);
        bt184.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt184.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt184.setPreferredSize(new java.awt.Dimension(40, 40));
        bt184.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt184ActionPerformed(evt);
            }
        });

        bt185.setBackground(Values.corBlocos);
        bt185.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt185.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt185.setPreferredSize(new java.awt.Dimension(40, 40));
        bt185.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt185ActionPerformed(evt);
            }
        });

        bt186.setBackground(Values.corBlocos);
        bt186.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt186.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt186.setPreferredSize(new java.awt.Dimension(40, 40));
        bt186.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt186ActionPerformed(evt);
            }
        });

        bt187.setBackground(Values.corBlocos);
        bt187.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt187.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt187.setPreferredSize(new java.awt.Dimension(40, 40));
        bt187.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt187ActionPerformed(evt);
            }
        });

        bt188.setBackground(Values.corBlocos);
        bt188.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt188.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt188.setPreferredSize(new java.awt.Dimension(40, 40));
        bt188.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt188ActionPerformed(evt);
            }
        });

        bt189.setBackground(Values.corBlocos);
        bt189.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt189.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt189.setPreferredSize(new java.awt.Dimension(40, 40));
        bt189.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt189ActionPerformed(evt);
            }
        });

        bt190.setBackground(Values.corBlocos);
        bt190.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt190.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt190.setPreferredSize(new java.awt.Dimension(40, 40));
        bt190.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt190ActionPerformed(evt);
            }
        });

        bt191.setBackground(Values.corBlocos);
        bt191.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt191.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt191.setPreferredSize(new java.awt.Dimension(40, 40));
        bt191.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt191ActionPerformed(evt);
            }
        });

        bt192.setBackground(Values.corBlocos);
        bt192.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt192.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt192.setPreferredSize(new java.awt.Dimension(40, 40));
        bt192.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt192ActionPerformed(evt);
            }
        });

        bt193.setBackground(Values.corBlocos);
        bt193.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt193.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt193.setPreferredSize(new java.awt.Dimension(40, 40));
        bt193.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt193ActionPerformed(evt);
            }
        });

        bt194.setBackground(Values.corBlocos);
        bt194.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt194.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt194.setPreferredSize(new java.awt.Dimension(40, 40));
        bt194.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt194ActionPerformed(evt);
            }
        });

        bt195.setBackground(Values.corBlocos);
        bt195.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt195.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt195.setPreferredSize(new java.awt.Dimension(40, 40));
        bt195.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt195ActionPerformed(evt);
            }
        });

        bt196.setBackground(Values.corBlocos);
        bt196.setFont(new java.awt.Font("Earthquake MF", 0, 14)); // NOI18N
        bt196.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        bt196.setPreferredSize(new java.awt.Dimension(40, 40));
        bt196.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bt196ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout painelJogoDificilLayout = new javax.swing.GroupLayout(painelJogoDificil);
        painelJogoDificil.setLayout(painelJogoDificilLayout);
        painelJogoDificilLayout.setHorizontalGroup(
            painelJogoDificilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelJogoDificilLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelJogoDificilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(painelJogoDificilLayout.createSequentialGroup()
                        .addGroup(painelJogoDificilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(painelJogoDificilLayout.createSequentialGroup()
                                .addComponent(bt1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(painelJogoDificilLayout.createSequentialGroup()
                                .addComponent(bt29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt36, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt38, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt40, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt41, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt42, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(painelJogoDificilLayout.createSequentialGroup()
                                .addComponent(bt43, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt44, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt45, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt46, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt47, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt48, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt49, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt50, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt51, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt52, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt53, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt54, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt55, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt56, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(painelJogoDificilLayout.createSequentialGroup()
                                .addComponent(bt57, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt58, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt59, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt60, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt61, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt62, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt63, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt64, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt65, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt66, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt67, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt68, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt69, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt70, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(painelJogoDificilLayout.createSequentialGroup()
                                .addComponent(bt71, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt72, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt73, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt74, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt75, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt76, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt77, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt78, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt79, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt80, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt81, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt82, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt83, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt84, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(painelJogoDificilLayout.createSequentialGroup()
                                .addComponent(bt85, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt86, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt87, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt88, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt89, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt90, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt91, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt92, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt93, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt94, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt95, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt96, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt97, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt98, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(painelJogoDificilLayout.createSequentialGroup()
                                .addComponent(bt99, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt100, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt101, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt102, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt103, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt104, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt105, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt106, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt107, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt108, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt109, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt110, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt111, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt112, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(painelJogoDificilLayout.createSequentialGroup()
                                .addComponent(bt113, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt114, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt115, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt116, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt117, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt118, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt119, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt120, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt121, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt122, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt123, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt124, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt125, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt126, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(painelJogoDificilLayout.createSequentialGroup()
                                .addComponent(bt127, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt128, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt129, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt130, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt131, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt132, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt133, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt134, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt135, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt136, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt137, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt138, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt139, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt140, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(painelJogoDificilLayout.createSequentialGroup()
                                .addComponent(bt141, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt142, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt143, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt144, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt145, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt146, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt147, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt148, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt149, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt150, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt151, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt152, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt153, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt154, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(painelJogoDificilLayout.createSequentialGroup()
                                .addComponent(bt155, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt156, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt157, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt158, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt159, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt160, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt161, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt162, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt163, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt164, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt165, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt166, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt167, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt168, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, painelJogoDificilLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(painelJogoDificilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, painelJogoDificilLayout.createSequentialGroup()
                                .addComponent(bt15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, painelJogoDificilLayout.createSequentialGroup()
                                .addComponent(bt169, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt170, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt171, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt172, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt173, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt174, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt175, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt176, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt177, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt178, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt179, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt180, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt181, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt182, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, painelJogoDificilLayout.createSequentialGroup()
                                .addComponent(bt183, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt184, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt185, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt186, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt187, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt188, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt189, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt190, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt191, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt192, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt193, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt194, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt195, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(bt196, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap())
        );
        painelJogoDificilLayout.setVerticalGroup(
            painelJogoDificilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelJogoDificilLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelJogoDificilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt13, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt12, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt11, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelJogoDificilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt28, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt27, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt26, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt25, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt24, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt22, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt17, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt15, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelJogoDificilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt42, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt41, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt40, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt39, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt38, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt36, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt35, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt34, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt33, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt32, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt31, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt30, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt29, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelJogoDificilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt56, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt55, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt54, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt53, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt52, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt51, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt50, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt49, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt48, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt47, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt46, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt45, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt44, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt43, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelJogoDificilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt70, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt69, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt68, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt67, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt66, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt65, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt64, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt63, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt62, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt61, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt60, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt59, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt58, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt57, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelJogoDificilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt84, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt83, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt82, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt81, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt80, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt79, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt78, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt77, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt76, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt75, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt74, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt73, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt72, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt71, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelJogoDificilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt98, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt97, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt96, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt95, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt94, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt93, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt92, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt91, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt90, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt89, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt88, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt87, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt86, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt85, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelJogoDificilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt112, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt111, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt110, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt109, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt108, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt107, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt106, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt105, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt104, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt103, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt102, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt101, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt100, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt99, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelJogoDificilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt126, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt125, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt124, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt123, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt122, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt121, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt120, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt119, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt118, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt117, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt116, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt115, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt114, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt113, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelJogoDificilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt140, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt139, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt138, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt137, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt136, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt135, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt134, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt133, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt132, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt131, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt130, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt129, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt128, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt127, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelJogoDificilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt154, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt153, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt152, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt151, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt150, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt149, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt148, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt147, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt146, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt145, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt144, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt143, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt142, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt141, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelJogoDificilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt168, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt167, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt166, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt165, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt164, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt163, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt162, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt161, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt160, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt159, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt158, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt157, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt156, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt155, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelJogoDificilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt182, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt181, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt180, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt179, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt178, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt177, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt176, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt175, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt174, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt173, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt172, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt171, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt170, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt169, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(painelJogoDificilLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(bt196, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt195, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt194, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt193, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt192, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt191, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt190, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt189, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt188, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt187, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt186, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt185, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt184, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(bt183, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        painelInfo.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Informações", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Comic Sans MS", 0, 16), new java.awt.Color(255, 0, 0))); // NOI18N
        painelInfo.setPreferredSize(new java.awt.Dimension(272, 543));

        painelPontuacao.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Pontuação", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Comic Sans MS", 0, 16), new java.awt.Color(0, 0, 0))); // NOI18N
        painelPontuacao.setToolTipText("Sua pontuação atual");
        painelPontuacao.setPreferredSize(new java.awt.Dimension(203, 70));

        lbPontuacao.setFont(new java.awt.Font("Comic Sans MS", 1, 14)); // NOI18N

        javax.swing.GroupLayout painelPontuacaoLayout = new javax.swing.GroupLayout(painelPontuacao);
        painelPontuacao.setLayout(painelPontuacaoLayout);
        painelPontuacaoLayout.setHorizontalGroup(
            painelPontuacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelPontuacaoLayout.createSequentialGroup()
                .addGap(75, 75, 75)
                .addComponent(lbPontuacao)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        painelPontuacaoLayout.setVerticalGroup(
            painelPontuacaoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelPontuacaoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbPontuacao)
                .addContainerGap(28, Short.MAX_VALUE))
        );

        painelJogador.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Informações", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Comic Sans MS", 0, 16), new java.awt.Color(0, 0, 0))); // NOI18N
        painelJogador.setToolTipText("Informações extras sobre a partida");

        lbNivelDificuldade.setFont(new java.awt.Font("Comic Sans MS", 0, 15)); // NOI18N
        lbNivelDificuldade.setForeground(new java.awt.Color(255, 0, 0));
        lbNivelDificuldade.setText("Nível: Difícil");

        avatarIcon.setIcon(new javax.swing.ImageIcon(getClass().getResource("/CampoMinado/Imagens/blank_avatar.jpg"))); // NOI18N
        avatarIcon.setToolTipText(TabelaScores.getNickJogador());

        javax.swing.GroupLayout painelJogadorLayout = new javax.swing.GroupLayout(painelJogador);
        painelJogador.setLayout(painelJogadorLayout);
        painelJogadorLayout.setHorizontalGroup(
            painelJogadorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelJogadorLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelJogadorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(painelJogadorLayout.createSequentialGroup()
                        .addComponent(lbNivelDificuldade)
                        .addContainerGap(177, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, painelJogadorLayout.createSequentialGroup()
                        .addGap(0, 74, Short.MAX_VALUE)
                        .addComponent(avatarIcon)
                        .addGap(79, 79, 79))))
        );
        painelJogadorLayout.setVerticalGroup(
            painelJogadorLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, painelJogadorLayout.createSequentialGroup()
                .addGap(109, 109, 109)
                .addComponent(avatarIcon)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbNivelDificuldade)
                .addContainerGap())
        );

        painelMenuRapido.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Menu", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Comic Sans MS", 0, 16), new java.awt.Color(0, 0, 0))); // NOI18N
        painelMenuRapido.setToolTipText("Menu de opções rápidas");

        btRestart.setFont(new java.awt.Font("Comic Sans MS", 1, 16)); // NOI18N
        btRestart.setForeground(new java.awt.Color(255, 0, 0));
        btRestart.setText("Reiniciar Partida");
        btRestart.setToolTipText("Clique aqui para reiniciar a partida atual");
        btRestart.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btRestart.setPreferredSize(new java.awt.Dimension(81, 30));
        btRestart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btRestartActionPerformed(evt);
            }
        });

        btDesistir.setFont(new java.awt.Font("Comic Sans MS", 1, 16)); // NOI18N
        btDesistir.setForeground(new java.awt.Color(255, 0, 0));
        btDesistir.setText("Abandonar Partida");
        btDesistir.setToolTipText("Clique aqui para abandonar a partida atual");
        btDesistir.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btDesistir.setPreferredSize(new java.awt.Dimension(81, 30));
        btDesistir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btDesistirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout painelMenuRapidoLayout = new javax.swing.GroupLayout(painelMenuRapido);
        painelMenuRapido.setLayout(painelMenuRapidoLayout);
        painelMenuRapidoLayout.setHorizontalGroup(
            painelMenuRapidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelMenuRapidoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelMenuRapidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btDesistir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btRestart, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        painelMenuRapidoLayout.setVerticalGroup(
            painelMenuRapidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelMenuRapidoLayout.createSequentialGroup()
                .addGap(25, 25, 25)
                .addComponent(btRestart, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btDesistir, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(29, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout painelInfoLayout = new javax.swing.GroupLayout(painelInfo);
        painelInfo.setLayout(painelInfoLayout);
        painelInfoLayout.setHorizontalGroup(
            painelInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, painelInfoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(painelInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(painelJogador, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(painelMenuRapido, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(painelPontuacao, javax.swing.GroupLayout.DEFAULT_SIZE, 285, Short.MAX_VALUE))
                .addContainerGap())
        );
        painelInfoLayout.setVerticalGroup(
            painelInfoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(painelInfoLayout.createSequentialGroup()
                .addComponent(painelPontuacao, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(painelJogador, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(painelMenuRapido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(8, 8, 8)
                .addComponent(painelJogoDificil, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(painelInfo, javax.swing.GroupLayout.PREFERRED_SIZE, 319, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(painelJogoDificil, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(painelInfo, javax.swing.GroupLayout.DEFAULT_SIZE, 666, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btRestartActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btRestartActionPerformed
        Values.recomecarPartida();
        dispose();
    }//GEN-LAST:event_btRestartActionPerformed

    private void btDesistirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btDesistirActionPerformed
        if(Values.voltarMenuPrincipal(TelaJogoDificil.this) == 0){
        dispose();
        }
    }//GEN-LAST:event_btDesistirActionPerformed

    private void bt1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt1ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt1, (byte)0, (byte)0);
    }//GEN-LAST:event_bt1ActionPerformed

    private void bt2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt2ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt2, (byte)0, (byte)1);
    }//GEN-LAST:event_bt2ActionPerformed

    private void bt3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt3ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt3, (byte)0, (byte)2);
    }//GEN-LAST:event_bt3ActionPerformed

    private void bt4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt4ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt4, (byte)0, (byte)3);
    }//GEN-LAST:event_bt4ActionPerformed

    private void bt5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt5ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt5, (byte)0, (byte)4);
    }//GEN-LAST:event_bt5ActionPerformed

    private void bt6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt6ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt6, (byte)0, (byte)5);
    }//GEN-LAST:event_bt6ActionPerformed

    private void bt7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt7ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt7, (byte)0, (byte)6);
    }//GEN-LAST:event_bt7ActionPerformed

    private void bt8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt8ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt8, (byte)0, (byte)7);
    }//GEN-LAST:event_bt8ActionPerformed

    private void bt9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt9ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt9, (byte)0, (byte)8);
    }//GEN-LAST:event_bt9ActionPerformed

    private void bt10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt10ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt10, (byte)0, (byte)9);
    }//GEN-LAST:event_bt10ActionPerformed

    private void bt11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt11ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt11, (byte)0, (byte)10);
    }//GEN-LAST:event_bt11ActionPerformed

    private void bt12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt12ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt12, (byte)0, (byte)11);
    }//GEN-LAST:event_bt12ActionPerformed

    private void bt13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt13ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt13, (byte)0, (byte)12);
    }//GEN-LAST:event_bt13ActionPerformed

    private void bt14ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt14ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt14, (byte)0, (byte)13);
    }//GEN-LAST:event_bt14ActionPerformed

    private void bt15ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt15ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt15, (byte)1, (byte)0);
    }//GEN-LAST:event_bt15ActionPerformed

    private void bt16ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt16ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt16, (byte)1, (byte)1);
    }//GEN-LAST:event_bt16ActionPerformed

    private void bt17ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt17ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt17, (byte)1, (byte)2);
    }//GEN-LAST:event_bt17ActionPerformed

    private void bt18ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt18ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt18, (byte)1, (byte)3);
    }//GEN-LAST:event_bt18ActionPerformed

    private void bt19ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt19ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt19, (byte)1, (byte)4);
    }//GEN-LAST:event_bt19ActionPerformed

    private void bt20ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt20ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt20, (byte)1, (byte)5);
    }//GEN-LAST:event_bt20ActionPerformed

    private void bt21ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt21ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt21, (byte)1, (byte)6);
    }//GEN-LAST:event_bt21ActionPerformed

    private void bt22ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt22ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt22, (byte)1, (byte)7);
    }//GEN-LAST:event_bt22ActionPerformed

    private void bt23ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt23ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt23, (byte)1, (byte)8);
    }//GEN-LAST:event_bt23ActionPerformed

    private void bt24ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt24ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt24, (byte)1, (byte)9);
    }//GEN-LAST:event_bt24ActionPerformed

    private void bt25ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt25ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt25, (byte)1, (byte)10);
    }//GEN-LAST:event_bt25ActionPerformed

    private void bt26ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt26ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt26, (byte)1, (byte)11);
    }//GEN-LAST:event_bt26ActionPerformed

    private void bt27ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt27ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt27, (byte)1, (byte)12);
    }//GEN-LAST:event_bt27ActionPerformed

    private void bt28ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt28ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt28, (byte)1, (byte)13);
    }//GEN-LAST:event_bt28ActionPerformed

    private void bt29ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt29ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt29, (byte)2, (byte)0);
    }//GEN-LAST:event_bt29ActionPerformed

    private void bt30ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt30ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt30, (byte)2, (byte)1);
    }//GEN-LAST:event_bt30ActionPerformed

    private void bt31ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt31ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt31, (byte)2, (byte)2);
    }//GEN-LAST:event_bt31ActionPerformed

    private void bt32ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt32ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt32, (byte)2, (byte)3);
    }//GEN-LAST:event_bt32ActionPerformed

    private void bt33ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt33ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt33, (byte)2, (byte)4);
    }//GEN-LAST:event_bt33ActionPerformed

    private void bt34ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt34ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt34, (byte)2, (byte)5);
    }//GEN-LAST:event_bt34ActionPerformed

    private void bt35ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt35ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt35, (byte)2, (byte)6);
    }//GEN-LAST:event_bt35ActionPerformed

    private void bt36ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt36ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt36, (byte)2, (byte)7);
    }//GEN-LAST:event_bt36ActionPerformed

    private void bt37ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt37ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt37, (byte)2, (byte)8);
    }//GEN-LAST:event_bt37ActionPerformed

    private void bt38ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt38ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt38, (byte)2, (byte)9);
    }//GEN-LAST:event_bt38ActionPerformed

    private void bt39ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt39ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt39, (byte)2, (byte)10);
    }//GEN-LAST:event_bt39ActionPerformed

    private void bt40ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt40ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt40, (byte)2, (byte)11);
    }//GEN-LAST:event_bt40ActionPerformed

    private void bt41ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt41ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt41, (byte)2, (byte)12);
    }//GEN-LAST:event_bt41ActionPerformed

    private void bt42ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt42ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt42, (byte)2, (byte)13);
    }//GEN-LAST:event_bt42ActionPerformed

    private void bt43ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt43ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt43, (byte)3, (byte)0);
    }//GEN-LAST:event_bt43ActionPerformed

    private void bt44ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt44ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt44, (byte)3, (byte)1);
    }//GEN-LAST:event_bt44ActionPerformed

    private void bt45ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt45ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt45, (byte)3, (byte)2);
    }//GEN-LAST:event_bt45ActionPerformed

    private void bt46ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt46ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt46, (byte)3, (byte)3);
    }//GEN-LAST:event_bt46ActionPerformed

    private void bt47ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt47ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt47, (byte)3, (byte)4);
    }//GEN-LAST:event_bt47ActionPerformed

    private void bt48ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt48ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt48, (byte)3, (byte)5);
    }//GEN-LAST:event_bt48ActionPerformed

    private void bt49ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt49ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt49, (byte)3, (byte)6);
    }//GEN-LAST:event_bt49ActionPerformed

    private void bt50ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt50ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt50, (byte)3, (byte)7);
    }//GEN-LAST:event_bt50ActionPerformed

    private void bt51ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt51ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt51, (byte)3, (byte)8);
    }//GEN-LAST:event_bt51ActionPerformed

    private void bt52ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt52ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt52, (byte)3, (byte)9);
    }//GEN-LAST:event_bt52ActionPerformed

    private void bt53ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt53ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt53, (byte)3, (byte)10);
    }//GEN-LAST:event_bt53ActionPerformed

    private void bt54ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt54ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt54, (byte)3, (byte)11);
    }//GEN-LAST:event_bt54ActionPerformed

    private void bt55ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt55ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt55, (byte)3, (byte)12);
    }//GEN-LAST:event_bt55ActionPerformed

    private void bt56ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt56ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt56, (byte)3, (byte)13);
    }//GEN-LAST:event_bt56ActionPerformed

    private void bt57ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt57ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt57, (byte)4, (byte)0);
    }//GEN-LAST:event_bt57ActionPerformed

    private void bt58ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt58ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt58, (byte)4, (byte)1);
    }//GEN-LAST:event_bt58ActionPerformed

    private void bt59ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt59ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt59, (byte)4, (byte)2);
    }//GEN-LAST:event_bt59ActionPerformed

    private void bt60ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt60ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt60, (byte)4, (byte)3);
    }//GEN-LAST:event_bt60ActionPerformed

    private void bt61ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt61ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt61, (byte)4, (byte)4);
    }//GEN-LAST:event_bt61ActionPerformed

    private void bt62ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt62ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt62, (byte)4, (byte)5);
    }//GEN-LAST:event_bt62ActionPerformed

    private void bt63ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt63ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt63, (byte)4, (byte)6);
    }//GEN-LAST:event_bt63ActionPerformed

    private void bt64ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt64ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt64, (byte)4, (byte)7);
    }//GEN-LAST:event_bt64ActionPerformed

    private void bt65ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt65ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt65, (byte)4, (byte)8);
    }//GEN-LAST:event_bt65ActionPerformed

    private void bt66ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt66ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt66, (byte)4, (byte)9);
    }//GEN-LAST:event_bt66ActionPerformed

    private void bt67ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt67ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt67, (byte)4, (byte)10);
    }//GEN-LAST:event_bt67ActionPerformed

    private void bt68ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt68ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt68, (byte)4, (byte)11);
    }//GEN-LAST:event_bt68ActionPerformed

    private void bt69ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt69ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt69, (byte)4, (byte)12);
    }//GEN-LAST:event_bt69ActionPerformed

    private void bt70ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt70ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt70, (byte)4, (byte)13);
    }//GEN-LAST:event_bt70ActionPerformed

    private void bt71ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt71ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt71, (byte)5, (byte)0);
    }//GEN-LAST:event_bt71ActionPerformed

    private void bt72ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt72ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt72, (byte)5, (byte)1);
    }//GEN-LAST:event_bt72ActionPerformed

    private void bt73ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt73ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt73, (byte)5, (byte)2);
    }//GEN-LAST:event_bt73ActionPerformed

    private void bt74ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt74ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt74, (byte)5, (byte)3);
    }//GEN-LAST:event_bt74ActionPerformed

    private void bt75ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt75ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt75, (byte)5, (byte)4);
    }//GEN-LAST:event_bt75ActionPerformed

    private void bt76ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt76ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt76, (byte)5, (byte)5);
    }//GEN-LAST:event_bt76ActionPerformed

    private void bt77ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt77ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt77, (byte)5, (byte)6);
    }//GEN-LAST:event_bt77ActionPerformed

    private void bt78ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt78ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt78, (byte)5, (byte)7);
    }//GEN-LAST:event_bt78ActionPerformed

    private void bt79ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt79ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt79, (byte)5, (byte)8);
    }//GEN-LAST:event_bt79ActionPerformed

    private void bt80ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt80ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt80, (byte)5, (byte)9);
    }//GEN-LAST:event_bt80ActionPerformed

    private void bt81ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt81ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt81, (byte)5, (byte)10);
    }//GEN-LAST:event_bt81ActionPerformed

    private void bt82ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt82ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt82, (byte)5, (byte)11);
    }//GEN-LAST:event_bt82ActionPerformed

    private void bt83ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt83ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt83, (byte)5, (byte)12);
    }//GEN-LAST:event_bt83ActionPerformed

    private void bt84ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt84ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt84, (byte)5, (byte)13);
    }//GEN-LAST:event_bt84ActionPerformed

    private void bt85ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt85ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt85, (byte)6, (byte)0);
    }//GEN-LAST:event_bt85ActionPerformed

    private void bt86ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt86ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt86, (byte)6, (byte)1);
    }//GEN-LAST:event_bt86ActionPerformed

    private void bt87ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt87ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt87, (byte)6, (byte)2);
    }//GEN-LAST:event_bt87ActionPerformed

    private void bt88ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt88ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt88, (byte)6, (byte)3);
    }//GEN-LAST:event_bt88ActionPerformed

    private void bt89ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt89ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt89, (byte)6, (byte)4);
    }//GEN-LAST:event_bt89ActionPerformed

    private void bt90ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt90ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt90, (byte)6, (byte)5);
    }//GEN-LAST:event_bt90ActionPerformed

    private void bt91ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt91ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt91, (byte)6, (byte)6);
    }//GEN-LAST:event_bt91ActionPerformed

    private void bt92ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt92ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt92, (byte)6, (byte)7);
    }//GEN-LAST:event_bt92ActionPerformed

    private void bt93ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt93ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt93, (byte)6, (byte)8);
    }//GEN-LAST:event_bt93ActionPerformed

    private void bt94ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt94ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt94, (byte)6, (byte)9);
    }//GEN-LAST:event_bt94ActionPerformed

    private void bt95ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt95ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt95, (byte)6, (byte)10);
    }//GEN-LAST:event_bt95ActionPerformed

    private void bt96ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt96ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt96, (byte)6, (byte)11);
    }//GEN-LAST:event_bt96ActionPerformed

    private void bt97ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt97ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt97, (byte)6, (byte)12);
    }//GEN-LAST:event_bt97ActionPerformed

    private void bt98ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt98ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt98, (byte)6, (byte)13);
    }//GEN-LAST:event_bt98ActionPerformed

    private void bt99ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt99ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt99, (byte)7, (byte)0);
    }//GEN-LAST:event_bt99ActionPerformed

    private void bt100ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt100ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt100, (byte)7, (byte)1);
    }//GEN-LAST:event_bt100ActionPerformed

    private void bt101ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt101ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt101, (byte)7, (byte)2);
    }//GEN-LAST:event_bt101ActionPerformed

    private void bt102ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt102ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt102, (byte)7, (byte)3);
    }//GEN-LAST:event_bt102ActionPerformed

    private void bt103ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt103ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt103, (byte)7, (byte)4);
    }//GEN-LAST:event_bt103ActionPerformed

    private void bt104ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt104ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt104, (byte)7, (byte)5);
    }//GEN-LAST:event_bt104ActionPerformed

    private void bt105ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt105ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt105, (byte)7, (byte)6);
    }//GEN-LAST:event_bt105ActionPerformed

    private void bt106ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt106ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt106, (byte)7, (byte)7);
    }//GEN-LAST:event_bt106ActionPerformed

    private void bt107ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt107ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt107, (byte)7, (byte)8);
    }//GEN-LAST:event_bt107ActionPerformed

    private void bt108ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt108ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt108, (byte)7, (byte)9);
    }//GEN-LAST:event_bt108ActionPerformed

    private void bt109ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt109ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt109, (byte)7, (byte)10);
    }//GEN-LAST:event_bt109ActionPerformed

    private void bt110ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt110ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt110, (byte)7, (byte)11);
    }//GEN-LAST:event_bt110ActionPerformed

    private void bt111ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt111ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt111, (byte)7, (byte)12);
    }//GEN-LAST:event_bt111ActionPerformed

    private void bt112ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt112ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt112, (byte)7, (byte)13);
    }//GEN-LAST:event_bt112ActionPerformed

    private void bt113ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt113ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt113, (byte)8, (byte)0);
    }//GEN-LAST:event_bt113ActionPerformed

    private void bt114ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt114ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt114, (byte)8, (byte)1);
    }//GEN-LAST:event_bt114ActionPerformed

    private void bt115ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt115ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt115, (byte)8, (byte)2);
    }//GEN-LAST:event_bt115ActionPerformed

    private void bt116ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt116ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt116, (byte)8, (byte)3);
    }//GEN-LAST:event_bt116ActionPerformed

    private void bt117ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt117ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt117, (byte)8, (byte)4);
    }//GEN-LAST:event_bt117ActionPerformed

    private void bt118ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt118ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt118, (byte)8, (byte)5);
    }//GEN-LAST:event_bt118ActionPerformed

    private void bt119ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt119ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt119, (byte)8, (byte)6);
    }//GEN-LAST:event_bt119ActionPerformed

    private void bt120ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt120ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt120, (byte)8, (byte)7);
    }//GEN-LAST:event_bt120ActionPerformed

    private void bt121ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt121ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt121, (byte)8, (byte)8);
    }//GEN-LAST:event_bt121ActionPerformed

    private void bt122ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt122ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt122, (byte)8, (byte)9);
    }//GEN-LAST:event_bt122ActionPerformed

    private void bt123ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt123ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt123, (byte)8, (byte)10);
    }//GEN-LAST:event_bt123ActionPerformed

    private void bt124ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt124ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt124, (byte)8, (byte)11);
    }//GEN-LAST:event_bt124ActionPerformed

    private void bt125ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt125ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt125, (byte)8, (byte)12);
    }//GEN-LAST:event_bt125ActionPerformed

    private void bt126ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt126ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt126, (byte)8, (byte)13);
    }//GEN-LAST:event_bt126ActionPerformed

    private void bt127ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt127ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt127, (byte)9, (byte)0);
    }//GEN-LAST:event_bt127ActionPerformed

    private void bt128ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt128ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt128, (byte)9, (byte)1);
    }//GEN-LAST:event_bt128ActionPerformed

    private void bt129ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt129ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt129, (byte)9, (byte)2);
    }//GEN-LAST:event_bt129ActionPerformed

    private void bt130ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt130ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt130, (byte)9, (byte)3);
    }//GEN-LAST:event_bt130ActionPerformed

    private void bt131ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt131ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt131, (byte)9, (byte)4);
    }//GEN-LAST:event_bt131ActionPerformed

    private void bt132ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt132ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt132, (byte)9, (byte)5);
    }//GEN-LAST:event_bt132ActionPerformed

    private void bt133ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt133ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt133, (byte)9, (byte)6);
    }//GEN-LAST:event_bt133ActionPerformed

    private void bt134ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt134ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt134, (byte)9, (byte)7);
    }//GEN-LAST:event_bt134ActionPerformed

    private void bt135ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt135ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt135, (byte)9, (byte)8);
    }//GEN-LAST:event_bt135ActionPerformed

    private void bt136ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt136ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt136, (byte)9, (byte)9);
    }//GEN-LAST:event_bt136ActionPerformed

    private void bt137ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt137ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt137, (byte)9, (byte)10);
    }//GEN-LAST:event_bt137ActionPerformed

    private void bt138ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt138ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt138, (byte)9, (byte)11);
    }//GEN-LAST:event_bt138ActionPerformed

    private void bt139ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt139ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt139, (byte)9, (byte)12);
    }//GEN-LAST:event_bt139ActionPerformed

    private void bt140ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt140ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt140, (byte)9, (byte)13);
    }//GEN-LAST:event_bt140ActionPerformed

    private void bt141ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt141ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt141, (byte)10, (byte)0);
    }//GEN-LAST:event_bt141ActionPerformed

    private void bt142ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt142ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt142, (byte)10, (byte)1);
    }//GEN-LAST:event_bt142ActionPerformed

    private void bt143ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt143ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt143, (byte)10, (byte)2);
    }//GEN-LAST:event_bt143ActionPerformed

    private void bt144ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt144ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt144, (byte)10, (byte)3);
    }//GEN-LAST:event_bt144ActionPerformed

    private void bt145ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt145ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt145, (byte)10, (byte)4);
    }//GEN-LAST:event_bt145ActionPerformed

    private void bt146ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt146ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt146, (byte)10, (byte)5);
    }//GEN-LAST:event_bt146ActionPerformed

    private void bt147ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt147ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt147, (byte)10, (byte)6);
    }//GEN-LAST:event_bt147ActionPerformed

    private void bt148ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt148ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt148, (byte)10, (byte)7);
    }//GEN-LAST:event_bt148ActionPerformed

    private void bt149ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt149ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt149, (byte)10, (byte)8);
    }//GEN-LAST:event_bt149ActionPerformed

    private void bt150ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt150ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt150, (byte)10, (byte)9);
    }//GEN-LAST:event_bt150ActionPerformed

    private void bt151ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt151ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt151, (byte)10, (byte)10);
    }//GEN-LAST:event_bt151ActionPerformed

    private void bt152ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt152ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt152, (byte)10, (byte)11);
    }//GEN-LAST:event_bt152ActionPerformed

    private void bt153ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt153ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt153, (byte)10, (byte)12);
    }//GEN-LAST:event_bt153ActionPerformed

    private void bt154ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt154ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt154, (byte)10, (byte)13);
    }//GEN-LAST:event_bt154ActionPerformed

    private void bt155ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt155ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt155, (byte)11, (byte)0);
    }//GEN-LAST:event_bt155ActionPerformed

    private void bt156ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt156ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt156, (byte)11, (byte)1);
    }//GEN-LAST:event_bt156ActionPerformed

    private void bt157ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt157ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt157, (byte)11, (byte)2);
    }//GEN-LAST:event_bt157ActionPerformed

    private void bt158ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt158ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt158, (byte)11, (byte)3);
    }//GEN-LAST:event_bt158ActionPerformed

    private void bt159ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt159ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt159, (byte)11, (byte)4);
    }//GEN-LAST:event_bt159ActionPerformed

    private void bt160ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt160ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt160, (byte)11, (byte)5);
    }//GEN-LAST:event_bt160ActionPerformed

    private void bt161ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt161ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt161, (byte)11, (byte)6);
    }//GEN-LAST:event_bt161ActionPerformed

    private void bt162ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt162ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt162, (byte)11, (byte)7);
    }//GEN-LAST:event_bt162ActionPerformed

    private void bt163ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt163ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt163, (byte)11, (byte)8);
    }//GEN-LAST:event_bt163ActionPerformed

    private void bt164ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt164ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt164, (byte)11, (byte)9);
    }//GEN-LAST:event_bt164ActionPerformed

    private void bt165ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt165ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt165, (byte)11, (byte)10);
    }//GEN-LAST:event_bt165ActionPerformed

    private void bt166ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt166ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt166, (byte)11, (byte)11);
    }//GEN-LAST:event_bt166ActionPerformed

    private void bt167ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt167ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt167, (byte)11, (byte)12);
    }//GEN-LAST:event_bt167ActionPerformed

    private void bt168ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt168ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt168, (byte)11, (byte)13);
    }//GEN-LAST:event_bt168ActionPerformed

    private void bt169ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt169ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt169, (byte)12, (byte)0);
    }//GEN-LAST:event_bt169ActionPerformed

    private void bt170ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt170ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt170, (byte)12, (byte)1);
    }//GEN-LAST:event_bt170ActionPerformed

    private void bt171ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt171ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt171, (byte)12, (byte)2);
    }//GEN-LAST:event_bt171ActionPerformed

    private void bt172ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt172ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt172, (byte)12, (byte)3);
    }//GEN-LAST:event_bt172ActionPerformed

    private void bt173ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt173ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt173, (byte)12, (byte)4);
    }//GEN-LAST:event_bt173ActionPerformed

    private void bt174ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt174ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt174, (byte)12, (byte)5);
    }//GEN-LAST:event_bt174ActionPerformed

    private void bt175ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt175ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt175, (byte)12, (byte)6);
    }//GEN-LAST:event_bt175ActionPerformed

    private void bt176ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt176ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt176, (byte)12, (byte)7);
    }//GEN-LAST:event_bt176ActionPerformed

    private void bt177ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt177ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt177, (byte)12, (byte)8);
    }//GEN-LAST:event_bt177ActionPerformed

    private void bt178ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt178ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt178, (byte)12, (byte)9);
    }//GEN-LAST:event_bt178ActionPerformed

    private void bt179ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt179ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt179, (byte)12, (byte)10);
    }//GEN-LAST:event_bt179ActionPerformed

    private void bt180ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt180ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt180, (byte)12, (byte)11);
    }//GEN-LAST:event_bt180ActionPerformed

    private void bt181ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt181ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt181, (byte)12, (byte)12);
    }//GEN-LAST:event_bt181ActionPerformed

    private void bt182ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt182ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt181, (byte)12, (byte)13);
    }//GEN-LAST:event_bt182ActionPerformed

    private void bt183ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt183ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt183, (byte)13, (byte)0);
    }//GEN-LAST:event_bt183ActionPerformed

    private void bt184ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt184ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt184, (byte)13, (byte)1);
    }//GEN-LAST:event_bt184ActionPerformed

    private void bt185ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt185ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt185, (byte)13, (byte)2);
    }//GEN-LAST:event_bt185ActionPerformed

    private void bt186ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt186ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt186, (byte)13, (byte)3);
    }//GEN-LAST:event_bt186ActionPerformed

    private void bt187ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt187ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt187, (byte)13, (byte)4);
    }//GEN-LAST:event_bt187ActionPerformed

    private void bt188ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt188ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt188, (byte)13, (byte)5);
    }//GEN-LAST:event_bt188ActionPerformed

    private void bt189ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt189ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt189, (byte)13, (byte)6);
    }//GEN-LAST:event_bt189ActionPerformed

    private void bt190ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt190ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt190, (byte)13, (byte)7);
    }//GEN-LAST:event_bt190ActionPerformed

    private void bt191ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt191ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt191, (byte)13, (byte)8);
    }//GEN-LAST:event_bt191ActionPerformed

    private void bt192ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt192ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt192, (byte)13, (byte)9);
    }//GEN-LAST:event_bt192ActionPerformed

    private void bt193ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt193ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt193, (byte)13, (byte)10);
    }//GEN-LAST:event_bt193ActionPerformed

    private void bt194ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt194ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt194, (byte)13, (byte)11);
    }//GEN-LAST:event_bt194ActionPerformed

    private void bt195ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt195ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt195, (byte)13, (byte)12);
    }//GEN-LAST:event_bt195ActionPerformed

    private void bt196ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_bt196ActionPerformed
        Values.clickBloco(TelaJogoDificil.this, lbPontuacao, bt196, (byte)13, (byte)13);
    }//GEN-LAST:event_bt196ActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel avatarIcon;
    private javax.swing.JButton bt1;
    private javax.swing.JButton bt10;
    private javax.swing.JButton bt100;
    private javax.swing.JButton bt101;
    private javax.swing.JButton bt102;
    private javax.swing.JButton bt103;
    private javax.swing.JButton bt104;
    private javax.swing.JButton bt105;
    private javax.swing.JButton bt106;
    private javax.swing.JButton bt107;
    private javax.swing.JButton bt108;
    private javax.swing.JButton bt109;
    private javax.swing.JButton bt11;
    private javax.swing.JButton bt110;
    private javax.swing.JButton bt111;
    private javax.swing.JButton bt112;
    private javax.swing.JButton bt113;
    private javax.swing.JButton bt114;
    private javax.swing.JButton bt115;
    private javax.swing.JButton bt116;
    private javax.swing.JButton bt117;
    private javax.swing.JButton bt118;
    private javax.swing.JButton bt119;
    private javax.swing.JButton bt12;
    private javax.swing.JButton bt120;
    private javax.swing.JButton bt121;
    private javax.swing.JButton bt122;
    private javax.swing.JButton bt123;
    private javax.swing.JButton bt124;
    private javax.swing.JButton bt125;
    private javax.swing.JButton bt126;
    private javax.swing.JButton bt127;
    private javax.swing.JButton bt128;
    private javax.swing.JButton bt129;
    private javax.swing.JButton bt13;
    private javax.swing.JButton bt130;
    private javax.swing.JButton bt131;
    private javax.swing.JButton bt132;
    private javax.swing.JButton bt133;
    private javax.swing.JButton bt134;
    private javax.swing.JButton bt135;
    private javax.swing.JButton bt136;
    private javax.swing.JButton bt137;
    private javax.swing.JButton bt138;
    private javax.swing.JButton bt139;
    private javax.swing.JButton bt14;
    private javax.swing.JButton bt140;
    private javax.swing.JButton bt141;
    private javax.swing.JButton bt142;
    private javax.swing.JButton bt143;
    private javax.swing.JButton bt144;
    private javax.swing.JButton bt145;
    private javax.swing.JButton bt146;
    private javax.swing.JButton bt147;
    private javax.swing.JButton bt148;
    private javax.swing.JButton bt149;
    private javax.swing.JButton bt15;
    private javax.swing.JButton bt150;
    private javax.swing.JButton bt151;
    private javax.swing.JButton bt152;
    private javax.swing.JButton bt153;
    private javax.swing.JButton bt154;
    private javax.swing.JButton bt155;
    private javax.swing.JButton bt156;
    private javax.swing.JButton bt157;
    private javax.swing.JButton bt158;
    private javax.swing.JButton bt159;
    private javax.swing.JButton bt16;
    private javax.swing.JButton bt160;
    private javax.swing.JButton bt161;
    private javax.swing.JButton bt162;
    private javax.swing.JButton bt163;
    private javax.swing.JButton bt164;
    private javax.swing.JButton bt165;
    private javax.swing.JButton bt166;
    private javax.swing.JButton bt167;
    private javax.swing.JButton bt168;
    private javax.swing.JButton bt169;
    private javax.swing.JButton bt17;
    private javax.swing.JButton bt170;
    private javax.swing.JButton bt171;
    private javax.swing.JButton bt172;
    private javax.swing.JButton bt173;
    private javax.swing.JButton bt174;
    private javax.swing.JButton bt175;
    private javax.swing.JButton bt176;
    private javax.swing.JButton bt177;
    private javax.swing.JButton bt178;
    private javax.swing.JButton bt179;
    private javax.swing.JButton bt18;
    private javax.swing.JButton bt180;
    private javax.swing.JButton bt181;
    private javax.swing.JButton bt182;
    private javax.swing.JButton bt183;
    private javax.swing.JButton bt184;
    private javax.swing.JButton bt185;
    private javax.swing.JButton bt186;
    private javax.swing.JButton bt187;
    private javax.swing.JButton bt188;
    private javax.swing.JButton bt189;
    private javax.swing.JButton bt19;
    private javax.swing.JButton bt190;
    private javax.swing.JButton bt191;
    private javax.swing.JButton bt192;
    private javax.swing.JButton bt193;
    private javax.swing.JButton bt194;
    private javax.swing.JButton bt195;
    private javax.swing.JButton bt196;
    private javax.swing.JButton bt2;
    private javax.swing.JButton bt20;
    private javax.swing.JButton bt21;
    private javax.swing.JButton bt22;
    private javax.swing.JButton bt23;
    private javax.swing.JButton bt24;
    private javax.swing.JButton bt25;
    private javax.swing.JButton bt26;
    private javax.swing.JButton bt27;
    private javax.swing.JButton bt28;
    private javax.swing.JButton bt29;
    private javax.swing.JButton bt3;
    private javax.swing.JButton bt30;
    private javax.swing.JButton bt31;
    private javax.swing.JButton bt32;
    private javax.swing.JButton bt33;
    private javax.swing.JButton bt34;
    private javax.swing.JButton bt35;
    private javax.swing.JButton bt36;
    private javax.swing.JButton bt37;
    private javax.swing.JButton bt38;
    private javax.swing.JButton bt39;
    private javax.swing.JButton bt4;
    private javax.swing.JButton bt40;
    private javax.swing.JButton bt41;
    private javax.swing.JButton bt42;
    private javax.swing.JButton bt43;
    private javax.swing.JButton bt44;
    private javax.swing.JButton bt45;
    private javax.swing.JButton bt46;
    private javax.swing.JButton bt47;
    private javax.swing.JButton bt48;
    private javax.swing.JButton bt49;
    private javax.swing.JButton bt5;
    private javax.swing.JButton bt50;
    private javax.swing.JButton bt51;
    private javax.swing.JButton bt52;
    private javax.swing.JButton bt53;
    private javax.swing.JButton bt54;
    private javax.swing.JButton bt55;
    private javax.swing.JButton bt56;
    private javax.swing.JButton bt57;
    private javax.swing.JButton bt58;
    private javax.swing.JButton bt59;
    private javax.swing.JButton bt6;
    private javax.swing.JButton bt60;
    private javax.swing.JButton bt61;
    private javax.swing.JButton bt62;
    private javax.swing.JButton bt63;
    private javax.swing.JButton bt64;
    private javax.swing.JButton bt65;
    private javax.swing.JButton bt66;
    private javax.swing.JButton bt67;
    private javax.swing.JButton bt68;
    private javax.swing.JButton bt69;
    private javax.swing.JButton bt7;
    private javax.swing.JButton bt70;
    private javax.swing.JButton bt71;
    private javax.swing.JButton bt72;
    private javax.swing.JButton bt73;
    private javax.swing.JButton bt74;
    private javax.swing.JButton bt75;
    private javax.swing.JButton bt76;
    private javax.swing.JButton bt77;
    private javax.swing.JButton bt78;
    private javax.swing.JButton bt79;
    private javax.swing.JButton bt8;
    private javax.swing.JButton bt80;
    private javax.swing.JButton bt81;
    private javax.swing.JButton bt82;
    private javax.swing.JButton bt83;
    private javax.swing.JButton bt84;
    private javax.swing.JButton bt85;
    private javax.swing.JButton bt86;
    private javax.swing.JButton bt87;
    private javax.swing.JButton bt88;
    private javax.swing.JButton bt89;
    private javax.swing.JButton bt9;
    private javax.swing.JButton bt90;
    private javax.swing.JButton bt91;
    private javax.swing.JButton bt92;
    private javax.swing.JButton bt93;
    private javax.swing.JButton bt94;
    private javax.swing.JButton bt95;
    private javax.swing.JButton bt96;
    private javax.swing.JButton bt97;
    private javax.swing.JButton bt98;
    private javax.swing.JButton bt99;
    private javax.swing.JButton btDesistir;
    private javax.swing.JButton btRestart;
    private javax.swing.JLabel lbNivelDificuldade;
    private javax.swing.JLabel lbPontuacao;
    private javax.swing.JPanel painelInfo;
    private javax.swing.JPanel painelJogador;
    private javax.swing.JPanel painelJogoDificil;
    private javax.swing.JPanel painelJogoNormal;
    private javax.swing.JPanel painelMenuRapido;
    private javax.swing.JPanel painelPontuacao;
    // End of variables declaration//GEN-END:variables
}
