package CampoMinado.Principal;

import CampoMinado.Classes.ConexaoSQLite;
import CampoMinado.Telas.TelaMenuPrincipal;
import java.io.File;
import javax.swing.*;
import javax.swing.plaf.metal.MetalLookAndFeel;

/**
 * @author Tarcísio de Lima
 * @version 1.0.0
 */
public class Principal {
    //Objetos e Atributos Globais
    private static File arquivoBanco = new File("MineSweeperBD.db");
    
    /**
     * @param args
     * 
     * Metodo <b>main</b> responsavel pela execução de todo programa, ele recebe como parametros um vetor
     * de <b>Strings</b> e não retorna valor nenhum.
     */
    public static void main(String[] args) {
        
        if(arquivoBanco.exists() == false){
            ConexaoSQLite.conectarBancoDados(null);
            ConexaoSQLite.criarBancoDados(null);
            ConexaoSQLite.desconectarBancoDados(null);
        }
        
        try {
            UIManager.setLookAndFeel(new MetalLookAndFeel());
            new TelaMenuPrincipal().setVisible(true);
        } catch (UnsupportedLookAndFeelException lafErro) {
            JOptionPane.showMessageDialog(null, "Um erro inesperado ocorreu, não foi possivel iniciar o programa, "
                    + "assinatura do erro:\n"+lafErro, "Erro fatal!", JOptionPane.ERROR_MESSAGE);
        }
    }
}
